;
;      To plot 1D slice for 1D test simulations
;      2006.1.20(the)
;
;  Usage:
;
;  idl> .r dread
;  idl> .r vonz1D
;
;
; choice of device
;plot_choice='ps'
plot_choice='x'

iwx=2 & jwy=2

!p.charsize=1.0
!p.charthick=1.1
!p.thick=1.1
;!p.charsize=1.1
;!p.charthick=1.0
;!p.thick=1.0
chsz=1.0
rx=0.6

izon=4
;izon=0 : z=0.0 rs

mm1=0
mm2=20


!x.range=[1.0,30.0]

int=1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
case plot_choice of
'x' : begin
window,4,xs=700,ys=500,retain=2
end
'ps' : begin
set_plot,'ps'
device,/landscape
device,/color,xoffset=0.5,yoffset=26.5,xsize=20,ysize=14,font_size=8
;device,/inches,xsize=7.0,ysize=4.5,yoffset=2.5,xoffset=0.5,font_size=8.5
;!p.font=0 & device,/times,/italic
end
endcase

iw=iwx*jwy
dx0=1./iwx
dy0=1./jwy

!p.multi=[0,iwx,jwy]
!y.margin=[7.,7.]
!x.margin=[10.,6.]

tmp1=0.0
tmp2=0.0

;for i=0,ixmax-1 do begin
;tmp1=tmp1+(dd(i,21)-dd(i,1))
;tmp2=tmp2+dd(i,1)
;endfor
;print, tmp1, tmp2, tmp1/tmp2
;
;(1) density
;
;

;ldmyct,clr_index=clr,white=white,black=black
;loadct,5

!y.range=[1.e-3,0.1]

plot,xar,dd(*,mm1),title='!7q!8',xtitle='r',xstyle=1,ystyle=1,linestyle=0,/ylog
oplot,xar,dd(*,mm2),linestyle=1


i=0
	x0=dx0*(i mod iwx)
	y0=dy0*((i-(i mod iwx))/iwx)
   xyouts,/normal,x0+dx0*0.15,1.-(y0+dy0*0.20-0.03),'time = '
   xyouts,/normal,x0+dx0*0.50,1.-(y0+dy0*0.20-0.03), $
     strmid(strcompress(string(ctime(mm1)),/remove_all),0,4)



; (2) pressure
;

!y.range=[1.e-5,1.e-1]
;
plot,xar,pp(*,mm1),title='p',xtitle='r',xstyle=1,ystyle=1,linestyle=0,/ylog
oplot,xar,pp(*,mm2),linestyle=1

;;
;
;
;
; (3) Ux
;
gam=4./3.
roh=dd+(gam/(gam-1.)*pp)
cssq=gam*pp/roh

!y.range=[-0.1,1.0]

plot,xar,-u1(*,mm1),title='u!Dr!N',xtitle='r',xstyle=1,ystyle=1,linestyle=0
oplot,xar,-u1(*,mm2),linestyle=2

;
; (3) Vphi
;

;!y.range=[-0.3,0.1]

;plot,xar,u2(*,mm1),title='u!Dphi!N',xtitle='r',xstyle=1,ystyle=1,linestyle=1
;oplot,xar,u2(*,mm2),linestyle=2
;
;
;
;
; (4) Bx
;
;
;!y.range=[-0.002,0.01]
;
;plot,xar,b1(*,mm1),title='B!Dx!N',xtitle='x',xstyle=1,ystyle=1,linestyle=0
;oplot,xar,b1(*,10),linestyle=1
;
;
; (5) radial 3-velocity
;

rr=xx
akm=0.0

del=rr*rr-2.*rr+akm*akm
sig=rr
grr=sig/del
;grr=1.+2.*rr/sig

lo=sqrt(1.+grr*u1*u1)
v1=u1/lo

!y.range=[-0.05,0.6]

plot,xar,-v1(*,mm1),title='v!Dr!N',xtitle='r',xstyle=1,ystyle=1,linestyle=0
oplot,xar,-v1(*,mm2),linestyle=1


if (plot_choice ne 'x') then begin
;   device,color=0
   device,/close
;   !p.font=-1
;   set_plot,'x'
endif
end


