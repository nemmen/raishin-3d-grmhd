sfile=1
mfile=2
inter=1
direc='/Users/mizuno/IDL/1DTest/'
ixmax=400
iskix=1
nq=9
ctime=fltarr(mfile+1)

xx=fltarr(ixmax,mfile+1)
u1=fltarr(ixmax,mfile+1)
u2=fltarr(ixmax,mfile+1)
u3=fltarr(ixmax,mfile+1)
b1=fltarr(ixmax,mfile+1)
b2=fltarr(ixmax,mfile+1)
b3=fltarr(ixmax,mfile+1)
dd=fltarr(ixmax,mfile+1)
pp=fltarr(ixmax,mfile+1)
;
for cfile=sfile,mfile,inter do begin
   print,cfile

;   cfname=strtrim(cfile,1)
   cfname=string(cfile,FORMAT='(I3.3)')

   odir=direc+'ok'+cfname
   openr,1,odir

   aa=fltarr(nq,ixmax)
   readf,1,aa
   readf,1,time
   close,1
   ctime(cfile)=time
   for i=0,ixmax-1 do begin
        xx(i,cfile)=aa(0,i)
        dd(i,cfile)=aa(1,i)
        u1(i,cfile)=aa(2,i)
        u2(i,cfile)=aa(3,i)
        u3(i,cfile)=aa(4,i)
        pp(i,cfile)=aa(5,i)   
        b1(i,cfile)=aa(6,i)
        b2(i,cfile)=aa(7,i)
        b3(i,cfile)=aa(8,i)  
   endfor

endfor
;
xar=fltarr(ixmax)
;
for i=0,ixmax-1 do begin
 xar(i)=xx(i,1)
endfor
;
cfile=mfile

end
