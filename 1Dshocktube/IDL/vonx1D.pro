;
;      To plot 1D slice for 1D test simulations
;      2006.1.20(the)
;
;  Usage:
;
;  idl> .r dread1
;  idl> .r vonz1D1
;
;
; choice of device
;plot_choice='ps'
plot_choice='x'

iwx=4 & jwy=2

!p.charsize=1.8
!p.charthick=2.0
!p.thick=1.5
;!p.charsize=2.0
;!p.charthick=2.0
;!p.thick=2.0
chsz=2.0
rx=0.6

;izon=0 : z=0.0 rs

!x.range=[-0.5,0.5]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
case plot_choice of
'x' : begin
window,4,xs=700,ys=500,retain=2
end
'ps' : begin
set_plot,'ps'
device,/landscape
device,/color,xoffset=0.5,yoffset=24.5,xsize=24,ysize=14,font_size=8
;device,/inches,xsize=7.0,ysize=4.5,yoffset=2.5,xoffset=0.5,font_size=8.5
;!p.font=0 & device,/times,/italic
end
endcase

iw=iwx*jwy
dx0=1./iwx
dy0=1./jwy

!p.multi=[0,iwx,jwy]
!y.margin=[4.,5.]
!x.margin=[5.,6.]

mm1=1
mm2=2

gam=2.0
utsq=u1^2+u2^2+u3^2
;vsq=v1^2+v2^2+v3^2
;
lo=sqrt(1.+utsq)
;lo=1.0/sqrt(1.0-vsq)

v1=u1/lo
v2=u2/lo
v3=u3/lo

bt=b1^2+b2^2+b3^2

bt1=(b1^2+b2^2+b3^2)/lo^2+(b1*v1+b2*v2+b3*v3)^2
;
;(1) density
;
;

;ldmyct,clr_index=clr,white=white,black=black
;loadct,5

!y.range=[0.0,1.1]

plot,xar,dd(*,mm1),title='!7q!8',xtitle='x',xstyle=1,ystyle=1,linestyle=2
oplot,xar,dd(*,mm2),linestyle=0

i=0
	x0=dx0*(i mod iwx)
	y0=dy0*((i-(i mod iwx))/iwx)
   xyouts,/normal,x0+dx0*0.15,1.-(y0+dy0*0.20-0.05),'time = '
;   xyouts,/normal,x0+dx0*0.50,1.-(y0+dy0*0.20-0.05), $
   xyouts,/normal,x0+dx0*0.7,1.-(y0+dy0*0.20-0.05), $
     strmid(strcompress(string(ctime(mm2)),/remove_all),0,4)
;
; (2) pressure
;

!y.range=[0.0,1.1]


plot,xar,pp(*,mm1),title='p',xtitle='x',xstyle=1,ystyle=1,linestyle=2
oplot,xar,pp(*,mm2),linestyle=0

;
; (3) Vx
;

!y.range=[-0.1,0.5]

plot,xar,v1(*,mm1),title='v!Dx!N',xtitle='x',xstyle=1,ystyle=1,linestyle=2
oplot,xar,v1(*,mm2),linestyle=0

;
; (4) Vy
;

!y.range=[-0.8,0.2]

plot,xar,v2(*,mm1),title='v!Dy!N',xtitle='x',xstyle=1,ystyle=1,linestyle=2
oplot,xar,v2(*,mm2),linestyle=0

;
; (5) Vz
;

!y.range=[-0.5,0.5]
;
plot,xar,v3(*,mm1),title='v!Dz!N',xtitle='x',xstyle=1,ystyle=1,linestyle=2
oplot,xar,v3(*,mm2),linestyle=0

; (6) By
;

!y.range=[-1.2,1.2]

plot,xar,b2(*,mm1),title='B!Dy!N',xtitle='x',xstyle=1,ystyle=1,linestyle=2
oplot,xar,b2(*,mm2),linestyle=0

;
; (7) Bz
;

!y.range=[-1.0,1.0]
;
plot,xar,b3(*,mm1),title='B!Dz!N',xtitle='x',xstyle=1,ystyle=1,linestyle=2
oplot,xar,b3(*,mm2),linestyle=0

;
; (8)Lorentz factor
;

!y.range=[0.9,1.5]


plot,xar,lo(*,mm1),title='Lorentz factor',xtitle='x',xstyle=1,ystyle=1,linestyle=2
oplot,xar,lo(*,mm2),linestyle=0


if (plot_choice ne 'x') then begin
;   device,color=0
   device,/close
;   !p.font=-1
;   set_plot,'x'
endif
end


