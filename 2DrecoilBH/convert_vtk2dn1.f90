!***********************************************************************
!      RAISHIN code: 3D General Relativistic MHD (3D MPI) version 
!      program for output to VTK format
!      written by Y. Mizuno
!      ver. 150715
!      new version
!      using "uria" in main program (output1)
!***********************************************************************
!
program convert_vtk2dn1
  use pram, only: imax, jmax, kmax, iprocs, jprocs, kprocs, akm, metric

  implicit none

!======================================================================@
!    Difinition for variables
!======================================================================@
!
  integer, parameter :: nv=9
  integer, parameter :: npe=iprocs*jprocs*kprocs ! number of cpus 
  integer, parameter :: ns=0, ne=1 ! start and end data file number
  integer, parameter :: dataformat=0 !- 0= ascii, 1=binary -!
  integer, parameter :: idirec=12 ! direction (1=x, 2=y, 3=z)

!  integer, parameter :: nq=13
!
  integer :: i, j, k, l, m, n
  integer :: is, ie, js, je, ks, ke
  integer :: myrank, nd, npe1, myrank1, merr
  integer :: jh, kh, jh1, kh1, mmax, nmax
!  
  character*256 :: filename, filename1 
!
  real(8) :: uri(nv,imax,jmax,kmax)
  real(8) :: detg(imax,jmax,kmax), gfl(imax,jmax,kmax)
  real(8) :: x1(imax), x2(jmax), x3(kmax)
  real(8) :: rr, th, ph, sth, cth, del, sig, aa, gfl1a
  real(8) :: util(1:3), gcov1(0:3,0:3), gcon1(0:3,0:3)
!
  real(8), allocatable :: uri1(:,:,:,:), detg1(:,:,:), x1a(:), x2a(:), x3a(:)
  real(8), allocatable :: gfl1(:,:,:)
  real(8) :: time
  
!  real(8) :: qq(nq,imax,jmax,kmax)
! 
  real(4), allocatable :: xx1(:,:), xx2(:,:), xx3(:,:)
  real(4), allocatable :: dd(:,:), pp(:,:), ink(:,:)  
  real(4), allocatable :: u1(:,:), u2(:,:), u3(:,:), gf(:,:) 
  real(4), allocatable :: b1(:,:), b2(:,:), b3(:,:) 
!
!
!======================================================================@
!     File Open
!======================================================================@
!
  open(unit=6,file='convert.outdat',status='unknown')
!  open(unit=8,file='structr.outdat',status='unknown',form='unformatted')
!
!======================================================================@
!
! Data initialize
!
!  do k=1,kmax
!    do j=1,jmax
!      do i=1,imax
!        do n=1,nv
!          uri(n,i,j,k)=1.d0
!        enddo
!        detg(i,j,k)=0.d0
!      enddo
!    enddo
!  enddo
!
!======================================================================@
!
! Start main loop
!
  do nd=ns,ne
    write(*,*) 'working on ',nd,'th data'
! 
! read each process data
!
    do myrank=0,npe-1
      write(*,*) 'reading ', myrank, 'th process data'

      write(filename,990) myrank, nd
990   format('structr',i3.3,'-',i3.3,'.outdat')
      open( unit=7,file=filename,form='unformatted',status='old')

      read(7) time, myrank1, npe1
!
      if(myrank .ne. myrank1) then
        write(*,*) 'reading process data is wrong'
         stop
      endif
      if(npe .ne. npe1) then
        write(*,*) 'total process number is wrong'
        stop
      endif   
!
      read(7) is,ie,js,je,ks,ke
      allocate(uri1(nv,0:ie-is,0:je-js,0:ke-ks), &
               detg1(0:ie-is,0:je-js,0:ke-ks), gfl1(0:ie-is,0:je-js,0:ke-ks), &
               x1a(0:ie-is), x2a(0:je-js), x3a(0:ke-ks),stat=merr)

      do i=0,ie-is
        do j=0,je-js
          do k=0,ke-ks
            read(7) x1a(i),x2a(j),x3a(k),uri1(1,i,j,k),uri1(2,i,j,k), &
                   uri1(3,i,j,k),uri1(4,i,j,k),uri1(5,i,j,k),uri1(6,i,j,k), &
                   uri1(7,i,j,k),uri1(8,i,j,k),uri1(9,i,j,k), detg1(i,j,k) 
          enddo
        enddo
      enddo
!
!- calculation of Lorentz facor -!
      do i=0,ie-is
        do j=0,je-js
          do k=0,ke-ks            
!
            rr=x1a(i)
            ph=x2a(j)
            th=x3a(k)

            do n=0,3
              do m=0,3
                gcov1(m,n)=0.d0
                gcon1(m,n)=0.d0
              enddo
            enddo  
!
            sth=sin(th)
            cth=cos(th)
            del=rr*rr-(2.*rr)+akm*akm
            sig=rr*rr+akm*cth*akm*cth
            aa=((rr*rr+akm*akm)**2)-akm*akm*del*sth*sth
              
            if(metric .eq. 203) then

              gcov1(0,0)=-(sig-2.*rr)/sig     
              gcov1(1,1)=sig/del               
              gcov1(2,2)=(aa/sig)*sth**2 
              gcov1(0,2)=-(2.*akm*rr*(sth**2))/sig 
              gcov1(2,0)=gcov1(0,2)              
              gcov1(3,3)=sig                  
!
              gcon1(0,0)=-aa/(sig*del)
              gcon1(1,1)=del/sig                          
              gcon1(2,2)=(sig-2.*rr)/(del*sig*sin(th)*sin(th)) 
              gcon1(0,2)=-(2.*akm*rr)/(sig*del)           
              gcon1(2,0)=gcon1(0,2)   
              gcon1(3,3)=1./sig 
!               
            elseif(metric .eq. 303) then

              gcov1(0,0)=-1.+2.*rr/sig     
              gcov1(0,1)=2.*rr/sig          
              gcov1(0,2)=-2.*akm*rr*sth*sth/sig 
              gcov1(1,0)=gcov1(0,1)        
              gcov1(1,1)=1.+2.*rr/sig      
              gcov1(1,2)=-1.*akm*sth*sth*(1.+2.*rr/sig) 
              gcov1(2,0)=gcov1(0,2)        
              gcov1(2,1)=gcov1(1,2)        
              gcov1(2,2)=aa*sth*sth/sig 

              gcon1(0,0)=-1.-2.*rr/sig
              gcon1(0,1)=2.*rr/sig
              gcon1(1,0)=gcon1(0,1)
              gcon1(1,1)=del/sig
              gcon1(1,2)=akm/sig
              gcon1(2,1)=gcon1(1,2)
              gcon1(2,2)=1./(sig*sth*sth)
              gcon1(3,3)=1./sig
              
            endif   
!
            util(1)=uri1(2,i,j,k)
            util(2)=uri1(3,i,j,k)
            util(3)=uri1(4,i,j,k)
!
            call calgfl(util,gfl1a,gcov1)

            gfl1(i,j,k)=gfl1a
          enddo
        enddo            
     enddo
!     
!- convert data on full array -!       
      do i=0,ie-is
        x1(is+i)=x1a(i)
      enddo
!
      do j=0,je-js
        x2(js+j)=x2a(j)
      enddo
!
      do k=0,ke-ks
        x3(ks+k)=x3a(k)
      enddo
!
      do k=0,ke-ks
        do j=0,je-js
          do i=0,ie-is
            uri(1,is+i,js+j,ks+k)=uri1(1,i,j,k)
            uri(2,is+i,js+j,ks+k)=uri1(2,i,j,k)
            uri(3,is+i,js+j,ks+k)=uri1(3,i,j,k)
            uri(4,is+i,js+j,ks+k)=uri1(4,i,j,k)
            uri(5,is+i,js+j,ks+k)=uri1(5,i,j,k)
            uri(6,is+i,js+j,ks+k)=uri1(6,i,j,k)
            uri(7,is+i,js+j,ks+k)=uri1(7,i,j,k)
            uri(8,is+i,js+j,ks+k)=uri1(8,i,j,k)
            uri(9,is+i,js+j,ks+k)=uri1(9,i,j,k)
            detg(is+i,js+j,ks+k)=detg1(i,j,k)
            gfl(is+i,js+j,ks+k)=gfl1(i,j,k)
          enddo
        enddo
      enddo    
!
      close(7)
      deallocate(uri1,detg1,gfl1,x1a,x2a,x3a,stat=merr)
    enddo
!
!----------------------------------------------------------------------@
!- Output data for analysis -!
!
!- set output file -!
    write(filename1,991) nd
    open( unit=9, file=filename1,status='replace',form="formatted")
991 format('ok',i3.3,'.vtk')  
!
    if(idirec .eq. 13) then !- xz plane -!
!
!- set half grid position -!
      jh1=(jmax+1)/2       
!- set j-th direction =1 -!
      mmax=1
!
!- data allocate -!
      allocate(xx1(imax,kmax), xx2(imax,kmax), xx3(imax,kmax), &
               dd(imax,kmax), pp(imax,kmax), ink(imax,kmax), &
               u1(imax,kmax), u2(imax,kmax), u3(imax,kmax), &
               b1(imax,kmax), b2(imax,kmax), b3(imax,kmax), &
               gf(imax,kmax),   stat=merr)
!
!- convert the data array -!
!
      do k=1, kmax 
        do i=1, imax
!    
          xx1(i,k)=x1(i)*sin(x3(k))
          xx2(i,k)=x1(i)*cos(x3(k))
          xx3(i,k)=0.d0
            
          dd(i,k)=uri(1,i,jh1,k) !- density -!            
          u1(i,k)=uri(2,i,jh1,k)*sin(x3(k)) &
                 +uri(4,i,jh1,k)*cos(x3(k))
                 !- 1st component of velocity -!
          u2(i,k)=uri(2,i,jh1,k)*cos(x3(k)) &
                 -uri(4,i,jh1,k)*sin(x3(k))
                 !- 2nd component of velocity -!
          u3(i,k)=uri(3,i,jh1,k)
          pp(i,k)=uri(5,i,jh1,k) !- gas pressure -!
          b1(i,k)=uri(7,i,jh1,k)*sin(x3(k)) &
                 +uri(9,i,jh1,k)*cos(x3(k))
                 !- 1st component of B-field -!
          b2(i,k)=uri(7,i,jh1,k)*cos(x3(k)) &
                 -uri(9,i,jh1,k)*sin(x3(k))
                 !- 2nd component of B-field -!
          b3(i,k)=uri(8,i,jh1,k)
                 !- 3rd component of B-field -!
          if(k .eq. 60) then
            write(*,*) "rr, b_phi=", x1(i), uri(8,i,jh1,k), b3(i,k)
          endif   
          gf(i,k)=gfl(i,jh1,k)
        enddo
      enddo
!
      call write_vtk2d(imax,kmax,mmax,xx1,xx2,xx3,dd,u1,u2,u3,pp,&
                       b1,b2,b3,gf,nd,dataformat,filename)      
!
      deallocate(xx1, xx2, xx3, dd, pp, ink, u1, u2, u3, b1, b2, b3, gf, &
                 stat=merr)
!
    elseif(idirec .eq. 12) then !- xy plane -!
!      
!- set half grid position -!
      kh1=(kmax+1)/2       
!- set k-th direction =1 -!
      nmax=1
!- set j-th direction =jmax - overlap region
      mmax=jmax-6
!      mmax=jmax-5      
!
!- data allocate -!
      allocate(xx1(imax,mmax), xx2(imax,mmax), xx3(imax,mmax), &
               dd(imax,mmax), pp(imax,mmax), ink(imax,mmax), &
               u1(imax,mmax), u2(imax,mmax), u3(imax,mmax), &
               b1(imax,mmax), b2(imax,mmax), b3(imax,mmax), &
               gf(imax,mmax), stat=merr)
!
!- convert the data array -!
!
      do j=1, mmax 
        do i=1, imax
          jh1=j+3  
!    
          xx1(i,j)=x1(i)*cos(x2(jh1))
          xx2(i,j)=x1(i)*sin(x2(jh1))
          xx3(i,j)=0.d0
            
          dd(i,j)=uri(1,i,jh1,kh1) !- density -!            
          u1(i,j)=uri(2,i,jh1,kh1)*cos(x2(jh1)) &
                 -uri(3,i,jh1,kh1)*sin(x2(jh1))
                 !- 1st component of velocity -!
          u2(i,j)=uri(2,i,jh1,kh1)*sin(x2(jh1)) &
                 +uri(3,i,jh1,kh1)*cos(x2(jh1))
                 !- 2nd component of velocity -!
          u3(i,j)=uri(4,i,jh1,kh1)
          pp(i,j)=uri(5,i,jh1,kh1) !- gas pressure -!
          b1(i,j)=uri(7,i,jh1,kh1)*cos(x2(jh1)) &
                 -uri(8,i,jh1,kh1)*sin(x2(jh1))
                 !- 1st component of B-field -!
          b2(i,j)=uri(7,i,jh1,kh1)*sin(x2(jh1)) &
                 +uri(8,i,jh1,kh1)*cos(x2(jh1))
                 !- 2nd component of B-field -!
          b3(i,j)=uri(9,i,jh1,kh1)
                 !- 3rd component of B-field -!
          gf(i,j)=gfl(i,jh1,kh1)
        enddo
      enddo
!
      call write_vtk2d(imax,mmax,nmax,xx1,xx2,xx3,dd,u1,u2,u3,pp,&
                       b1,b2,b3,gf,nd,dataformat,filename)      
!
      deallocate(xx1, xx2, xx3, dd, pp, ink, u1, u2, u3, b1, b2, b3, gf, &
                 stat=merr)


       
    endif
!      
  enddo    
!
  stop    
end program convert_vtk2dn1

!------------------------------------------------------------
subroutine write_vtk2d(xmax,ymax,zmax,xx,yy,zz,dd,ux,uy,uz,pp,&
                       bx,by,bz,gf,mh,dataformat,filename)
!------------------------------------------------------------
!   
!- write data for VTK format -!
!  Now the data must be cartesian coordinates!
!  The gird points does not need to be regular.!
!
  implicit none
!
  integer :: i, j, k, xmax, ymax, zmax, mh
  integer :: merr
  integer :: dataformat
  character*256 :: filename
!  
  real(4) :: xx(xmax,ymax), yy(xmax,ymax), zz(xmax,ymax)
  real(4) :: dd(xmax,ymax), pp(xmax,ymax)  
  real(4) :: ux(xmax,ymax), uy(xmax,ymax), uz(xmax,ymax) 
  real(4) :: bx(xmax,ymax), by(xmax,ymax), bz(xmax,ymax)
  real(4) :: gf(xmax,ymax)
!  
  real(4) :: dum_r
  character(LEN=1), parameter :: newline = achar(10) ! newline symbol!
  
!  There are five basic parts to the VTK file format.  
!  1. Write file version and identifier (Header)
!     
    write(9, "(a)") "# vtk DataFile Version 2.0"    
!
!  2. Tilte
!    
    write(9, "(a)") "RMHD Simulation Result"    
!
!  3. Data type (ASCII or BINARY)
!    
    if(dataformat .eq. 0) then
      write(9, "(a)") "ASCII" 
    else
      write(9, "(a)") "BINARY"
    endif
!
!  4.  Dataset structure 
!
10  format(a11, 3(1x, i6))
11  format(a, 2x, i9, 2x, a5)
    write(9, "(a)") "DATASET STRUCTURED_GRID"
    write(9, 10)    "DIMENSIONS ", xmax, ymax, zmax
    write(9, 11)    "POINTS", xmax*ymax*zmax, "float"
!
    write(6,*) 'after header: mh=', mh
!
! -- now dumps coordnates
    if(dataformat .ne. 0) then 
      close(9)
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")
!           position="append", access="stream")
    endif
! -- now dumps data
    do j = 1, ymax
      do i = 1, xmax
        if(dataformat .eq. 0) then                 
          write(9,*) xx(i,j), yy(i,j), zz(i,j)
        else
          write(9) xx(i,j), yy(i,j), zz(i,j)
        endif
      enddo
    enddo
!
    write(6,*) 'after coordinates: mh=', mh
!    
!  5 . Data
!    
    if(dataformat .ne. 0) then
      close(9)
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")          
    endif
!
12  format(a, a11, 1x, i12)
! You need to add newline character for safty after writing data in binary
!   write(9, "(a)") 
    write(9, 12) newline, "POINT_DATA ", xmax*ymax*zmax
!
    write(6,*) 'after new line: mh=', mh   
!
! Write density
!
    write(9, "(a)") "SCALARS density float"
    write(9, "(a)") "LOOKUP_TABLE default"
!
! --- now dumps data
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")  
    endif
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax             
          dum_r = max(dd(i,j), 1e-30) ! for safety
          write(9,*) dum_r
        enddo
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          dum_r = max(dd(i,j), 1e-30) ! for safety
          write(9) dum_r
        enddo
      enddo
    endif
!
    write(6,*) 'after density: mh=', mh  
!
! Write velocity
!
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")   
    endif
    
    write(9, "(a, a)") newline, "VECTORS velocity float"
    
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")   
    endif
! -- now dumps data
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax
          write(9,*) ux(i,j), uy(i,j), uz(i,j)
        enddo           
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          write(9) ux(i,j), uy(i,j), uz(i,j)
        enddo
      enddo           
    endif
!
    write(6,*) 'after velocity vector: mh=', mh  
!
! Write pressure
!
    write(9, "(a)") "SCALARS pressure float"
    write(9, "(a)") "LOOKUP_TABLE default"
!
! --- now dumps data
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")  
    endif
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax             
          dum_r = max(pp(i,j), 1e-30) ! for safety
          write(9,*) dum_r
        enddo
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          dum_r = max(pp(i,j), 1e-30) ! for safety
          write(9) dum_r
        enddo
      enddo
    endif
!
    write(6,*) 'after pressure: mh=', mh  
!
! Write Lorentz factor
!
    write(9, "(a)") "SCALARS LorentzW float"
    write(9, "(a)") "LOOKUP_TABLE default"
!
! --- now dumps data
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")  
    endif
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax             
          dum_r = max(gf(i,j), 1e-30) ! for safety
          write(9,*) dum_r
        enddo
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          dum_r = max(gf(i,j), 1e-30) ! for safety
          write(9) dum_r
        enddo
      enddo
    endif
!
    write(6,*) 'after Lorentz factor: mh=', mh  
!
!
! Write B-field vector
!
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")   
    endif
    
    write(9, "(a, a)") newline, "VECTORS B-field float"
    
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
 !          position="append", access="stream") 
    endif
! -- now dumps data
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax
          write(9,*) bx(i,j), by(i,j), 0.d0
        enddo           
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          write(9) bx(i,j), by(i,j), bz(i,j)
        enddo
      enddo           
    endif
!
    write(9, "(a)") "SCALARS Bx float"
    write(9, "(a)") "LOOKUP_TABLE default"
!
! --- now dumps data
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")  
    endif
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax             
          dum_r = bx(i,j) ! for safety
          write(9,*) dum_r
        enddo
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          dum_r = bx(i,j) ! for safety
          write(9) dum_r
        enddo
      enddo
    endif
!
    write(9, "(a)") "SCALARS By float"
    write(9, "(a)") "LOOKUP_TABLE default"
!
! --- now dumps data
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")  
    endif
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax             
          dum_r = by(i,j) ! for safety
          write(9,*) dum_r
        enddo
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          dum_r = by(i,j) ! for safety
          write(9) dum_r
        enddo
      enddo
    endif   
!
    write(9, "(a)") "SCALARS Bz float"
    write(9, "(a)") "LOOKUP_TABLE default"
!
! --- now dumps data
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")  
    endif
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax             
          dum_r = bz(i,j) ! for safety
          write(9,*) dum_r
        enddo
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          dum_r = bz(i,j) ! for safety
          write(9) dum_r
        enddo
      enddo
    endif
    
    write(6,*) 'end of the data: mh=', mh  
!
    close(9)
!
  return
end subroutine write_vtk2d
!
!--------------------------------------------------------------------
subroutine calgfl(util,gfl,gcov1)
!--------------------------------------------------------------------
!- Calculation of Lorentz factor -!
!
  implicit none

  integer :: m, n
  real(8) :: gcov1(0:3,0:3) !- covariant metric -!
  real(8) :: util(1:3) !- \tilde{u}^i = \gamma v^i -!
  real(8) :: utsq, gfl
!
  utsq=0.d0
!  vsq=0.d0
!
  do m=1,3
    do n=1,3
      utsq=utsq+gcov1(m,n)*util(m)*util(n)     
    enddo
  enddo
!
  if(utsq .lt. 0.d0) then
    if(abs(utsq) .le. 1.0d-10) then
      utsq=abs(utsq)
    else
      utsq=1.d-10 !- set floot number -!
    endif
  endif
  if(abs(utsq) .gt. 1.0d10) then
    utsq=1.0d9
  endif
!
  gfl=sqrt(1.+utsq) !- Lorentz factor -!
!  gfl=1./sqrt(1.-vsq)
!
!  util(1)=gfl*vcon(1)
!  util(2)=gfl*vcon(2)
!  util(3)=gfl*vcon(3)
!
  return
end subroutine calgfl
!
