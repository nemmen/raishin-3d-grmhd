;;
;; $Id: color_bar.pro,v 1.1 1998/06/05 03:11:35 katosi Exp $
;;
pro color_bar,levels1,clr_index $
     ,Position=position, Noerase=noerase $
     ,Out=out $
     ,Label_position=label_position $
     ,Horizon=horizon,b_min=b_min,b_max=b_max

;+
; NAME:
;   color_bar
; PURPOSE:
;   make a color bar
; CATEGORY:
;   General graphics.
; CALLING SEQUENCE:
;   color_bar, levels, clr_index
; INPUTS:
;   levels1 = 1-d array for levels of color map
;   clr_index = 1-d array for color map.
;            Each element of this array cooresponds to one in levels1.
; OPTIONAL INPUT PARAMETERS:
;
; KEYWORD PARAMETERS:
;   position = set the position in normalized cordinate [x0,y0,x1,y1]
;   out =  The level assumed outside the plotted region in filled-contour map.
;   b_max = the maximum value for the color bar
;   b_min = the minimum value for the color bar
;
; OUTPUTS:
;       No explicit outputs.
; COMMON BLOCKS:
;       none.
; SIDE EFFECTS:
;       The currently selected display is affected.
; RESTRICTIONS:
;       None that are obvious.
; PROCEDURE:
;
; MODIFICATION HISTORY:
;    ver 0.0  T. yokoyama (Ntl Astron. Obs. Japan) Oct. 8, 1996
;    ver 0.1  T. yokoyama (Ntl Astron. Obs. Japan) Oct. 10, 1996
;-

on_error,2                      ;Return to caller if an error occurs
sz = size(levels1)             ;Size of image

c_mx = !d.n_colors-1          ;Brightest color
black = 0b & white = c_mx

;;;;;;;;;;;;;;;; default parameters ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if n_elements(out) eq 0 then out=-10.
if n_elements(noerase) eq 0 then noerase=0
if n_elements(label_position) eq 0 then label_position=0
if n_elements(horizon) eq 0 then horizon=0
if n_elements(b_min) eq 0 then b_min=levels1(0)
if n_elements(b_max) eq 0 then $
 b_max=levels1(sz(1)-1)+(levels1(sz(1)-1)-levels1(sz(1)-2))
;;;;;;;;; Flame ;;;;;;;;;;;;;;;
;set window used by contour
if (n_elements(position) eq 0) then begin
  contour,[[0,0],[1,1]],/nodata, xstyle=4, ystyle = 4

  pxb = !x.window * !d.x_vsize	;Get size of window in device units
  pyb = !y.window * !d.y_vsize

  pyb=pyb-8.
endif else begin
  pxb=[position(0),position(2)] * !d.x_vsize
  pyb=[position(1),position(3)] * !d.y_vsize
endelse

  swxb = pxb(1)-pxb(0)		;Size in x in device units
  swyb = pyb(1)-pyb(0)		;Size in Y

;;;;;;;;;;;;;;;; color bar ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
db=float(b_max-b_min)
if (horizon eq 0) then begin
  ib=20 & jb=256
  yb=b_min+db/jb*findgen(jb)
  xb=findgen(ib)
  bardata=fltarr(ib,jb)
  for i=0,ib-1 do bardata(i,*)=yb
  xrange=[xb(0),xb(ib-1)] 
  yrange=[b_min,b_max]
endif else begin
  ib=256 & jb=20
  xb=b_min+db/ib*findgen(ib)
  yb=findgen(jb)
  bardata=fltarr(ib,jb)
  for j=0,jb-1 do bardata(*,j)=xb
  xrange=[b_min,b_max]
  yrange=[yb(0),yb(jb-1)]
endelse

bardata2=replicate(out,ib+2,jb+2)
xb2=[xb(0)-(xb(1)-xb(0)),xb,xb(ib-1)+(xb(ib-1)-xb(ib-2))]
yb2=[yb(0)-(yb(1)-yb(0)),yb,yb(jb-1)+(yb(jb-1)-yb(jb-2))]
bardata2(1,1)=bardata

if (long(strmid(!version.release,0,1)) le 3) then begin
spawn,'echo $$',pid
tmpfile=pid(0)+'tmp_cpath.dat'
contour,bardata2,xb2,yb2,levels=levels1,path_filename=tmpfile,$
         xst=5,yst=5,$
         pos = [pxb(0),pyb(0), pxb(0)+swxb,pyb(0)+swyb],/dev,$
         xrange=xrange,yrange=yrange,$
         noerase=noerase
base_color=clr_index(0)
polyfill,[xb(0),xb(ib-1),xb(ib-1),xb(0)],$
   [yb(0),yb(0),yb(jb-1),yb(jb-1)],$
   color=base_color
polycontour,tmpfile,color_index=[clr_index(0),clr_index],/delete_file
endif else begin
contour,/fill,bardata2,xb2,yb2,levels=levels1, $
         xst=5,yst=5,$
         pos = [pxb(0),pyb(0), pxb(0)+swxb,pyb(0)+swyb],/dev,$
         xrange=xrange,yrange=yrange,$
         noerase=noerase,$
         c_colors=clr_index
endelse

frame_color=black
if (!d.name eq 'PS') then frame_color=white
polyfill,[xb(0),xb2(0),xb2(0),xb(0)],$
   [yb(0),yb(0),yb(jb-1),yb(jb-1)],$
   color=frame_color
polyfill,[xb(ib-1),xb2(ib+1),xb2(ib+1),xb(ib-1)],$
   [yb(0),yb(0),yb(jb-1),yb(jb-1)],$
   color=frame_color
polyfill,[xb2(0),xb2(ib+1),xb2(ib+1),xb2(0)],$
   [yb2(0),yb2(0),yb(0),yb(0)],$
   color=frame_color
polyfill,[xb2(0),xb2(ib+1),xb2(ib+1),xb2(0)],$
   [yb2(jb+1),yb2(jb+1),yb(jb-1),yb(jb-1)],$
   color=frame_color

;;;;;;;;;;;;;;;; axis and frame ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

axis_color=white
if (!d.name eq 'PS') then axis_color=black
blank_arr=[' ',' ',' ',' ',' ',' ',' ',' ']

if (horizon eq 0) then begin
   plots,[xb(0),xb(ib-1)],[yb(0),yb(0)],color=axis_color
   plots,[xb(0),xb(ib-1)],[yb(jb-1),yb(jb-1)],color=axis_color
if (label_position eq 0) then begin
   axis,yaxis=0                 ,color=axis_color,yst=1,yrange=yrange
   axis,yaxis=1,ytickn=blank_arr,color=axis_color,yst=1,yrange=yrange
endif else begin
   axis,yaxis=1                 ,color=axis_color,yst=1,yrange=yrange
   axis,yaxis=0,ytickn=blank_arr,color=axis_color,yst=1,yrange=yrange
endelse
endif else begin
   plots,[xb(0),xb(0)],[yb(0),yb(jb-1)],color=axis_color
   plots,[xb(ib-1),xb(ib-1)],[yb(0),yb(jb-1)],color=axis_color
if (label_position eq 0) then begin
   axis,xaxis=0                 ,color=axis_color,xst=1,xrange=xrange
   axis,xaxis=1,xtickn=blank_arr,color=axis_color,xst=1,xrange=xrange
endif else begin
   axis,xaxis=1                 ,color=axis_color,xst=1,xrange=xrange
   axis,xaxis=0,xtickn=blank_arr,color=axis_color,xst=1,xrange=xrange
endelse
endelse

return
end

