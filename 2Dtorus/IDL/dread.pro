sfile=1
mfile=6
inter=1
direc='/Users/mizuno/IDL/2Dacc/'
ixmax=128
iymax=512
iskix=1
iskiy=1
nq=11
ctime=fltarr(mfile+1)

xx=fltarr(ixmax,iymax,mfile+1)
yy=fltarr(ixmax,iymax,mfile+1)
zz=fltarr(ixmax,iymax,mfile+1)
v1=fltarr(ixmax,iymax,mfile+1)
v2=fltarr(ixmax,iymax,mfile+1)
v3=fltarr(ixmax,iymax,mfile+1)
b1=fltarr(ixmax,iymax,mfile+1)
b2=fltarr(ixmax,iymax,mfile+1)
b3=fltarr(ixmax,iymax,mfile+1)
dd=fltarr(ixmax,iymax,mfile+1)
pp=fltarr(ixmax,iymax,mfile+1)
pom=fltarr(ixmax,iymax,mfile+1)
;
for cfile=sfile,mfile,inter do begin
   print,cfile

;   cfname=strtrim(cfile,1)
   cfname=string(cfile,FORMAT='(I3.3)')

   odir=direc+'ok'+cfname
   openr,1,odir

   aa=fltarr(nq,ixmax,iymax)
   readf,1,aa
   readf,1,time
   close,1
   ctime(cfile)=time
   for i=0,ixmax-1 do begin
      for j=0,iymax-1 do begin
        xx(i,j,cfile)=aa(0,i,j)
        zz(i,j,cfile)=aa(1,i,j)
        dd(i,j,cfile)=aa(2,i,j)
        v1(i,j,cfile)=aa(3,i,j)
        v2(i,j,cfile)=aa(4,i,j)
        v3(i,j,cfile)=aa(5,i,j)
        pp(i,j,cfile)=aa(6,i,j)   
        b1(i,j,cfile)=aa(7,i,j)
        b2(i,j,cfile)=aa(8,i,j)
        b3(i,j,cfile)=aa(9,i,j)
        pom(i,j,cfile)=aa(10,i,j)        
      endfor  
   endfor

endfor
;
xar=fltarr(ixmax)
yar=fltarr(iymax)
;
for i=0,ixmax-1 do begin
 xar(i)=xx(i,1,1)
endfor
;
for j=0,iymax-1 do begin
 yar(j)=zz(1,j,1)
endfor
;
cfile=mfile

end
