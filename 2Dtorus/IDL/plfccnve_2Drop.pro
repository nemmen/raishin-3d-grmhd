;;
;; Ploting 2D results
;;
;; $Id$
;;
; choice of device
;plot_choice='cps'
;plot_choice='ps'
plot_choice='x'
;
iwx=2 & jwy=1

a=findgen(17) * (!pi * 2 / 16.)
usersym, 0.5*cos(a), 0.5*sin(a),/fill

!p.charsize=1.5
!p.charthick=1.5
;!p.charsize=1.0
;!p.charthick=1.0

mmmin=1
mmmax=6

xrange=[0.0,60.0] & yrange=[-40.0,40.0]


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
case plot_choice of
'x' : begin
window,4,xs=600,ys=500,xpos=400,retain=2
;window,5,xs=1200,ys=1200,xpos=400
end
'ps' : begin
set_plot,'ps'
device,/portrait
device,/inches,xsize=7,ysize=7.5,yoffset=1,font_size=8
;!p.font=0 & device,/times,/italic
end
'cps' : begin
set_plot,'ps'
;device,/color,bits_per_pix=5
device,/color,/inches,xsize=7.5,ysize=5.5,yoffset=1,font_size=8
;device,/landscape
;device,xoffset=0.5,yoffset=24.5,xsize=19*6.5/7.*0.8,ysize=19*0.8
;!p.font=0 & device,/times,/italic
end
endcase

iw=iwx*jwy
dx0=1./iwx
dy0=1./jwy

!p.multi=[0,iwx,jwy]
!x.margin=[7.,8.]
!y.margin=[4.,5.]

mm1=1
mm2=6

i=0
itemp=0
;
; If plot_choice='ps' or 'cps', following line should comment out.
;
; CASE !D.Name OF
; 'X': device,decompose=0
; ELSE:
; ENDCASE
;
; If you want to use different color palette, you should change number of loadct; or select ldmyct (original color palette)
;
;   loadct,1
;   TVLCT, r, g, b, /Get
;   TVLCT, Reverse(r), Reverse(g), Reverse(b)
   
 ; loadct,0
  If(plot_choice eq 'x') then begin
    device,decompose=0
  endif
    ldmyct,clr_index=clr,white=white,black=black
;

;for i=0,ixmax-1 do begin
;  for j=0,iymax-1 do begin
;    for m=sfile,mfile do begin
;    
;      if(dd(i,j,m) le 0.01) then begin
;         dd(i,j,m)=0.1
;      endif
;    
;    endfor
;  endfor
;endfor
;
; (1) density
;
  
  data1=alog10(dd)
;  data1=dd
  
clr=[10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,190]

;levels1=[0.0,0.5,1.0,2.5,5.0,7.5,10.0,12.5,15.0,17.5,20.0,25.0,30.0,100.0]
;levels1=[-5.0,-3.0,-2.5,-2.2,-2.0,-1.8,-1.6,-1.4,-1.2,-1.0,-0.8,-0.6,-0.4,-0.2,0.0,0.1]
levels1=[-10.0,-3.0,-2.0,-1.8,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.75,-0.5,-0.25,1.0]
;levels1=[-3.0,-2.5,-2.0,-1.5,-1.0,-0.75,-0.5,-0.2,0.0,0.5,1.0,1.5,2.0,2.5,3.0]


;data2=abs(v2)
data2=pom
  
    fccnve $
     ,nofilled=0,nocontour=0,novector=0 $
     ,xrange=xrange,yrange=yrange $
     ,data1(*,*,mm1) $
     ,levels1=levels1,clr_index=clr, c_thick=1.5 $
     ,data2(*,*,mm1) $
     ,levels2=min(data2(*,*,mm1))+(max(data2(*,*,mm1))-min(data2(*,*,mm1)))*(findgen(10)/10)$
;     ,levels2=[0.0] $
     ,v1(*,*,mm1),v3(*,*,mm1) $
     ,scale=4.0,index_size=1.0, xarrow=0.65, limit=0.005,/sample $
     ,xplot=-0.0+4.0*findgen(400),yplot=-100.0+4.0*findgen(400) $
     ,xcord=xar,ycord=yar,xtitle='!8x',ytitle='!8z' $
     ,v_color=white 
;
; Shade of Black Hole
pi=3.141592
meshb=80
rblak=2.0
thet=findgen(meshb+1)/meshb*pi*0.5
;
xblak=rblak*cos(thet)
yblak=rblak*sin(thet)
xblak=[xblak,0.0,0.0]
yblak=[yblak,0.0,0.0]
polyfill,xblak,yblak

thet=findgen(meshb+1)/meshb*pi*0.5
;
xblak1=rblak*cos(2.*pi-thet)
yblak1=rblak*sin(2.*pi-thet)
xblak1=[xblak1,0.0,0.0]
yblak1=[yblak1,0.0,0.0]
polyfill,xblak1,yblak1

i=0
	x0=dx0*(i mod iwx)
	y0=dy0*((i-(i mod iwx))/iwx)

   xyouts,/normal,x0+dx0*0.08,1.-(y0+dy0*0.10-0.05),'!8(a)'
   
   xyouts,/normal,x0+dx0*0.25,1.-(y0+dy0*0.10-0.05),'!8time = '
   xyouts,/normal,x0+dx0*0.58,1.-(y0+dy0*0.10-0.05), $
     strmid(strcompress(string(ctime(mm1)),/remove_all),0,4)

;data2=abs(v3)
data2=abs(pom)

    printf,-1,mm2,format='(i3,$)'
    fccnve $
     ,nofilled=0,nocontour=0,novector=0 $
     ,xrange=xrange,yrange=yrange $
     ,data1(*,*,mm2) $
     ,levels1=levels1,clr_index=clr, c_thick=1.5 $
     ,data2(*,*,mm2) $
     ,levels2=min(data2(*,*,mm2))+(max(data2(*,*,mm2))-min(data2(*,*,mm2)))*(findgen(10)/10)^2$
;     ,levels2=min(data2(*,*,mm2))+(max(data2(*,*,mm2))-min(data2(*,*,mm2)))*(findgen(20)/20)$
     ,v1(*,*,mm2),v3(*,*,mm2) $
     ,scale=4.0,index_size=1.0, xarrow=0.65, limit=0.01,/sample $
     ,xplot=-0.+4.0*findgen(200),yplot=-100.+4.0*findgen(100) $
     ,xcord=xar,ycord=yar, xtitle='!8x',ytitle='!8z' $
     ,v_color=white

polyfill,xblak,yblak
polyfill,xblak1,yblak1

i=1
	x0=dx0*(i mod iwx)
	y0=dy0*((i-(i mod iwx))/iwx)

   xyouts,/normal,x0+dx0*0.08,1.-(y0+dy0*0.10-0.05),'!8(b)'
   xyouts,/normal,x0+dx0*0.25,1.-(y0+dy0*0.10-0.05),'!8time = '
   xyouts,/normal,x0+dx0*0.58,1.-(y0+dy0*0.10-0.05), $
     strmid(strcompress(string(ctime(mm2)),/remove_all),0,4)


    barposition=[0.97,0.58,0.99,0.88]
    b_min=-5.0 & b_max=-0.0

  color_bar,/noerase,levels1 $
    ,clr $
   ,position=barposition,b_min=b_min,b_max=b_max

; back to default

!x.range=[0.,0.]
!y.range=[0.,0.]
!x.margin=[10.,3.]
!y.margin=[4.,2.]
!p.charsize=1.
!p.thick=1.
!p.multi=0

if (plot_choice ne 'x') then begin
;   device,color=0
   device,/close
;   !p.font=-1
;   set_plot,'x'
endif

;delvar,data1,data2
end

