;
;      To plot 1D slice for 1D test simulations
;      2006.1.20(the)
;
;  Usage:
;
;  idl> .r dread
;  idl> .r vonz1D
;
;
; choice of device
;plot_choice='ps'
plot_choice='x'

iwx=2 & jwy=3

!p.charsize=2.0
!p.charthick=1.5
!p.thick=1.5
;!p.charsize=1.1
;!p.charthick=1.0
;!p.thick=1.0
chsz=2.0
rx=0.6

izon=64
;izon=0 : z=0.0 rs

mm1=6
mm2=11

!x.range=[2.0,30.0]

int=1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
case plot_choice of
'x' : begin
window,4,xs=700,ys=500,retain=2
end
'ps' : begin
set_plot,'ps'
device,/landscape
device,/color,xoffset=0.5,yoffset=26.5,xsize=24,ysize=14,font_size=8
;device,/inches,xsize=7.0,ysize=4.5,yoffset=2.5,xoffset=0.5,font_size=8.5
;!p.font=0 & device,/times,/italic
end
endcase

iw=iwx*jwy
dx0=1./iwx
dy0=1./jwy

!p.multi=[0,iwx,jwy]
!y.margin=[4.,5.]
!x.margin=[6.,6.]

;
;(1) density
;
;

;ldmyct,clr_index=clr,white=white,black=black
;loadct,5

!y.range=[-0.1,1.1]

plot,xar,dd(*,izon,mm1),title='!7q!8',xtitle='x',xstyle=1,ystyle=1,linestyle=0
oplot,xar,dd(*,izon,mm2),linestyle=1
;oplot,xar,dd(*,izon,1),linestyle=2

i=0
	x0=dx0*(i mod iwx)
	y0=dy0*((i-(i mod iwx))/iwx)
   xyouts,/normal,x0+dx0*0.15,1.-(y0+dy0*0.20-0.03),'time = '
   xyouts,/normal,x0+dx0*0.50,1.-(y0+dy0*0.20-0.03), $
     strmid(strcompress(string(ctime(mm1)),/remove_all),0,4)
;
; (2) pressure
;

!y.range=[-0.0001,0.006]
;
plot,xar,pp(*,izon,mm1),title='p',xtitle='x',xstyle=1,ystyle=1,linestyle=0
oplot,xar,pp(*,izon,mm2),linestyle=1
oplot,xar,pp(*,izon,1),linestyle=2
;
;
; (3) Vx
;

!y.range=[-0.05,1.0]

plot,xar,-v1(*,izon,mm1),title='v!Dx!N',xtitle='x',xstyle=1,ystyle=1,linestyle=0
oplot,xar,-v1(*,izon,mm2),linestyle=2
oplot,xar,-v1(*,izon,1),linestyle=1

;
; (3) Vphi
;

!y.range=[-0.2,0.2]

plot,xar,v3(*,izon,mm1),title='v!Dy!N',xtitle='x',xstyle=1,ystyle=1,linestyle=0
oplot,xar,v3(*,izon,mm2),linestyle=2
oplot,xar,v3(*,izon,1),linestyle=1
;
;
;
;
; (4) Bx
;
;
!y.range=[-0.001,0.001]
;
plot,xar,b2(*,izon,mm1),title='B!Dx!N',xtitle='x',xstyle=1,linestyle=0
oplot,xar,b2(*,izon,mm2),linestyle=1
oplot,xar,b2(*,izon,1),linestyle=2
;oplot,xar,b1(*,izon,1),linestyle=3
;

if (plot_choice ne 'x') then begin
;   device,color=0
   device,/close
;   !p.font=-1
;   set_plot,'x'
endif
end


