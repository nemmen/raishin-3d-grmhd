!--------------------------------------------------------------------
subroutine mdtorus4(uri,gcov,gcon, &
                    x1,x2,x3,x1b,x2b,x3b,dx1,dx2,dx3, &
                    is1,ie1,js1,je1,ks1,ke1)
!--------------------------------------------------------------------
!  constant-l rotating torus model with toroidal magnetic field 
!  Komissarov 2006, MNRAS, 368, 993
!
  use pram, only : imax, jmax, kmax, nv, gam, metric, akm, pi, iter, kpol
  implicit none
  include 'mpif.h'

  integer :: i, j, k, m, n, is1 ,ie1, js1, je1, ks1, ke1
  integer :: merr

  real(8) :: uri(nv,is1:ie1,js1:je1,ks1:ke1)
  
  real(8) :: gcov(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: gcon(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)
!
  real(8), allocatable :: gcov1(:,:), gcon1(:,:), gcov1cen(:,:)
  real(8), allocatable :: util(:), utiln(:), bcon(:)
  real(8), allocatable :: ucov(:), ucon(:), beta1(:)
!
  real(8) :: x1(imax), x2(jmax), x3(kmax) 
  real(8) :: x1b(imax), x2b(jmax), x3b(kmax) 
  real(8) :: dx1(imax), dx2(jmax), dx3(kmax)
!
  real(8) :: de, pr, pm, ut, wp, ah
  real(8) :: rin, win, rcen, thcen, ahcen, wcen, clm, betacen, &
             thedge, thedge1, rin1, rhomin, prmin, polkp, polkm
  real(8) :: rr, th, sth, cth, del, aa, sig, &
       sthcen, cthcen, delcen, aacen, sigcen
  real(8) :: bb, aa1, bbcen, omg, uut, ucovph
  real(8) :: tmp1a, tmp1b, tmp_rand
  real(8) :: x1aa, x2aa, x3aa, x3t
  real(8) :: tmp4a, tmp4b, tmp4c, discr, alpha1, gfl
! 
!- allocate variables -!
  allocate(gcov1(0:3,0:3), gcon1(0:3,0:3), gcov1cen(0:3,0:3), stat=merr)
  allocate(util(1:3), utiln(1:3), bcon(1:3), ucov(0:3), ucon(0:3), &
           beta1(1:3), stat=merr)

!--------------------------------------------------------------------
!- Parameter -!
!- we assume polytropic index for gas pressure and mag pressure is same -!  

  rin=1.58d0     !- radial position at cusp of torus
  win=0.030d0    !- W at surface of torus
  rcen=4.62d0    !- radial position at center of torus
  thcen=pi/2.d0  !- \theta position at center of torus
  ahcen=1.1d0     !- specific enthalpy at center of torus
  wcen=0.103d0   !- W at center of torus
  clm=2.8d0      !- angular momentum
  betacen=0.1d0  !- plasma beta at center of torus (\beta=p_gas/p_mag)
!
  thedge=0.1d0
  thedge1=pi-0.1d0
!  
  rin1=1.d0
  rhomin=1.d-7    !- minimum density at atmosphere (corona) 
  prmin=1.d-3*rhomin !-minimum pressure at atmosphere (corona)
!
!- initally set minimum plasma everywhere -!
  do k=ks1,ke1
    do j=js1,je1
      do i=is1,ie1

        do m=0,3
          do n=0,3
            gcon1(m,n)=gcon(m,n,i,j,k)
          enddo
        enddo

        rr=x1(i)

        de=rhomin
!        de=rhomin*exp(-3.*rr/rcen)
!        de=rhomin*((rr/rin1)**(-1.5d0))

!        pr=(gam-1.d0)*prmin*((rr/rin1)**(-2.5d0))
!        pr=kpol*(de**gam)
!
        pr=prmin
        util(1)=0.d0
!        util(1)=(gcon1(0,1)/gcon1(0,0))*(1.-(1./rr)**4)
        util(2)=0.d0
        util(3)=0.d0
        bcon(1)=0.d0
        bcon(2)=0.d0 
        bcon(3)=0.d0
!          
!- set primitive variables -!
!          
        uri(1,i,j,k)=de
        uri(2,i,j,k)=util(1)
        uri(3,i,j,k)=util(2)
        uri(4,i,j,k)=util(3)
        uri(5,i,j,k)=pr
        uri(6,i,j,k)=0.d0
        uri(7,i,j,k)=bcon(1)
        uri(8,i,j,k)=bcon(2)          
        uri(9,i,j,k)=bcon(3)
!       
      enddo
    enddo
  enddo
!  
!- calculation of BL Kerr metric at rcen, thcen -!
! 
  sthcen=sin(thcen)
  cthcen=cos(thcen)
  delcen=rcen*rcen-2.*rcen+akm*akm
  aacen=(rcen*rcen+akm*akm)*(rcen*rcen+akm*akm)-delcen*akm*akm*sthcen*sthcen
  sigcen=rcen*rcen+akm*akm*cthcen*cthcen  
!
  do n=0,3
    do m=0,3
      gcov1cen(m,n)=0.d0
    enddo
  enddo

  gcov1cen(0,0)=-(sigcen-2.*rcen)/sigcen               !- g_tt -!
  gcov1cen(1,1)=sigcen/delcen                          !- g_rr -!
  gcov1cen(2,2)=(aacen/sigcen)*sthcen*sthcen !- g_phi phi -!
  gcov1cen(0,2)=-(2.*akm*rcen*sthcen*sthcen)/sigcen  !- g_t phi -!
  gcov1cen(2,0)=gcov1cen(0,2)                  !- g_phi t -!
  gcov1cen(3,3)=sigcen                      !- g_th th -!  
!
!- calculate plokp and polkm
!- (polytropic constant for gas and mag pressure)
!
  bbcen=gcov1cen(0,2)*gcov1cen(0,2)-gcov1cen(0,0)*gcov1cen(2,2)
  plokp=(ahcen**(1.-gam))*(wcen-win)*(gam-1./gam)*(betacen/(1.+betacen))
  plokm=(bbcen**(1.-gam))*pokp/betacen  
!
!- calculate w at each grid point -!
!
  do k=ks1,ke1
    do j=js1,je1
      do i=is1,ie1
!
        rr=x1(i)
        th=x3(k)
        sth=sin(th)
        cth=cos(th) 
!
        if(rr .ge. rin) then         
!        if(rr .ge. rin .and. th .ge. thedge .and. th .le. thedge1) then
!- calculation of BL Kerr metric -! 
          del=(rr*rr)-(2.*rr)+(akm*akm)
          aa=(rr*rr+akm*akm)*(rr*rr+akm*akm)-del*akm*akm*sth*sth
          sig=rr*rr+akm*akm*cth*cth
!
          do n=0,3
            do m=0,3
              gcov1(m,n)=0.d0
              gcon1(m,n)=0.d0 
            enddo
          enddo
!
          gcov1(0,0)=-(sig-2.*rr)/sig               !- g^tt -!
          gcov1(1,1)=sig/del                          !- g^rr -!
          gcov1(2,2)=(aa/sig)*sth*sth             !- g^phi phi -!
          gcov1(0,2)=-(2.*akm*rr*sth*sth)/sig           !- g^t phi -!
          gcov1(2,0)=gcov1(0,2)                  !- g^phi t -!
          gcov1(3,3)=sig                      !- g^th th -!        
!
          gcon1(0,0)=-aa/(sig*del)               !- g^tt -!
          gcon1(1,1)=del/sig                          !- g^rr -!
          gcon1(2,2)=(sig-2.*rr)/(del*sig*sth*sth) !- g^phi phi -!
          gcon1(0,2)=-(2.*akm*rr)/(sig*del)           !- g^t phi -!
          gcon1(2,0)=gcon1(0,2)                  !- g^phi t -!
          gcon1(3,3)=1./sig 
!          
!- calculation of u_t at r,theta -!
!          
          bb=gcov1(0,2)*gcov1(0,2)-gcov1(0,0)*gcov1(2,2)
          aa1=gcov1(0,0)*clm*clm+2.*clm*gcov1(0,2)+gcov1(2,2)
          if(aa1 .gt. 0.d0) then
            ut=-sqrt(bb/aa1)
          else
            ut=-1.d0  
          endif        
!
!- calculation of W from u_t
!          
          wp=log(abs(ut))
!
!- calculation of h
!
          
          tmp1a=(wp-win)/(wcen-win)
          tmp1b=(polkp+polkm*bbcen**(gam-1.))/(polkp+polkm*bb**(gam-1.))
          ah=ahcen*(tmp1a*tmp1b)**(1./gam-1.)          
!
        else
          ah=-1.d0
        endif
!
!- set torus structure -!
!
        if(ah .gt. 0.d0 .and. rr .ge. rin  &
           .and. th .ge. thedge .and. th .le. thedge1) then    
          
          pr=polkp*ah**gam
          pm=polkm*(bb**(gam-1.))*(roh**gam)
          de=ah-pr
          bcon(1)=0.d0
          bcon(2)=sqrt(2.*pm)
          bcon(3)=0.d0
!
!         if(j .eq. 4 .and. k .eq. 32) then
!           write(*,*) 'ah, de=', ah, de, i
!         endif
!
          util(1)=0.d0
          util(3)=0.d0
!
!- calculation of omega & contravariant u^t
!          
          omg=-(gcov1(0,2)+gcov1(0,0)*clm)/(gcov1(2,2)+gcov1(0,2)*clm)          
!          uut=sqrt(-1./(gcov1(0,0)+2.*omg*gcov1(0,2)+omg*omg*gcov1(2,2)))
          uut=-1./(ut*(1.-omg*clm))
!
          ucovph=-omg*ut

          util(2)=(omg*uut)
!          util(2)=gcon1(0,2)*ut+gcon1(2,2)*ucovph
!          util(2)=0.d0
!
!- Put perturbation in pressure -!
!
!         call random_number(tmp_rand)
!!          write(*,*) "tmp_rand=", tmp_rand-0.5
!           pr=pr*(1.+0.05*(tmp_rand-0.5))
!           util(1)=util(1)+0.01*(tmp_rand-0.5)             
!
!- transform velocity from BL to KS coordinates -!
!
          if(metric .eq. 303 .or. metric .eq. 403) then
            x1aa=x1(i)
            x2aa=x2(j)
            x3aa=x3(k)   
            x3t=x3b(k)
            call transbl2ks(util,utiln,x1aa,x2aa,x3aa,x3t)
            util(1)=utiln(1)
            util(2)=utiln(2)
            util(3)=utiln(3)
!
          elseif(metric .eq. 203 .and. akm .ne. 0.d0) then
!
            do m=0,3
              do n=0,3
                gcov1(m,n)=gcov(m,n,i,j,k)
                gcon1(m,n)=gcon(m,n,i,j,k)
              enddo
            enddo
!
            alpha1=1./sqrt(-gcon1(0,0))
!
            beta1(1)=alpha1*alpha1*gcon1(0,1)
            beta1(2)=alpha1*alpha1*gcon1(0,2)
            beta1(3)=alpha1*alpha1*gcon1(0,3)
!
            ucon(1)=util(1)
            ucon(2)=util(2)
            ucon(3)=util(3)
!
            tmp4a=gcov1(0,0)
            tmp4b=2.*(gcov1(0,1)*ucon(1)+gcov1(0,2)*ucon(2)+gcov1(0,3)*ucon(3))
            tmp4c=1.+gcov1(1,1)*ucon(1)*ucon(1)+gcov1(2,2)*ucon(2)*ucon(2) &    
                  +gcov1(3,3)*ucon(3)*ucon(3) &
                  +2.*(gcov1(1,2)*ucon(1)*ucon(2)+gcov1(1,3)*ucon(1)*ucon(3) &  
                  +gcov1(2,3)*ucon(2)*ucon(3))   
            discr=tmp4b*tmp4b-4.*tmp4a*tmp4c
            ucon(0)=(-tmp4b-sqrt(discr))/(2.*tmp4a)
! 
            gfl=ucon(0)*alpha1
!
            util(1)=ucon(1)+gfl*beta1(1)/alpha1
            util(2)=ucon(2)+gfl*beta1(2)/alpha1
            util(3)=ucon(3)+gfl*beta1(3)/alpha1  
!
          endif
!
!          
!- set primitive variables -!
!          
          uri(1,i,j,k)=de
          uri(2,i,j,k)=util(1)
          uri(3,i,j,k)=util(2)
          uri(4,i,j,k)=util(3)
          uri(5,i,j,k)=pr
          uri(7,i,j,k)=bcon(1)
          uri(8,i,j,k)=bcon(2)
          uri(9,i,j,k)=bcon(3)
!          
        endif
!
      enddo
    enddo
  enddo
!
  deallocate(gcov1, gcon1, gcov1cen, util, utiln, bcon, ucov, ucon, &
             beta1, stat=merr)
!
  return
end subroutine mdtorus4
!
