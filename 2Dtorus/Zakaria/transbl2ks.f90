!--------------------------------------------------------------------
subroutine transbl2ks(util,utiln,x1aa,x2aa,x3aa,x3t)
!--------------------------------------------------------------------
!- convert BL 4-velocity to 4-velocity in Kerr-Schild coordinates -!
!
  use pram, only : metric, akm, ix1, ix2, ix3, R0, hslope, pi
  implicit none
!
!- vcov: covariant 4-vector -!
!- vcon: contravariant 4-vector -!
!- gcon1: contravariant metric -! 
!
!- input:  util (u^i+\gamma*\beta^i/\alpha) in BL coordinates
!- output: utiln (u^i+\gamma*\beta^i/\alpha) in KS coordinates 
  
  integer :: m, n, merr
! 
  real(8) :: util(1:3), utiln(1:3) 
!
  real(8), allocatable :: ucon(:), ucon1(:)
  real(8), allocatable :: gcov1(:,:), gcon1(:,:), beta1(:)
  real(8), allocatable :: tmp1(:), trans(:,:)
!
  real(8) :: x1aa, x2aa, x3aa, rr, aa, bb, cc, discr, alpha1, gfl, x3t
!

  allocate(ucon(0:3), ucon1(0:3), gcov1(0:3,0:3), gcon1(0:3,0:3), &
           beta1(1:3), tmp1(0:3), trans(0:3,0:3), stat=merr)
!
  rr=x1aa
!
!- get BL kerr metric (covariant metric) -!
  call kercov1(gcov1,x1aa,x2aa,x3aa)
!
!- contravariant 4-velocity in BL coordinates -!
  ucon(1)=util(1)
  ucon(2)=util(2)
  ucon(3)=util(3)
!
  aa=gcov1(0,0)
  bb=2.*(gcov1(0,1)*ucon(1)+gcov1(0,2)*ucon(2)+gcov1(0,3)*ucon(3))
  cc=1.+gcov1(1,1)*ucon(1)*ucon(1)+gcov1(2,2)*ucon(2)*ucon(2) &    
        +gcov1(3,3)*ucon(3)*ucon(3) &
        +2.*(gcov1(1,2)*ucon(1)*ucon(2)+gcov1(1,3)*ucon(1)*ucon(3) &  
        +gcov1(2,3)*ucon(2)*ucon(3))   
  discr=bb*bb-4.*aa*cc
  ucon(0)=(-bb-sqrt(discr))/(2.*aa)
!
!- make transform matrix -!
  do m=0,3
    do n=0,3
      trans(m,n)=0.d0
    enddo
  enddo
  trans(0,0)=1.d0
  trans(0,1)=2.*rr/(rr*rr -(2.*rr) +akm*akm)
  trans(1,1)=1.d0 
  trans(2,2)=1.d0
  trans(2,1)=akm/(rr*rr -(2.*rr) +akm*akm)
  trans(3,3)=1.d0
!
!- initialize -!
  do m=0,3
    tmp1(m)=0.d0
    ucon1(m)=0.d0
  enddo
!- conversion BL => KS -!
  do n=0,3
    tmp1(0)=tmp1(0)+trans(0,n)*ucon(n)
    tmp1(1)=tmp1(1)+trans(1,n)*ucon(n)
    tmp1(2)=tmp1(2)+trans(2,n)*ucon(n)
    tmp1(3)=tmp1(3)+trans(3,n)*ucon(n)
  enddo
!
!- store contravariant 4-velocity in KS coordinates -!
  do m=0,3
    ucon1(m)=tmp1(m)
  enddo
!- KS => modified KS coords (if need)-!
  if(metric .eq. 403) then
    if(ix1 .eq. 2) then
      ucon1(1)=ucon1(1)*(1./(rr-R0))
    endif
    if(ix3 .eq. 2) then
      ucon1(3)=ucon1(3)*(1./(pi+(1.-hslope)*pi*cos(2.*pi*x3t)))
    endif
  endif
!  
!- get Kerr-Schild metric (contravariant metric) -!
  if(metric .eq. 303) then
    call kshcon1(gcon1,x1aa,x2aa,x3aa)
  elseif(metric .eq. 403) then
    call mkshcov1(gcov1,x1aa,x2aa,x3aa,x3t)
    call invert_matrix(gcov1,gcon1,4)
  endif
! 
  alpha1=1./sqrt(-gcon1(0,0))
  gfl=ucon1(0)*alpha1
!
  beta1(1)=alpha1*alpha1*gcon1(0,1)
  beta1(2)=alpha1*alpha1*gcon1(0,2)
  beta1(3)=alpha1*alpha1*gcon1(0,3)
!
!- calculation new util^i = u^i+\gamma*\beta^i/\alpha
!  
  utiln(1)=ucon1(1)+gfl*beta1(1)/alpha1
  utiln(2)=ucon1(2)+gfl*beta1(2)/alpha1
  utiln(3)=ucon1(3)+gfl*beta1(3)/alpha1

  deallocate(ucon, ucon1, gcov1, gcon1, beta1, tmp1, trans, stat=merr)
!
  return
end subroutine transbl2ks
!--------------------------------------------------------------------
subroutine transks2bl(uri,uria,gcov,gcon,x1,x2,x3,is1,ie1,js1,je1,ks1,ke1)
!--------------------------------------------------------------------
!- convert KS 4-velocity to 4-velocity in Boyer-Lindquist coordinates -!
!  
!- input:  util (u^i+\gamma*\beta^i/\alpha) in KS coordinates
!- output: utiln (u^i+\gamma*\beta^i/\alpha) in BL coordinates
!  
  use pram, only : imax, jmax, kmax, nv, akm  
  implicit none

  integer :: i, j, k, m, n, is1, ie1, js1, je1, ks1, ke1 
  integer :: merr
       
  real(8) :: uri(nv,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: uria(nv,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: gcov(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: gcon(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)

  real(8), allocatable :: gcov1(:,:), gcon1(:,:), gcon1a(:,:), beta1(:)
  real(8), allocatable :: util(:), utiln(:)
  real(8), allocatable :: ucon(:), ucon1(:)
  real(8), allocatable :: tmp1(:), trans(:,:)

  real(8) :: x1(imax), x2(jmax), x3(kmax)      

  real(8) :: x1aa, x2aa, x3aa, rr, rbh1, alpha1, gfl
!
!- allocate variables -!
  allocate(gcov1(0:3,0:3), gcon1(0:3,0:3), gcon1a(0:3,0:3), beta1(1:3), &
           util(1:3), utiln(1:3), ucon(0:3), ucon1(0:3), &
           tmp1(0:3), trans(0:3,0:3), stat=merr)
!
!=====================================================================
!
  do k=ks1,ke1
    do j=js1,je1
      do i=is1,ie1

        rbh1=1.+sqrt(1.-akm*akm)
        rr=x1(i)
!
        if(rr .gt. rbh1) then
          util(1)=uri(2,i,j,k) !- util^i=u^i+\gamma*\beta^i/\alpha
          util(2)=uri(3,i,j,k)
          util(3)=uri(4,i,j,k)
!
!          v1=uri(2,i,j,k)
!          v2=uri(3,i,j,k)
!          v3=uri(4,i,j,k)

!
          do m=0,3
            do n=0,3
              gcov1(m,n)=gcov(m,n,i,j,k)
              gcon1(m,n)=gcon(m,n,i,j,k)
            enddo
          enddo
!
!- calculation of contravariant 4-velocity in KS coordinates -!        
          call calgfl(util,gfl,gcov1)
          call calucon(util,ucon,gfl,gcon1)
!
!- make transform matrix -!
!
          do m=0,3
            do n=0,3
              trans(m,n)=0.d0
            enddo
          enddo
          trans(0,0)=1.d0
          trans(0,1)=-2.*rr/(rr*rr -(2.*rr) +akm*akm)
          trans(1,1)=1.d0 
          trans(2,2)=1.d0
          trans(2,1)=-akm/(rr*rr -(2.*rr) +akm*akm)
          trans(3,3)=1.d0
!
!- initialize -!
          do m=0,3
            tmp1(m)=0.d0
            ucon1(m)=0.d0
          enddo
          do n=0,3
            tmp1(0)=tmp1(0)+trans(0,n)*ucon(n)
            tmp1(1)=tmp1(1)+trans(1,n)*ucon(n)
            tmp1(2)=tmp1(2)+trans(2,n)*ucon(n)
            tmp1(3)=tmp1(3)+trans(3,n)*ucon(n)
          enddo
!
!- contravariant velocity in BL coordinates -!
          do m=0,3
            ucon1(m)=tmp1(m)
          enddo
!
!- get Boyer-Lindquist metric (contravariant metric) -!
          x1aa=x1(i)
          x2aa=x2(j)
          x3aa=x3(k)
!
!          if(x1aa .le. rbh1) then
!            call kshcon1(gcon1a,x1aa,x2aa,x3aa)
!          else
            call kercon1(gcon1a,x1aa,x2aa,x3aa)
!          endif
!
          alpha1=1./sqrt(-gcon1a(0,0))
          gfl=ucon1(0)*alpha1
!
          beta1(1)=alpha1*alpha1*gcon1a(0,1)
          beta1(2)=alpha1*alpha1*gcon1a(0,2)
          beta1(3)=alpha1*alpha1*gcon1a(0,3)
!
!- calculation new u^tilda = u^i + \gamma*\beta^i/\alpha
! 
          utiln(1)=ucon1(1)+gfl*beta1(1)/alpha1
          utiln(2)=ucon1(2)+gfl*beta1(2)/alpha1
          utiln(3)=ucon1(3)+gfl*beta1(3)/alpha1
!
        else
          utiln(1)=0.d0
          utiln(2)=0.d0
          utiln(3)=0.d0
        endif
!
!- copy new primitive variables array -!
        uria(1,i,j,k)=uri(1,i,j,k)
        uria(2,i,j,k)=utiln(1)
        uria(3,i,j,k)=utiln(2)
        uria(4,i,j,k)=utiln(3)
        uria(5,i,j,k)=uri(5,i,j,k)
        uria(6,i,j,k)=uri(6,i,j,k)
        uria(7,i,j,k)=uri(7,i,j,k)
        uria(8,i,j,k)=uri(8,i,j,k)
        uria(9,i,j,k)=uri(9,i,j,k)  
!
      enddo
    enddo
  enddo
!
  deallocate( gcov1, gcon1, gcon1a, beta1, util, utiln, ucon, ucon1, &
              tmp1, trans, stat=merr)
      
  return
end subroutine transks2bl
!
