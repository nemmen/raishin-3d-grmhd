!***********************************************************************
!      RAISHIN code: 3D Special Relativistic MHD (3D MPI) version 
!      program for output to VTK format (output in Cartesian cooddinates)
!      written by Y. Mizuno
!      ver. 150223
!***********************************************************************
!
program convert_vtk2d
  use pram, only: imax, jmax, kmax, metric

  implicit none

!======================================================================@
!    Difinition for variables
!======================================================================@
!
  integer :: i, j, k, l, m, n, mh, jh
  integer :: mmax
  character*256 :: filename 
!
  integer, parameter :: nh=101 !- total output file number -!
  integer, parameter :: ns=1 !- start (reading) output file number -!
  integer, parameter :: nq=13
  integer, parameter :: dataformat=0 !- 0= ascii, 1=binary -!
!
  character(LEN=1), parameter :: newline = achar(10) ! newline symbol!
!
  real(8) :: qq(nq,imax,jmax,kmax)
  real(4) :: xx(imax,kmax), yy(imax,kmax), zz(imax,kmax)
  real(4) :: dd(imax,kmax), pp(imax,kmax), ink(imax,kmax)  
  real(4) :: ux(imax,kmax), uy(imax,kmax), uz(imax,kmax) 
  real(4) :: bx(imax,kmax), by(imax,kmax), bz(imax,kmax) 
!
  real(4) :: dum_r
  integer :: it, nt0, nt, time
!
!
!======================================================================@
!     File Open
!======================================================================@
!
  open(unit=6,file='convert.outdat',status='unknown')
  open(unit=8,file='structr.outdat',status='unknown',form='unformatted')
!
!======================================================================@
!- initialize -!
!
  nt0=0
  nt=0
!
!- read output file setting information -!
!
  write(6,*) 'number of output data:', nh
  write(6,*) 'number of start data:', ns
  write(6,*) 'dataformat (0:ascii, 1:binary):', dataformat
!  
!
!**********************************************************************@
!     Main Loop1 Start 
!**********************************************************************@
!
  do mh=1,nh

!======================================================================@
!     Read data from structr.outdat
!======================================================================@
!
    nt0=nt
!
    nt=ns+(mh-1)
    write(6,*) ' nt:',nt
!
!-  Initialize of reading array -!
!
    do k=1,kmax
      do j=1,jmax
        do i=1,imax
          do n=1,nq
            qq(n,i,j,k)=0.d0
          enddo
        enddo
      enddo
    enddo

!- read data -
    if( nt.lt.nt0 ) then
! 
      rewind(8)
!
      do it=1,nt
!
        read(8,end=140) time
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              read(8,end=142) (qq(n,i,j,k),n=1,nq)
            enddo
          enddo
        enddo
!
      enddo
!
    elseif( nt.gt.nt0 ) then
!
      do it=1,nt-nt0
! 
       read(8,end=140) time
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              read(8,end=142) (qq(n,i,j,k),n=1,nq)
            enddo
          enddo
        enddo
!
      enddo

    endif
!
    go to 164
! 
 142 continue
     write(6,*) 'file end found during reading ONE PROFILE'
     write(6,*) ' it =',it,' i =',i,' j =',j,' k =',k
     write(6,*) 'STOP at MAIN'
     stop
!
 140 continue
     write(6,*) 'file end found during reading'
     write(6,*) ' it =',it,' i =',i,' j =',j,' k =',k
     write(6,*) 'continue process'
! 
 164 continue
!
!----------------------------------------------------------------------@
!- Output data for analysis -!
!
!- set output file -!
    write(filename,990) mh-1
    open( unit=9, file=filename,status='replace',form="formatted")
990 format('ok',i3.3,'.vtk')  
!
!- set half grid position -!
    jh=(jmax+1)/2
!- set j-th direction =1 -!
    mmax=1    
!
!- convert the data array -!
!    
    do k=1,kmax
      do i=1,imax
        xx(i,k)=qq(1,i,jh,k)*sin(qq(3,i,jh,k))
        yy(i,k)=qq(1,i,jh,k)*cos(qq(3,i,jh,k))
        zz(i,k)=0.0

        dd(i,k)=qq(4,i,jh,k) !- density -!
        ux(i,k)=qq(5,i,jh,k)*sin(qq(3,i,jh,k)) &
               +qq(7,i,jh,k)*cos(qq(3,i,jh,k)) !- 1st component of velocity -!
        uy(i,k)=qq(5,i,jh,k)*cos(qq(3,i,jh,k)) &
               -qq(7,i,jh,k)*sin(qq(3,i,jh,k)) !- 2nd component of velocity -!
!        uz(i,k)=qq(6,i,jh,k) !- 3rd component of velocity -!
        uz(i,k)=0.d0
        pp(i,k)=qq(8,i,jh,k) !- gas pressure -!
        ink(i,k)=qq(9,i,jh,k) !- tracer scalar -!          
        bx(i,k)=qq(10,i,jh,k)*sin(qq(3,i,jh,k)) &
               +qq(12,i,jh,k)*cos(qq(3,i,jh,k)) !- 1st component of magnetic field -!
        by(i,k)=qq(10,i,jh,k)*cos(qq(3,i,jh,k)) &
               -qq(12,i,jh,k)*sin(qq(3,i,jh,k)) !- 2nd component of magnetic field -!
        bz(i,k)=qq(11,i,jh,k) !- 3rd component of magnetic field -!          
      enddo
    enddo
!   
!- write data for VTK format -!
!  Now the data must be cartesian coordinates!
!  The gird points does not need to be regular.!

!  There are five basic parts to the VTK file format.  
!  1. Write file version and identifier (Header)
!    
    write(9, "(a)") "# vtk DataFile Version 2.0"    
!
!  2. Tilte
!    
    write(9, "(a)") "RMHD Simulation Result"    
!
!  3. Data type (ASCII or BINARY)
!    
    if(dataformat .eq. 0) then
      write(9, "(a)") "ASCII" 
    else
      write(9, "(a)") "BINARY"
    endif
!
!  4.  Dataset structure 
!
10  format(a11, 3(1x, i6))
11  format(a, 2x, i9, 2x, a5)
    write(9, "(a)") "DATASET STRUCTURED_GRID"
    write(9, 10)    "DIMENSIONS ", imax, kmax, mmax
    write(9, 11)    "POINTS", imax*kmax*mmax, "float"
!
    write(6,*) 'after header: mh=', mh
!
! -- now dumps coordnates
    if(dataformat .ne. 0) then
      close(9)
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")
!           position="append", access="stream")
    endif
! -- now dumps data
    do k = 1, kmax
      do i = 1, imax
        if(dataformat .eq. 0) then                 
          write(9,*) xx(i,k), yy(i,k), zz(i,k)
        else
          write(9) xx(i,k), yy(i,k), zz(i,k)
        endif
      enddo
    enddo
!
    write(6,*) 'after coordinates: mh=', mh
!    
!  5 . Data
!    
    if(dataformat .ne. 0) then
      close(9)
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")          
    endif
!
12  format(a, a11, 1x, i12)
! You need to add newline character for safty after writing data in binary
!   write(9, "(a)") 
    write(9, 12) newline, "POINT_DATA ", imax*kmax*mmax
!
    write(6,*) 'after new line: mh=', mh   
!
! Write density
!
    write(9, "(a)") "SCALARS density float"
    write(9, "(a)") "LOOKUP_TABLE default"
!
! --- now dumps data
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")  
    endif
    if(dataformat .eq. 0) then
      do k = 1, kmax
        do i = 1, imax             
          dum_r = max(dd(i,k), 1e-30) ! for safety
          write(9,*) dum_r
        enddo
      enddo
    else
      do k = 1, kmax
        do i = 1, imax
          dum_r = max(dd(i,k), 1e-30) ! for safety
          write(9) dum_r
        enddo
      enddo
    endif
!
    write(6,*) 'after density: mh=', mh  
!
! Write velocity
!
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")   
    endif
    
    write(9, "(a, a)") newline, "VECTORS velocity float"
    
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")   
    endif
! -- now dumps data
    if(dataformat .eq. 0) then
      do k = 1, kmax
        do i = 1, imax
          write(9,*) ux(i,k), uy(i,k), uz(i,k)
        enddo           
      enddo
    else
      do k = 1, kmax
        do i = 1, imax
          write(9) ux(i,k), uy(i,k), uz(i,k)
        enddo
      enddo           
    endif
!
    write(6,*) 'after velocity vector: mh=', mh  
!
! Write B-field vector
!
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")   
    endif
    
    write(9, "(a, a)") newline, "VECTORS B-field float"
    
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
 !          position="append", access="stream") 
    endif
! -- now dumps data
    if(dataformat .eq. 0) then
      do k = 1, kmax
        do i = 1, imax
          write(9,*) bx(i,k), by(i,k), bz(i,k)
        enddo           
      enddo
    else
      do k = 1, kmax
        do i = 1, imax
          write(9) bx(i,k), by(i,k), bz(i,k)
        enddo
      enddo           
    endif
!
    write(6,*) 'end of the data: mh=', mh  
!
    close(9)
      
  enddo    

  stop    
end program convert_vtk2d
