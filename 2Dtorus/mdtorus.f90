!--------------------------------------------------------------------
subroutine mdtorus(uri,gcov,gcon,detg, &
                   x1,x2,x3,x1b,x2b,x3b,dx1,dx2,dx3, &
                   myrank,is1,ie1,js1,je1,ks1,ke1)
!--------------------------------------------------------------------
!  constant-l rotating torus model
!  Font & Daigne 2002, MNRAS, 334, 383
!
  use pram, only : imax, jmax, kmax, nv, gam, metric, akm, pi, iter, kpol
  implicit none
  include 'mpif.h'

  integer :: i, j, k, m, n, is1 ,ie1, js1, je1, ks1, ke1
  integer :: merr, myrank, jh, nnn

  real(8) :: uri(nv,is1:ie1,js1:je1,ks1:ke1)
  
  real(8) :: gcov(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: gcon(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)
!
  real(8) :: detg(is1:ie1,js1:je1,ks1:ke1)
!
  real(8), allocatable :: gcov1(:,:), gcon1(:,:), gcov1in(:,:)
  real(8), allocatable :: ucov(:), ucon(:), bbcov(:), bbcon(:), beta1(:)
  real(8), allocatable :: util(:), utiln(:), bcon(:)
!
  real(8) :: x1(imax), x2(jmax), x3(kmax) 
  real(8) :: x1b(imax), x2b(jmax), x3b(kmax) 
  real(8) :: dx1(imax), dx2(jmax), dx3(kmax)
!
  real(8),allocatable :: ah(:,:,:), pom(:,:,:)
!
  real(8) :: de, pr  
  real(8) :: kappa, beta, clm, utin, ut, uut, &
             rhomin, prmin, hm1, omg, thedge, thedge1, rcen, &
             rhomax1, rhomax1a, prmax1, prmax1a, prmax2, prmax2a 
  real(8) :: rr, th, sth, cth, del, aa, sig, rr1, th1, &
             rin, thin, sthin, cthin, delin, aain, sigin, rin1
  real(8) :: x1aa, x2aa, x3aa, x3t
  real(8) :: tmp_rand
  real(8) :: bsq_max, bsq_max1, bsq_max2, bsq_max2a, beta_act, &
             bnorm, pom1, rhoav, gfl, bbsq
  real(8) :: tmp4a, tmp4b, tmp4c, discr, alpha1
  real(8) :: tmp1a, tmp1b, tmp2a, tmp2b
  real(8) :: ucovph
! 
!- allocate variables -!
  allocate(gcov1(0:3,0:3), gcon1(0:3,0:3), gcov1in(0:3,0:3), stat=merr)
  allocate(util(1:3), utiln(1:3), bcon(1:3), ucov(0:3), ucon(0:3), &
           bbcov(0:3), bbcon(0:3), beta1(1:3), stat=merr)
  allocate(ah(is1:ie1,js1:je1,ks1:ke1), pom(is1:ie1,js1:je1,ks1:ke1), &
           stat=merr)

!--------------------------------------------------------------------
!- Parameter -!

  rin=9.34d0
!  rin=9.0d0
  thin=pi/2.d0
  kappa=1.0d-3
  beta=0.d0
!  beta=100.d0
!  beta=0.d0
!  clm=4.5d0 ! a=0.0
!  clm=4.35d0 ! a=0.5
  clm=4.25d0 ! a=0.9
!  
  thedge=0.1d0
  thedge1=pi-0.1d0
  rcen=25.d0
!
!- minimum density & pressure set-up for atmosphere
!  
  rin1=1.d0
  rhomin=1.d-7 
  prmin=1.d-3*rhomin
!
!- initialize for check maximum density, pressure, B-field square  
  rhomax1=0.d0
  rhomax1a=0.d0
  prmax1=0.d0 
  prmax1a=0.d0
  prmax2=0.d0
!  
  bsq_max=0.d0
  bsq_max1=0.d0
  bsq_max2=0.d0
  bsq_max2a=0.d0
!
!- First initally set minimum plasma everywhere -!
  do k=ks1,ke1
    do j=js1,je1
      do i=is1,ie1

        do m=0,3
          do n=0,3
            gcon1(m,n)=gcon(m,n,i,j,k)
          enddo
        enddo

        rr=x1(i)

        de=rhomin
!        de=rhomin*exp(-3.*rr/rcen)
!        de=rhomin*((rr/rin1)**(-1.5d0))

!        pr=(gam-1.d0)*prmin*((rr/rin1)**(-2.5d0))
!        pr=kpol*(de**gam)
!
        pr=prmin
        util(1)=0.d0
!        util(1)=(gcon1(0,1)/gcon1(0,0))*(1.-(1./rr)**4)
        util(2)=0.d0
        util(3)=0.d0
        bcon(1)=0.d0
        bcon(2)=0.d0 
        bcon(3)=0.d0
!          
!- set primitive variables -!
!          
        uri(1,i,j,k)=de      !- density
        uri(2,i,j,k)=util(1) !- util^r=u^r+\gamma*\beta^r/\alpha
        uri(3,i,j,k)=util(2) !- util^\phi=u^\phi+\gamma*\beta^\phi/\alpha
        uri(4,i,j,k)=util(3) !- util^\theta=u^\theta+\gamma*\beta^\theta/\alpha
        uri(5,i,j,k)=pr      !- gas pressure
        uri(6,i,j,k)=0.d0
        uri(7,i,j,k)=bcon(1) !- B^r (3-vector)
        uri(8,i,j,k)=bcon(2) !- B^\phi (3-vector)         
        uri(9,i,j,k)=bcon(3) !- B\theta (3-vector)
!       
      enddo
    enddo
  enddo
!  
!- calculation of BL Kerr metric at rin, thin -!
!  
  sthin=sin(thin)
  cthin=cos(thin)
  delin=rin*rin-2.*rin+akm*akm
  aain=(rin*rin+akm*akm)*(rin*rin+akm*akm)-delin*akm*akm*sthin*sthin
  sigin=rin*rin+akm*akm*cthin*cthin  
!
  do n=0,3
    do m=0,3
      gcov1in(m,n)=0.d0
    enddo
  enddo

  gcov1in(0,0)=-(sigin-2.*rin)/sigin               !- g_tt -!
  gcov1in(1,1)=sigin/delin                          !- g_rr -!
  gcov1in(2,2)=(aain/sigin)*sthin*sthin !- g_phi phi -!
  gcov1in(0,2)=-(2.*akm*rin*sthin*sthin)/sigin  !- g_t phi -!
  gcov1in(2,0)=gcov1in(0,2)                  !- g_phi t -!
  gcov1in(3,3)=sigin                      !- g_th th -!
! 
!- calculation of ut at rin -!
  tmp1a=gcov1in(0,2)*gcov1in(0,2)-gcov1in(0,0)*gcov1in(2,2)
  tmp1b=gcov1in(0,0)*clm*clm+2.*clm*gcov1in(0,2)+gcov1in(2,2)
  if(tmp1b .gt. 0.d0) then
    utin=-sqrt(tmp1a/tmp1b)
  else
    utin=-1.d0  
  endif
!
  if(myrank .eq. 0) then
    write(*,*) 'lin, utin=', clm, utin
  endif
!
!- calculate h -!
!
  do k=ks1,ke1
    do j=js1,je1
      do i=is1,ie1
!
        rr=x1(i)
        th=x3(k)
        sth=sin(th)
        cth=cos(th) 
!
        if(rr .ge. rin) then         
!        if(rr .ge. rin .and. th .ge. thedge .and. th .le. thedge1) then
!- calculation of BL Kerr metric -! 
          del=(rr*rr)-(2.*rr)+(akm*akm)
          aa=(rr*rr+akm*akm)*(rr*rr+akm*akm)-del*akm*akm*sth*sth
          sig=rr*rr+akm*akm*cth*cth
!
          do n=0,3
            do m=0,3
              gcov1(m,n)=0.d0
              gcon1(m,n)=0.d0 
            enddo
          enddo
!
!- covariant metric in Kerr-BL
          gcov1(0,0)=-(sig-2.*rr)/sig               !- g^tt -!
          gcov1(1,1)=sig/del                          !- g^rr -!
          gcov1(2,2)=(aa/sig)*sth*sth             !- g^phi phi -!
          gcov1(0,2)=-(2.*akm*rr*sth*sth)/sig           !- g^t phi -!
          gcov1(2,0)=gcov1(0,2)                  !- g^phi t -!
          gcov1(3,3)=sig                      !- g^th th -!
!          
!- contravariant metric in Kerr-BL
          gcon1(0,0)=-aa/(sig*del)               !- g^tt -!
          gcon1(1,1)=del/sig                          !- g^rr -!
          gcon1(2,2)=(sig-2.*rr)/(del*sig*sth*sth) !- g^phi phi -!
          gcon1(0,2)=-(2.*akm*rr)/(sig*del)           !- g^t phi -!
          gcon1(2,0)=gcon1(0,2)                  !- g^phi t -!
          gcon1(3,3)=1./sig 
!          
!- calculation of u_t at r,theta -!
!          
          tmp2a=gcov1(0,2)*gcov1(0,2)-gcov1(0,0)*gcov1(2,2)
          tmp2b=gcov1(0,0)*clm*clm+2.*clm*gcov1(0,2)+gcov1(2,2)
          if(tmp2b .gt. 0.d0) then
            ut=-sqrt(tmp2a/tmp2b)
          else
            ut=-1.d0  
          endif        
!
!- calculation of h = u_t,in/u_t
!          
          ah(i,j,k)=utin/ut
!
        else
          ah(i,j,k)=-1.d0
        endif
!
!- set torus structure -!
!
        if(ah(i,j,k) .gt. 1.d0 .and. rr .ge. rin  &
           .and. th .ge. thedge .and. th .le. thedge1) then    
!    
          hm1=max((ah(i,j,k) -1.)*(gam-1.)/gam, 0.d0)
          de=(hm1/kpol)**(1./(gam-1.))
          pr=kpol*(hm1/kpol)**(gam/(gam-1.))
!
          util(1)=0.d0
          util(3)=0.d0
!
!- calculation of omega & contravariant u^t
!          
          omg=-(gcov1(0,2)+gcov1(0,0)*clm)/(gcov1(2,2)+gcov1(0,2)*clm)          
          uut=sqrt(-1./(gcov1(0,0)+2.*omg*gcov1(0,2)+omg*omg*gcov1(2,2)))
!
          util(2)=(omg*uut)
!          util(2)=0.d0
!
!- Put perturbation in pressure -!
!
!         call random_number(tmp_rand)
!           pr=pr*(1.+0.05*(tmp_rand-0.5))
!           util(1)=util(1)+0.01*(tmp_rand-0.5)             
!
!- check density & pressure max -!
!
!          if(de .gt. rhomax1) then
!            rhomax1=de
!          endif
!          if(pr .gt. prmax1) then
!            prmax1=pr
!          endif
!
!- transform velocity from BL to KS coordinates -!
!
          if(metric .eq. 303 .or. metric .eq. 403) then !- KS coord
            x1aa=x1(i)
            x2aa=x2(j)
            x3aa=x3(k)   
            x3t=x3b(k)
            call transbl2ks(util,utiln,x1aa,x2aa,x3aa,x3t)
            util(1)=utiln(1)
            util(2)=utiln(2)
            util(3)=utiln(3)
!
          elseif(metric .eq. 203 .and. akm .ne. 0.d0) then !- Kerr-BL coord
!
            do m=0,3
              do n=0,3
                gcov1(m,n)=gcov(m,n,i,j,k) !- given BL-kerr covariant metric
                gcon1(m,n)=gcon(m,n,i,j,k) !- given BL-Kerr contravariant metric
              enddo
            enddo
!
            alpha1=1./sqrt(-gcon1(0,0))
!
            beta1(1)=alpha1*alpha1*gcon1(0,1)
            beta1(2)=alpha1*alpha1*gcon1(0,2)
            beta1(3)=alpha1*alpha1*gcon1(0,3)
!
            ucon(1)=util(1)
            ucon(2)=util(2)
            ucon(3)=util(3)
!
            tmp4a=gcov1(0,0)
            tmp4b=2.*(gcov1(0,1)*ucon(1)+gcov1(0,2)*ucon(2)+gcov1(0,3)*ucon(3))
            tmp4c=1.+gcov1(1,1)*ucon(1)*ucon(1)+gcov1(2,2)*ucon(2)*ucon(2) &    
                  +gcov1(3,3)*ucon(3)*ucon(3) &
                  +2.*(gcov1(1,2)*ucon(1)*ucon(2)+gcov1(1,3)*ucon(1)*ucon(3) &  
                  +gcov1(2,3)*ucon(2)*ucon(3))   
            discr=tmp4b*tmp4b-4.*tmp4a*tmp4c
            ucon(0)=(-tmp4b-sqrt(discr))/(2.*tmp4a)
! 
            gfl=ucon(0)*alpha1
!
            util(1)=ucon(1)+gfl*beta1(1)/alpha1
            util(2)=ucon(2)+gfl*beta1(2)/alpha1
            util(3)=ucon(3)+gfl*beta1(3)/alpha1  
!
          endif
!
!          
!- set primitive variables (ro, utilda^i, p_gas)-!
!          
          uri(1,i,j,k)=de
          uri(2,i,j,k)=util(1)
          uri(3,i,j,k)=util(2)
          uri(4,i,j,k)=util(3)
          uri(5,i,j,k)=pr
!          
        endif
!
      enddo
    enddo
  enddo
!
!  call mpi_barrier(mpi_comm_world,merr)
!  call mpi_allreduce(rhomax1,rhomax1a,1,mpi_double_precision,mpi_max, &
!                     mpi_comm_world,merr)
!  call mpi_allreduce(prmax1,prmax1a,1,mpi_double_precision,mpi_max, &
!                     mpi_comm_world,merr)
!
!  if(myrank .eq. 0) then
!    write(*,*) "rhomax1a, prmax1a=", rhomax1a, prmax1a
!  endif
!
!
!- Normalize the densities and pressure -!
!
!  do k=ks1,ke1
!    do j=js1,je1
!      do i=is1,ie1 
!        rr=x1(i)
!        th=x3(k)
!        if(ah(i,j,k) .gt. 1.d0 .and. rr .ge. rin &
!           .and. th .ge. thedge .and. th .le. thedge1) then
!          de=uri(1,i,j,k)/rhomax1a
!          pr=uri(5,i,j,k)/rhomax1a
!
!          if(de .le. rhomin) then
!            de=rhomin
!          endif
!          if(pr .le. prmin) then
!            pr=prmin
!          endif
!
!          uri(1,i,j,k)=de
!          uri(5,i,j,k)=pr
!        endif
!
!      enddo
!    enddo
!  enddo
!
!   prmax2a=prmax1a/rhomax1a
!   rhomax1a=1.d0
!
!  if(myrank .eq. 0) then
!    write(*,*) 'rhomax1a, prmax2a=', rhomax1a, prmax2a
!  endif
!
!
!- Set vector potential at cell-center (r-theta frame)-!
!
  do k=ks1,ke1
    do j=js1,je1
      do i=is1,ie1
        pom(i,j,k)=0.d0
      enddo
    enddo
  enddo
!
!  do k=ks1+1,ke1-1
!    do j=js1,je1
!      do i=is1+1,ie1-1
!
!         rhoav=0.25*(uri(1,i,j,k)+uri(1,i-1,j,k) &
!                     +uri(1,i,j,k-1)+uri(1,i-1,j,k-1))
!        rhoav=uri(1,i,j,k)
!
!        if(rhoav .ne. 0.d0) then
!          pom1=rhoav/rhomax1a - 0.3
!        else
!          pom1=0.d0
!        endif
!
!
!        if(pom1 .gt. 0.d0) then
!          pom(i,j,k)=pom1
!        endif
!
!      enddo
!    enddo
!  enddo
!
!- Set-up of Magnetic field -!
!  do k=ks1+1,ke1-1
!    do j=js1,je1
!      do i=is1+1,ie1-1
!
!        tmp1a=pom(i,j,k)-pom(i,j,k+1)+pom(i+1,j,k)-pom(i+1,j,k+1)
!        if(tmp1a .ne. 0.d0) then
!          bcon(1)=-tmp1a/(2.*dx3(k)*detg(i,j,k))
!        else
!          bcon(1)=0.d0
!        endif
!!
!        tmp1b=pom(i,j,k)+pom(i,j,k+1)-pom(i+1,j,k)-pom(i+1,j,k+1)
!        if(tmp1b .ne. 0.d0) then
!          bcon(3)=tmp1b/(2.*dx1(i)*detg(i,j,k))
!        else
!          bcon(3)=0.d0
!        endif
!
!        uri(7,i,j,k)=bcon(1)
!        uri(9,i,j,k)=bcon(3)
!
!      enddo
!    enddo
!  enddo
!
!- put boundary data for magnetic field -!
!
!  do j=js1,je1
!    do i=is1,ie1
!      uri(7,i,j,ks1)=uri(7,i,j,ks1+1)
!      uri(7,i,j,ke1)=uri(7,i,j,ke1-1)
!
!      uri(9,i,j,ks1)=uri(9,i,j,ks1+1)
!      uri(9,i,j,ke1)=uri(9,i,j,ke1-1)
!    enddo
!  enddo
!
!  do k=ks1,ke1
!    do j=js1,je1
!      uri(7,is1,j,k)=uri(7,is1+1,j,k)
!      uri(7,ie1,j,k)=uri(7,ie1-1,j,k)
!
!      uri(9,is1,j,k)=uri(9,is1+1,j,k)
!      uri(9,ie1,j,k)=uri(9,ie1-1,j,k)
!    enddo
!  enddo
!
!- calculation maximum magnetic field strength -!
!  do k=ks1,ke1
!    do j=js1,je1
!      do i=is1,ie1
!
!        util(1)=uri(2,i,j,k)
!        util(2)=uri(3,i,j,k)
!        util(3)=uri(4,i,j,k)
!        bcon(1)=uri(7,i,j,k)
!        bcon(2)=uri(8,i,j,k)
!        bcon(3)=uri(9,i,j,k)
!       
!        do m=0,3
!          do n=0,3
!            gcov1(m,n)=gcov(m,n,i,j,k)
!          enddo
!        enddo
!
!        call calgfl(util,gfl,gcov1)
!        call calucon(util,ucon,gfl,gcon1)
!        call lower(ucon,ucov,gcov1)
!        call calbbcon(bcon,bbcon,ucov,ucon)
!        call lower(bbcon,bbcov,gcov1)
!        call calbbsq(bbcon,bbcov,bbsq)
!
!        if(bbsq .gt. bsq_max) then
!          bsq_max=bbsq
!        endif
!
!      enddo
!    enddo
!  enddo    
!
!  call mpi_barrier(mpi_comm_world,merr)
!  call mpi_allreduce(bsq_max,bsq_max1,1,mpi_double_precision,mpi_max, &
!                     mpi_comm_world,merr)
!
!  beta_act=prmax2a/(0.5*bsq_max1)
!
!  if(beta .eq. 0.d0) then
!    bnorm=0.d0
!  else
!    bnorm=sqrt(beta_act/beta)
!  endif
!
!  if(myrank .eq. 0) then
!    write(*,*) "beta_act, bnorm=", beta_act, bnorm
!  endif
!
!- Normalize magnetic field strength -!
!
!  do k=ks1,ke1
!    do j=js1,je1
!      do i=is1,ie1
!
!        bcon(1)=uri(7,i,j,k)*bnorm            
!        bcon(2)=uri(8,i,j,k)*bnorm  
!        bcon(3)=uri(9,i,j,k)*bnorm  
!
!        util(1)=uri(2,i,j,k)
!        util(2)=uri(3,i,j,k)
!        util(3)=uri(4,i,j,k)
!
!        uri(7,i,j,k)=bcon(1)
!        uri(8,i,j,k)=bcon(2)
!        uri(9,i,j,k)=bcon(3)
!
!        do m=0,3
!          do n=0,3
!            gcov1(m,n)=gcov(m,n,i,j,k)
!          enddo
!        enddo        
!
!        call calgfl(util,gfl,gcov1)
!        call calucon(util,ucon,gfl,gcon1)
!        call lower(ucon,ucov,gcov1)
!        call calbbcon(bcon,bbcon,ucov,ucon)
!        call lower(bbcon,bbcov,gcov1)
!        call calbbsq(bbcon,bbcov,bbsq)
!
!        if(bbsq .gt. bsq_max2) then
!          bsq_max2=bbsq
!        endif
!
!      enddo
!    enddo
!  enddo 
!
!  call mpi_barrier(mpi_comm_world,merr)
!  call mpi_allreduce(bsq_max2,bsq_max2a,1,mpi_double_precision,mpi_max, &
!                     mpi_comm_world,merr)
!  beta_act=prmax2a/(0.5*bsq_max2a)
!
!  if(myrank .eq. 0) then
!    write(*,*) "beta_act=", beta_act
!  endif    
!
  deallocate(gcov1, gcon1, gcov1in, util, utiln, bcon, ucov, ucon, &
             bbcov, bbcon, beta1, ah, pom, stat=merr)
!
  return
end subroutine mdtorus
!
