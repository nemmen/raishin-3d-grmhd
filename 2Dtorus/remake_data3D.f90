!***********************************************************************
!                     for I / O 
!***********************************************************************
program remake_data3D
  implicit none
  
  integer, parameter :: imax=200, jmax=7, kmax=200 ! grid number
  integer, parameter :: nv=9
  integer, parameter :: npe=4 ! number of cpus
  integer, parameter :: ns=0, ne=101 ! start and end data file number
!
  integer :: i, j, k, n 
  character*256 :: filename
  
  real(8) :: uri(nv,imax,jmax,kmax)
  real(8) :: detg(imax,jmax,kmax)
  real(8) :: x1(imax), x2(jmax), x3(kmax)
  real(8), allocatable :: uri1(:,:,:,:), detg1(:,:,:), x1a(:), x2a(:), x3a(:)
  real(8) :: time
  integer :: is, ie, js, je, ks, ke
  integer :: myrank, nd, npe1, myrank1, merr
!
!  File open for output file
!
  open( unit=8, file='structr.outdat', status='unknown', &
       form='unformatted')
!
! Data initialize
!
  do k=1,kmax
    do j=1,jmax
      do i=1,imax
        do n=1,nv
          uri(n,i,j,k)=1.d0
        enddo  
        detg(i,j,k)=0.d0
      enddo
    enddo
  enddo
!
!
  do nd=ns,ne
    write(*,*) 'working on ',nd,'th data'
! 
! read each process data
!
    do myrank=0,npe-1
      write(*,*) 'reading ', myrank, 'th process data'

      write(filename,990) myrank, nd
990   format('structr',i3.3,'-',i3.3,'.outdat')
      open( unit=7,file=filename,form='unformatted',status='old')

      read(7) time, myrank1, npe1
!
      if(myrank .ne. myrank1) then
        write(*,*) 'reading process data is wrong'
         stop
      endif
      if(npe .ne. npe1) then
        write(*,*) 'total process number is wrong'
        stop
      endif   
!
      read(7) is,ie,js,je,ks,ke
      allocate(uri1(nv,0:ie-is,0:je-js,0:ke-ks), &
               detg1(0:ie-is,0:je-js,0:ke-ks),&
               x1a(0:ie-is), x2a(0:je-js), x3a(0:ke-ks),stat=merr)

      do i=0,ie-is
        do j=0,je-js
          do k=0,ke-ks
            read(7) x1a(i),x2a(j),x3a(k),uri1(1,i,j,k),uri1(2,i,j,k), &
                   uri1(3,i,j,k),uri1(4,i,j,k),uri1(5,i,j,k),uri1(6,i,j,k), &
                   uri1(7,i,j,k),uri1(8,i,j,k),uri1(9,i,j,k), detg1(i,j,k) 
          enddo
        enddo
      enddo
! 
      do i=0,ie-is
        x1(is+i)=x1a(i)
      enddo
!
      do j=0,je-js
        x2(js+j)=x2a(j)
      enddo
!
      do k=0,ke-ks
        x3(ks+k)=x3a(k)
      enddo
!
      do k=0,ke-ks
        do j=0,je-js
          do i=0,ie-is
            uri(1,is+i,js+j,ks+k)=uri1(1,i,j,k)
            uri(2,is+i,js+j,ks+k)=uri1(2,i,j,k)
            uri(3,is+i,js+j,ks+k)=uri1(3,i,j,k)
            uri(4,is+i,js+j,ks+k)=uri1(4,i,j,k)
            uri(5,is+i,js+j,ks+k)=uri1(5,i,j,k)
            uri(6,is+i,js+j,ks+k)=uri1(6,i,j,k)
            uri(7,is+i,js+j,ks+k)=uri1(7,i,j,k)
            uri(8,is+i,js+j,ks+k)=uri1(8,i,j,k)
            uri(9,is+i,js+j,ks+k)=uri1(9,i,j,k)
            detg(is+i,js+j,ks+k)=detg1(i,j,k)  
          enddo
        enddo
      enddo    
!
      close(7)
      deallocate(uri1,detg1,x1a,x2a,x3a,stat=merr)
    enddo
!
    write(8) time
    do i=1,imax
      do j=1,jmax
        do k=1,kmax
          write(8) x1(i),x2(j),x3(k),uri(1,i,j,k),uri(2,i,j,k), &
                   uri(3,i,j,k),uri(4,i,j,k),uri(5,i,j,k),uri(6,i,j,k), &
                   uri(7,i,j,k),uri(8,i,j,k),uri(9,i,j,k),detg(i,j,k)
        enddo
      enddo
    enddo    
!
  enddo
!
  stop  
end program remake_data3D
!
