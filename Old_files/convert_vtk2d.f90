!***********************************************************************
!      RAISHIN code: 3D Special Relativistic MHD (3D MPI) version 
!      program for output to VTK format (output in Cartesian cooddinates)
!      written by Y. Mizuno
!      ver. 150223
!***********************************************************************
!
program convert_vtk2d
  use pram, only: imax, jmax, kmax, metric

  implicit none

!======================================================================@
!    Difinition for variables
!======================================================================@
!
  integer :: i, j, k, l, m, n, mh, jh, kh, jh1, merr
  integer :: mmax, nmax
  character*256 :: filename 
!
  integer, parameter :: nh=2 !- total output file number -!
  integer, parameter :: ns=1 !- start (reading) output file number -!
  integer, parameter :: nq=13
  integer, parameter :: dataformat=0 !- 0= ascii, 1=binary -!
  integer, parameter :: idirec=13 ! direction (1=x, 2=y, 3=z)
!
  real(8) :: qq(nq,imax,jmax,kmax)
! 
  real(4), allocatable :: xx(:,:), yy(:,:), zz(:,:)
  real(4), allocatable :: dd(:,:), pp(:,:), ink(:,:)  
  real(4), allocatable :: ux(:,:), uy(:,:), uz(:,:) 
  real(4), allocatable :: bx(:,:), by(:,:), bz(:,:) 
!
  integer :: it, nt0, nt, time
!
!
!======================================================================@
!     File Open
!======================================================================@
!
  open(unit=6,file='convert.outdat',status='unknown')
  open(unit=8,file='structr.outdat',status='unknown',form='unformatted')
!
!======================================================================@
!- initialize -!
!
  nt0=0
  nt=0
!
!- read output file setting information -!
!
  write(6,*) 'number of output data:', nh
  write(6,*) 'number of start data:', ns
  write(6,*) 'dataformat (0:ascii, 1:binary):', dataformat
!  
!
!**********************************************************************@
!     Main Loop1 Start 
!**********************************************************************@
!
  do mh=1,nh

!======================================================================@
!     Read data from structr.outdat
!======================================================================@
!
    nt0=nt
!
    nt=ns+(mh-1)
    write(6,*) ' nt:',nt
!
!-  Initialize of reading array -!
!
    do k=1,kmax
      do j=1,jmax
        do i=1,imax
          do n=1,nq
            qq(n,i,j,k)=0.d0
          enddo
        enddo
      enddo
    enddo

!- read data -
    if( nt.lt.nt0 ) then
! 
      rewind(8)
!
      do it=1,nt
!
        read(8,end=140) time
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              read(8,end=142) (qq(n,i,j,k),n=1,nq)
            enddo
          enddo
        enddo
!
      enddo
!
    elseif( nt.gt.nt0 ) then
!
      do it=1,nt-nt0
! 
       read(8,end=140) time
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              read(8,end=142) (qq(n,i,j,k),n=1,nq)
            enddo
          enddo
        enddo
!
      enddo

    endif
!
    go to 164
! 
 142 continue
     write(6,*) 'file end found during reading ONE PROFILE'
     write(6,*) ' it =',it,' i =',i,' j =',j,' k =',k
     write(6,*) 'STOP at MAIN'
     stop
!
 140 continue
     write(6,*) 'file end found during reading'
     write(6,*) ' it =',it,' i =',i,' j =',j,' k =',k
     write(6,*) 'continue process'
! 
 164 continue
!
!----------------------------------------------------------------------@
!- Output data for analysis -!
!
!- set output file -!
    write(filename,990) mh-1
    open( unit=9, file=filename,status='replace',form="formatted")
990 format('ok',i3.3,'.vtk')  
!
    if(idirec .eq. 13) then    
!- set half grid position -!
      jh=(jmax+1)/2
!- set j-th direction =1 -!
      mmax=1      
!
!- data allocate -!
      allocate(xx(imax,kmax), yy(imax,kmax), zz(imax,kmax), &
               dd(imax,kmax), pp(imax,kmax), ink(imax,kmax), &
               ux(imax,kmax), uy(imax,kmax), uz(imax,kmax), &
               bx(imax,kmax), by(imax,kmax), bz(imax,kmax), stat=merr)
!      
!- convert the data array -!
!    
      do k=1,kmax
        do i=1,imax
          xx(i,k)=real(qq(1,i,jh,k)*sin(qq(3,i,jh,k)))
          yy(i,k)=real(qq(1,i,jh,k)*cos(qq(3,i,jh,k)))
!          xx(i,k)=real(qq(1,i,jh,k)*cos(qq(3,i,jh,k)))
!          yy(i,k)=real(qq(1,i,jh,k)*sin(qq(3,i,jh,k)))
           zz(i,k)=0.0

          dd(i,k)=real(qq(4,i,jh,k)) !- density -!
          ux(i,k)=real(qq(5,i,jh,k)*sin(qq(3,i,jh,k)) &
                +qq(7,i,jh,k)*cos(qq(3,i,jh,k))) !- 1st component of velocity -!
          uy(i,k)=real(qq(5,i,jh,k)*cos(qq(3,i,jh,k)) &
               -qq(7,i,jh,k)*sin(qq(3,i,jh,k))) !- 2nd component of velocity -!
!          uy(i,k)=real(qq(5,i,jh,k)*sin(qq(3,i,jh,k)) &
!                +qq(7,i,jh,k)*cos(qq(3,i,jh,k))) !- 1st component of velocity -!
!          ux(i,k)=real(qq(5,i,jh,k)*cos(qq(3,i,jh,k)) &
!                -qq(7,i,jh,k)*sin(qq(3,i,jh,k))) !- 2nd component of velocity -!
!          uz(i,k)=real(qq(6,i,jh,k)) !- 3rd component of velocity -!
          uz(i,k)=0.0
          pp(i,k)=real(qq(8,i,jh,k)) !- gas pressure -!
          ink(i,k)=real(qq(9,i,jh,k)) !- tracer scalar -!          
          bx(i,k)=real(qq(10,i,jh,k)*sin(qq(3,i,jh,k)) &
                +qq(12,i,jh,k)*cos(qq(3,i,jh,k))) !- 1st component of magnetic field -!
          by(i,k)=real(qq(10,i,jh,k)*cos(qq(3,i,jh,k)) &
               -qq(12,i,jh,k)*sin(qq(3,i,jh,k))) !- 2nd component of magnetic field -!
!          by(i,k)=real(qq(10,i,jh,k)*sin(qq(3,i,jh,k)) &
!                +qq(12,i,jh,k)*cos(qq(3,i,jh,k))) !- 1st component of magnetic field -!
!          bx(i,k)=real(qq(10,i,jh,k)*cos(qq(3,i,jh,k)) &
!                -qq(12,i,jh,k)*sin(qq(3,i,jh,k))) !- 2nd component of magnetic field -!
          bz(i,k)=real(qq(11,i,jh,k)) !- 3rd component of magnetic field -!          
        enddo
      enddo
!     
      call write_vtk2d(imax,kmax,mmax,xx,yy,zz,dd,ux,uy,uz,pp,&
                       bx,by,bz,mh,dataformat,filename)
!      
      deallocate(xx, yy, zz, dd, pp, ink, ux, uy, uz, bx, by, bz, stat=merr)
!
   elseif(idirec .eq. 12) then
!
!- set half grid position -!
      kh=(kmax+1)/2
!- set k-th direction =1 -!
      nmax=1
!- set j-th direction =jmax - overlap region
!      mmax=jmax-6
      mmax=jmax-5
!- data allocate -!
      allocate(xx(imax,mmax), yy(imax,mmax), zz(imax,mmax), &
               dd(imax,mmax), pp(imax,mmax), ink(imax,mmax), &
               ux(imax,mmax), uy(imax,mmax), uz(imax,mmax), &
               bx(imax,mmax), by(imax,mmax), bz(imax,mmax), stat=merr)
!
!- convert the data array -!
!    
      do j=1, mmax
        do i=1,imax
!          jh1=j+3
          jh1=j+2
    
          xx(i,j)=real(qq(1,i,jh1,kh)*cos(qq(2,i,jh1,kh)))
          yy(i,j)=real(qq(1,i,jh1,kh)*sin(qq(2,i,jh1,kh)))
!          xx(i,j)=real(qq(1,i,jh1,kh)*sin(qq(2,i,jh1,kh)))
!          yy(i,j)=real(qq(1,i,jh1,kh)*cos(qq(2,i,jh1,kh)))
          zz(i,j)=0.0

!          if(i .eq. 10) then
!            write(*,*) "jh1,ph,xx,yy=", jh1, qq(2,i,jh1,kh),xx(i,j), yy(i,j)
!          endif
         
          dd(i,j)=real(qq(4,i,jh1,kh)) !- density -!
          ux(i,j)=real(qq(5,i,jh1,kh)*cos(qq(2,i,jh1,kh)) &
                -qq(6,i,jh1,kh)*sin(qq(2,i,jh1,kh))) !- 1st component of velocity -!
          uy(i,j)=real(qq(5,i,jh1,kh)*sin(qq(2,i,jh1,kh)) &
                +qq(6,i,jh1,kh)*cos(qq(2,i,jh1,kh))) !- 2nd component of velocity -!
!          uy(i,j)=real(qq(5,i,jh1,kh)*cos(qq(2,i,jh1,kh)) &
!                -qq(6,i,jh1,kh)*sin(qq(2,i,jh1,kh))) !- 1st component of velocity -!
!          ux(i,j)=real(qq(5,i,jh1,kh)*sin(qq(2,i,jh1,kh)) &
!                +qq(6,i,jh1,kh)*cos(qq(2,i,jh1,kh))) !- 2nd component of velocity -!
          uz(i,j)=real(qq(6,i,jh1,kh)) !- 3rd component of velocity -!
!          uz(i,j)=0.0
          pp(i,j)=real(qq(8,i,jh1,kh)) !- gas pressure -!
          ink(i,j)=real(qq(9,i,jh1,kh)) !- tracer scalar -!          
          bx(i,j)=real(qq(10,i,jh1,kh)*cos(qq(2,i,jh1,kh)) &
                -qq(11,i,jh1,kh)*sin(qq(2,i,jh1,kh))) !- 1st component of magnetic field -!
          by(i,j)=real(qq(10,i,jh1,kh)*sin(qq(2,i,jh1,kh)) &
               +qq(11,i,jh1,kh)*cos(qq(2,i,jh1,kh))) !- 2nd component of magnetic field -!
!          by(i,j)=real(qq(10,i,jh1,kh)*cos(qq(2,i,jh1,kh)) &
!                -qq(11,i,jh1,kh)*sin(qq(2,i,jh1,kh))) !- 1st component of magnetic field -!
!          bx(i,j)=real(qq(10,i,jh1,kh)*sin(qq(2,i,jh1,kh)) &
!                +qq(11,i,jh1,kh)*cos(qq(2,i,jh1,kh))) !- 2nd component of magnetic field -!          
          bz(i,j)=real(qq(12,i,jh1,kh)) !- 3rd component of magnetic field -!          
        enddo
      enddo
!     
      call write_vtk2d(imax,mmax,nmax,xx,yy,zz,dd,ux,uy,uz,pp,&
                       bx,by,bz,mh,dataformat,filename)
!
      deallocate(xx, yy, zz, dd, pp, ink, ux, uy, uz, bx, by, bz, stat=merr)
!      
    endif
!      
  enddo    

  stop    
end program convert_vtk2d

!------------------------------------------------------------
subroutine write_vtk2d(xmax,ymax,zmax,xx,yy,zz,dd,ux,uy,uz,pp,&
                       bx,by,bz,mh,dataformat,filename)
!------------------------------------------------------------
!   
!- write data for VTK format -!
!  Now the data must be cartesian coordinates!
!  The gird points does not need to be regular.!
!
  implicit none
!
  integer :: i, j, k, xmax, ymax, zmax, mh
  integer :: merr
  integer :: dataformat
  character*256 :: filename
!  
  real(4) :: xx(xmax,ymax), yy(xmax,ymax), zz(xmax,ymax)
  real(4) :: dd(xmax,ymax), pp(xmax,ymax)  
  real(4) :: ux(xmax,ymax), uy(xmax,ymax), uz(xmax,ymax) 
  real(4) :: bx(xmax,ymax), by(xmax,ymax), bz(xmax,ymax)
!  
  real(4) :: dum_r
  character(LEN=1), parameter :: newline = achar(10) ! newline symbol!
  
!  There are five basic parts to the VTK file format.  
!  1. Write file version and identifier (Header)
!     
    write(9, "(a)") "# vtk DataFile Version 2.0"    
!
!  2. Tilte
!    
    write(9, "(a)") "RMHD Simulation Result"    
!
!  3. Data type (ASCII or BINARY)
!    
    if(dataformat .eq. 0) then
      write(9, "(a)") "ASCII" 
    else
      write(9, "(a)") "BINARY"
    endif
!
!  4.  Dataset structure 
!
10  format(a11, 3(1x, i6))
11  format(a, 2x, i9, 2x, a5)
    write(9, "(a)") "DATASET STRUCTURED_GRID"
    write(9, 10)    "DIMENSIONS ", xmax, ymax, zmax
    write(9, 11)    "POINTS", xmax*ymax*zmax, "float"
!
    write(6,*) 'after header: mh=', mh
!
! -- now dumps coordnates
    if(dataformat .ne. 0) then
      close(9)
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")
!           position="append", access="stream")
    endif
! -- now dumps data
    do j = 1, ymax
      do i = 1, xmax
        if(dataformat .eq. 0) then                 
          write(9,*) xx(i,j), yy(i,j), zz(i,j)
        else
          write(9) xx(i,j), yy(i,j), zz(i,j)
        endif
      enddo
    enddo
!
    write(6,*) 'after coordinates: mh=', mh
!    
!  5 . Data
!    
    if(dataformat .ne. 0) then
      close(9)
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")          
    endif
!
12  format(a, a11, 1x, i12)
! You need to add newline character for safty after writing data in binary
!   write(9, "(a)") 
    write(9, 12) newline, "POINT_DATA ", xmax*ymax*zmax
!
    write(6,*) 'after new line: mh=', mh   
!
! Write density
!
    write(9, "(a)") "SCALARS density float"
    write(9, "(a)") "LOOKUP_TABLE default"
!
! --- now dumps data
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")  
    endif
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax             
          dum_r = max(dd(i,j), 1e-30) ! for safety
          write(9,*) dum_r
        enddo
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          dum_r = max(dd(i,j), 1e-30) ! for safety
          write(9) dum_r
        enddo
      enddo
    endif
!
    write(6,*) 'after density: mh=', mh  
!
! Write velocity
!
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")   
    endif
    
    write(9, "(a, a)") newline, "VECTORS velocity float"
    
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
!           position="append", access="stream")   
    endif
! -- now dumps data
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax
          write(9,*) ux(i,j), uy(i,j), uz(i,j)
        enddo           
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          write(9) ux(i,j), uy(i,j), uz(i,j)
        enddo
      enddo           
    endif
!
    write(6,*) 'after velocity vector: mh=', mh  
!
! Write B-field vector
!
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as formatted and append
      open(unit=9, file=filename, status="old", form="formatted", &
           position="append")   
    endif
    
    write(9, "(a, a)") newline, "VECTORS B-field float"
    
    if(dataformat .ne. 0) then
      close(9) 
! reopen it as unformatted and append
      open(unit=9, file=filename, status="old", form="unformatted", &
           position="append")   
 !          position="append", access="stream") 
    endif
! -- now dumps data
    if(dataformat .eq. 0) then
      do j = 1, ymax
        do i = 1, xmax
          write(9,*) bx(i,j), by(i,j), bz(i,j)
        enddo           
      enddo
    else
      do j = 1, ymax
        do i = 1, xmax
          write(9) bx(i,j), by(i,j), bz(i,j)
        enddo
      enddo           
    endif
!
    write(6,*) 'end of the data: mh=', mh  
!
    close(9)
!
  return
end subroutine write_vtk2d   
