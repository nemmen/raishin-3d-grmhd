!***********************************************************************
!      RAISHIN code: 3D General Relativistic MHD (3D MPI) version 
!      analysis (output) program (output in Cartesian cooddinates)
!      written by Y. Mizuno
!      ver. 140618
!***********************************************************************
!
program kamt
  use pram, only: imax, jmax, kmax, xmin, xmax, ymin, ymax, zmin, zmax, &
                   ix1, ix2, ix3, pi, R0, hslope
  use outpram !- for input output parameter -!
 
  implicit none

!======================================================================@
!    Difinition for variables
!======================================================================@
!
  integer :: i, j, k, l, m, n, n2, n3, ii, kk
  integer :: mh, it, ih, jh, kh
  integer :: merr
  character*256 :: filename
!
  integer, parameter :: nq=13
!
  real(8), allocatable :: qq(:,:,:,:)
  real(8), allocatable :: pom(:,:,:)
! 
  real(8), allocatable :: dir1dx(:), dir1dy(:), dir1dz(:), &
                          de1d(:), vx1d(:), vy1d(:), vz1d(:), &
                          pr1d(:), ink1d(:), bx1d(:), by1d(:), bz1d(:)
!
  real(8), allocatable :: dir2dx(:,:), dir2dy(:,:), dir2dz(:,:), &
                          de2d(:,:), vx2d(:,:), vy2d(:,:), vz2d(:,:), &
                          pr2d(:,:), ink2d(:,:), &
                          bx2d(:,:), by2d(:,:), bz2d(:,:), pom2d(:,:)
!
  real(8), allocatable :: dir3dx(:,:,:), dir3dy(:,:,:), dir3dz(:,:,:), &
                          de3d(:,:,:), &
                          vx3d(:,:,:), vy3d(:,:,:), vz3d(:,:,:), &
                          pr3d(:,:,:), ink3d(:,:,:), &
                          bx3d(:,:,:), by3d(:,:,:), bz3d(:,:,:)
!
  real(8), allocatable ::  x1_old(:), x3_old(:), x1_new(:), x3_new(:)

  integer :: nt0, nt, ifile, ini, ino, kni, kno, icheck, kcheck
  real(8) :: time 
  real(8) :: rr, th, ph, vr, vph, vth, br, bph, bth
  real(8) :: dx1o, dx3o, dx1n, dx3n, dr1n, xx0, rr0, x1ostat, r1nstat, &
             x1to, x1tn, x3ostat, x3to, x3tn, tmpth, slp1, slp2, tmp1
  real(8) :: psiaa, phiaa
  real(8) :: detmp1, detmp2, vrtmp1, vrtmp2, tmpvr, &
             vphtmp1, vphtmp2, tmpvph,  vthtmp1, vthtmp2, tmpvth, &
             prtmp1, prtmp2, inktmp1, inktmp2, &
             brtmp1, brtmp2, tmpbr, bphtmp1, bphtmp2, tmpbph, &
             bthtmp1, bthtmp2, tmpbth, pomtmp1, pomtmp2   
!
!- allocate variables -!
  allocate(qq(nq,imax,jmax,kmax),stat=merr)      
!
!
!======================================================================@
!     File Open
!======================================================================@
!
!  open(unit=1,file='iout_new',status='unknown')
  open(unit=6,file='kamt.outdat',status='unknown')
  open(unit=8,file='structr.outdat',status='unknown',form='unformatted')
!
!======================================================================@
!- initialize -!
!
  nt0=0
  nt=0
  ifile=1
!
!- read output file setting information -!
!
!  read(1,*) nh, ns, ndim, idirec, metric

  write(6,*) ' number of historical section:', nh
  write(6,*) ' dimension:', ndim, ' direction:', idirec 
  write(6,*) ' metric:', metric
!
!**********************************************************************@
!     Main Loop1 Start 
!**********************************************************************@
!
  do mh=1,nh

!======================================================================@
!     Read data from structr.outdat
!======================================================================@
!
    nt0=nt
!
    nt=ns+(mh-1)
    write(6,*) ' nt:',nt
!
!-  Initialize of reading array -!
!
    do k=1,kmax
      do j=1,jmax
        do i=1,imax
          do n=1,nq
            qq(n,i,j,k)=0.d0
          enddo
        enddo
      enddo
    enddo

!- read data -
    if( nt.lt.nt0 ) then
! 
      rewind(8)
!
      do it=1,nt
!
        read(8,end=140) time
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              read(8,end=142) (qq(n,i,j,k),n=1,nq)
            enddo
          enddo
        enddo
!
      enddo
!
    elseif( nt.gt.nt0 ) then
!
      do it=1,nt-nt0
! 
       read(8,end=140) time
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              read(8,end=142) (qq(n,i,j,k),n=1,nq)
            enddo
          enddo
        enddo
!
      enddo

    endif
!
    go to 164
! 
 142 continue
     write(6,*) 'file end found during reading ONE PROFILE'
     write(6,*) ' it =',it,' i =',i,' j =',j,' k =',k
     write(6,*) 'STOP at MAIN'
     stop
!
 140 continue
     write(6,*) 'file end found during reading'
     write(6,*) ' it =',it,' i =',i,' j =',j,' k =',k
     write(6,*) 'continue process'
! 
 164 continue
!
!----------------------------------------------------------------------@
!- Output data for analysis -!
!
!- set output file -!
    write(filename,990) mh
    open( unit=9, file=filename,status='unknown')
990 format('ok',i3.3)  
!
    if(ndim .eq. 1) then !- 1D output -!
!
      if(idirec .eq. 1) then !- i direction -!
!- allocate of 1D array -!
        allocate(dir1dx(imax), de1d(imax), vx1d(imax), vy1d(imax), vz1d(imax), &
                 pr1d(imax), ink1d(imax), bx1d(imax), by1d(imax), bz1d(imax), &
                 stat=merr)
!- set half grid position -!
        jh=(jmax+1)/2
        kh=(kmax+1)/2
        write(*,*) "phi, theta",qq(2,1,jh,kh), qq(3,1,jh,kh)
!
        if(metric .eq. 1 .or. metric .eq. 2) then        
!- Cartesian, clyndrical => Cartesian             
          do i=1,imax
            dir1dx(i)=qq(1,i,jh,kh)
!
            de1d(i)=qq(4,i,jh,kh)
            vx1d(i)=qq(5,i,jh,kh)
            vy1d(i)=qq(6,i,jh,kh)
            vz1d(i)=qq(7,i,jh,kh)
            pr1d(i)=qq(8,i,jh,kh)
            ink1d(i)=qq(9,i,jh,kh)
            bx1d(i)=qq(10,i,jh,kh)
            by1d(i)=qq(11,i,jh,kh)
            bz1d(i)=qq(12,i,jh,kh)            
!           write(*,*) 'i,dir1dx,de=', i, dir1dx(i), de1d(i)
          enddo  
        elseif(metric .eq. 3) then
!- Spherical => Cartesian
          do i=1,imax
            dir1dx(i)=qq(1,i,jh,kh)
!
            de1d(i)=qq(4,i,jh,kh)
            vx1d(i)=qq(5,i,jh,kh)
            vy1d(i)=qq(6,i,jh,kh)
            vz1d(i)=-qq(7,i,jh,kh)
            pr1d(i)=qq(8,i,jh,kh)
            ink1d(i)=qq(9,i,jh,kh)
            bx1d(i)=qq(10,i,jh,kh)
            by1d(i)=qq(11,i,jh,kh)
            bz1d(i)=-qq(12,i,jh,kh) 
          enddo
        endif
!- output
!        do i=1,imax
!          write(10+mh,800) dir1dx(i),de1d(i),vx1d(i),vy1d(i),vz1d(i), &
!                           pr1d(i),bx1d(i),by1d(i),bz1d(i)
!        enddo
!        write(10+mh,*) time,'  time'
!
        do i=1,imax
          write(9,800) dir1dx(i),de1d(i),vx1d(i),vy1d(i),vz1d(i), &
                       pr1d(i),bx1d(i),by1d(i),bz1d(i)
        enddo
        write(9,*) time,'  time'
!
        deallocate(dir1dx, de1d, vx1d, vy1d, vz1d, &
                   pr1d, ink1d, bx1d, by1d, bz1d, stat=merr)
!
      elseif(idirec .eq. 2) then !- j-direction -!
!- allocate of 1D array -!
        allocate(dir1dy(jmax), de1d(jmax), vx1d(jmax), vy1d(jmax), vz1d(jmax), &
                 pr1d(jmax), ink1d(jmax), bx1d(jmax), by1d(jmax), bz1d(jmax), &
                 stat=merr)
!- set half grid position -!
        ih=(imax+1)/2
        kh=(kmax+1)/2
!
        if(metric .eq. 1) then
!- Cartesian, => Cartesian
          do j=1,jmax
            dir1dy(j)=qq(2,ih,j,kh)
!
            de1d(j)=qq(4,ih,j,kh)
            vx1d(j)=qq(5,ih,j,kh)
            vy1d(j)=qq(6,ih,j,kh)
            vz1d(j)=qq(7,ih,j,kh)
            pr1d(j)=qq(8,ih,j,kh)
            ink1d(j)=qq(9,ih,j,kh)
            bx1d(j)=qq(10,ih,j,kh)
            by1d(j)=qq(11,ih,j,kh)
            bz1d(j)=qq(12,ih,j,kh)                   
          enddo
        endif
!- output
        do j=1,jmax
          write(9,800) dir1dy(j),de1d(j),vx1d(j),vy1d(j),vz1d(j), &
                       pr1d(j),bx1d(j),by1d(j),bz1d(j)
        enddo
        write(9,*) time,'  time'
!
        deallocate(dir1dy, de1d, vx1d, vy1d, vz1d, &
                   pr1d, ink1d, bx1d, by1d, bz1d, stat=merr)
!
      elseif(idirec .eq. 3) then !- k-direction -!
!- allocate of 1D array -!
        allocate(dir1dz(kmax), de1d(kmax), vx1d(kmax), vy1d(kmax), vz1d(kmax), &
                 pr1d(kmax), ink1d(kmax), bx1d(kmax), by1d(kmax), bz1d(kmax), &
                 stat=merr)
!- set half grid position -!
        ih=(imax+1)/2
        jh=(jmax+1)/2
!
        if(metric .eq. 1) then
!- Cartesian, => Cartesian 
          do k=1,kmax  
            dir1dz(k)=qq(3,ih,jh,k)
!
            de1d(k)=qq(4,ih,jh,k)
            vx1d(k)=qq(5,ih,jh,k)
            vy1d(k)=qq(6,ih,jh,k)
            vz1d(k)=qq(7,ih,jh,k)
            pr1d(k)=qq(8,ih,jh,k)
            ink1d(k)=qq(9,ih,jh,k)
            bx1d(k)=qq(10,ih,jh,k)
            by1d(k)=qq(11,ih,jh,k)
            bz1d(k)=qq(12,ih,jh,k)  
!            write(*,*) 'k,bx1d,by1d,bz1d',k,bx1d(k),by1d(k),bz1d(k)
          enddo
        endif
!- output
        do k=1,kmax
          write(9,800) dir1dz(k),de1d(k),vx1d(k),vy1d(k),vz1d(k), &
                       pr1d(k),bx1d(k),by1d(k),bz1d(k)
        enddo
        write(9,*) time,'  time'
!
        deallocate(dir1dz, de1d, vx1d, vy1d, vz1d, &
                   pr1d, ink1d, bx1d, by1d, bz1d, stat=merr)
!
      endif
!
!=========
 !
    elseif(ndim .eq. 2) then !- 2D Output -! 
!
      if(idirec .eq. 1) then !- xz plane -!

!- set half grid position -!
        jh=(jmax+1)/2
!
        if(metric .eq. 1 .or. metric .eq. 2) then
!- Cartesian, Cylindrical => Cartesian
!- allocate of 2D array -!
          allocate(dir2dx(imax,kmax), dir2dz(imax,kmax), de2d(imax,kmax), &
                   vx2d(imax,kmax), vy2d(imax,kmax), vz2d(imax,kmax), &
                   pr2d(imax,kmax), ink2d(imax,kmax), &
                   bx2d(imax,kmax), by2d(imax,kmax), bz2d(imax,kmax), &
                   stat=merr)
!
          do k=1,kmax
            do i=1,imax
              dir2dx(i,k)=qq(1,i,jh,k)
              dir2dz(i,k)=qq(3,i,jh,k)
!
              de2d(i,k)=qq(4,i,jh,k)
              vx2d(i,k)=qq(5,i,jh,k)
              vy2d(i,k)=qq(6,i,jh,k)
              vz2d(i,k)=qq(7,i,jh,k)
              pr2d(i,k)=qq(8,i,jh,k)
              ink2d(i,k)=qq(9,i,jh,k)
              bx2d(i,k)=qq(10,i,jh,k)
              by2d(i,k)=qq(11,i,jh,k)
              bz2d(i,k)=qq(12,i,jh,k) 
            enddo
          enddo
!- Output
          do k=1,kmax
            do i=1,imax
            
!              if(k .eq. 64) then
!                write(*,*) "i,dir2dx,dir2dz=",i,dir2dx(i,k),dir2dz(i,k)
!              endif

              write(9,800) dir2dx(i,k),dir2dz(i,k),de2d(i,k), &
                           vx2d(i,k),vy2d(i,k),vz2d(i,k), &
                           pr2d(i,k),bx2d(i,k),by2d(i,k),bz2d(i,k)
            enddo
          enddo
          write(9,*) time,'  time'
!
          deallocate(dir2dx, dir2dz, de2d, vx2d, vy2d, vz2d, &
                     pr2d, ink2d, bx2d, by2d, bz2d, stat=merr)
!      
        elseif(metric .eq. 3) then
!-Spherical => Cartesian with new grid point
!- set new cartesian image array set -!
!- allocate of 2D array -!
          allocate(dir2dx(lmax,nmax), dir2dz(lmax,nmax), de2d(lmax,nmax), &
                   vx2d(lmax,nmax), vy2d(lmax,nmax), vz2d(lmax,nmax), &
                   pr2d(lmax,nmax), ink2d(lmax,nmax), &
                   bx2d(lmax,nmax), by2d(lmax,nmax), bz2d(lmax,nmax), &
                   x1_old(imax), x3_old(kmax), x1_new(lmax), x3_new(nmax), &
                   pom(imax,jmax,kmax), pom2d(lmax,nmax), stat=merr)
!
!- calculation of vector potential -!
         do k=1,kmax
           do j=1,jmax
             do i=1,imax
!
               pom(i,j,k)=0.d0
               ii=i
               kk=k
!
               psiaa=0.d0
               phiaa=0.d0

               do n=1,kk
                 psiaa=psiaa+qq(13,i,j,n)*qq(10,i,j,n)
               enddo
!
               do m=1,ii
                 phiaa=phiaa+qq(13,m,j,k)*qq(12,m,j,k)
               enddo
!
               pom(i,j,k)=psiaa-phiaa
!        
             enddo
           enddo
         enddo
!
!- new grid point (x,z) -!       
         if(ix1 .eq. 1) then
            
           do l=1,lmax
             dx1n=(xnmax-xnmin)/float(lmax-1)            
             x1_new(l)=xnmin+dx1n*float(l-1)
           enddo
           do n=1,nmax
             dx3n=(znmax-znmin)/float(nmax-1)            
             x3_new(n)=znmin+dx3n*float(n-1)
          enddo
          
         elseif(ix1 .eq. 2) then          
           rr0=0.d0
           r1nstat=log(xnmin-rr0)
           tmp1=log((xnmax-rr0)/(xnmin-rr0))
           dr1n=tmp1/float(lmax-1)
!- x-direction
           do l=1,lmax              
             x1tn=r1nstat+float(l-1)*dr1n
             x1_new(l)=exp(x1tn)+rr0
           enddo
!- z-direction
!           do n=1,nmax
!             dx3n=(znmax-znmin)/float(nmax-1)            
!             x3_new(n)=znmin+dx3n*float(n-1)
!           enddo
!          
           n3=nmax/2+1
           do n=n3, nmax
             n2=n-((nmax/2))-1 
             x3tn=r1nstat+float(n2)*dr1n
             x3_new(n)=exp(x3tn)+rr0 
           enddo  
           do n=n3-1, 1, -1
             n2=-(n-((nmax/2))) 
             x3tn=r1nstat+float(n2)*dr1n
             x3_new(n)=-(exp(x3tn)+rr0)               
           enddo
!           do n=1, nmax
!             write(*,*) "n, zz=", n, x3_new(n)
!            enddo  
         endif
!
          
!- old grid point (r,th) -!
         if(ix1 .eq. 1) then
           do i=1,imax
             dx1o=(xmax-xmin)/float(imax-1)
             x1_old(i)=xmin+dx1o*float(i-1)
           enddo
         elseif(ix1 .eq. 2) then
           xx0=R0
           x1ostat=log(xmin-xx0) 
           dx1o=log((xmax-xx0)/(xmin-xx0))/float(imax-1)
           do i=1,imax
             x1to=x1ostat+float(i-1)*dx1o
             x1_old(i)=exp(x1to)+xx0
           enddo
         endif
!
         if(ix3 .eq. 1) then          
           do k=1,kmax
             dx3o=(zmax-zmin)/float(kmax-1)
             x3_old(k)=zmin+dx3o*float(k-1) 
           enddo
         elseif(ix3 .eq. 2) then
           x3ostat=zmin/pi
           dx3o=((zmax/pi)-(zmin/pi))/float(kmax-1) 
           do k=1,kmax
             x3to=x3ostat+float(k-1)*dx3o
             x3_old(k)=pi*x3to+0.5*(1.-hslope)*sin(2.*zmax*x3to)
           enddo
         endif
!
!- set new direction -!
          do n=1,nmax
            do l=1,lmax
!- position at spherical coordinates -!
              rr=sqrt(x1_new(l)**2+x3_new(n)**2)
              if(x1_new(l) .eq. 0.d0) then
                tmpth=0.d0
              else
                tmpth=abs(x1_new(l))/x3_new(n)
              endif
              if(x3_new(n) .ge. 0.d0) then
                if(x1_new(l) .ge. 0.d0) then
                  th=atan(tmpth)
                else
                  th=2.*pi-abs(atan(tmpth))
                endif
              else
                if(x1_new(l) .ge. 0.d0) then
                  th=pi-abs(atan(tmpth))
                else
                  th=pi+abs(atan(tmpth))
                endif
              endif
!- check nearby grid point in old coordinates -!
              ini=1
              ino=1
              icheck=0
              do i=1,imax
                if(x1_old(i) .gt. rr .and. icheck .eq. 0) then
                  icheck=1
                  ino=i
                  if(ino .eq. 1) then
                    ini=ino
                  else
                    ini=ino-1
                  endif
                endif
              enddo
              if(icheck .eq. 0) then
                ini=imax
                ino=imax
              endif
!
              kni=1
              kno=1
              kcheck=0
              do k=1,kmax
                if(x3_old(k) .gt. th .and. kcheck .eq. 0) then
                  kcheck=1
                  kno=k
                  if(kno .eq. 1) then
                    kni=kno
                  else
                    kni=kno-1
                  endif  
                endif
              enddo
              if(kcheck .eq. 0) then
                kni=kmax
                kno=kmax
              endif
!
!              write(*,*) 'rr, x1_in, x1_out=', rr, x1_old(ini), x1_old(ino)
!
!- averaging new data from surrounding points -!
              if(ini .eq. ino) then
                slp1=0.d0
              else
                slp1=(rr-x1_old(ini))/(x1_old(ino)-x1_old(ini))
              endif
              if(kni .eq. kno) then
                slp2=0.d0
              else
                slp2=(th-x3_old(kni))/(x3_old(kno)-x3_old(kni))
              endif
!
              detmp1=qq(4,ini,jh,kni) &
                     +slp1*(qq(4,ino,jh,kni)-qq(4,ini,jh,kni))
              detmp2=qq(4,ino,jh,kno) &
                     +slp1*(qq(4,ino,jh,kno)-qq(4,ini,jh,kno))
              de2d(l,n)=detmp1+slp2*(detmp2-detmp1)
!
              vrtmp1=qq(5,ini,jh,kni) &
                     +slp1*(qq(5,ino,jh,kni)-qq(5,ini,jh,kni))
              vrtmp2=qq(5,ino,jh,kno) &
                     +slp1*(qq(5,ino,jh,kno)-qq(5,ini,jh,kno))
              tmpvr=vrtmp1+slp2*(vrtmp2-vrtmp1)
              vphtmp1=qq(6,ini,jh,kni) &
                      +slp1*(qq(6,ino,jh,kni)-qq(6,ini,jh,kni))
              vphtmp2=qq(6,ino,jh,kno) &
                      +slp1*(qq(6,ino,jh,kno)-qq(6,ini,jh,kno))
              tmpvph=vphtmp1+slp2*(vphtmp2-vphtmp1)
              vthtmp1=qq(7,ini,jh,kni) &
                      +slp1*(qq(7,ino,jh,kni)-qq(7,ini,jh,kni))
              vthtmp2=qq(7,ino,jh,kno) &
                      +slp1*(qq(7,ino,jh,kno)-qq(7,ini,jh,kno))
              tmpvth=vthtmp1+slp2*(vthtmp2-vthtmp1)
!
              vx2d(l,n)=tmpvr*sin(th)+tmpvth*cos(th)
              vy2d(l,n)=tmpvph
              vz2d(l,n)=tmpvr*cos(th)-tmpvth*sin(th)

              prtmp1=qq(8,ini,jh,kni) &
                     +slp1*(qq(8,ino,jh,kni)-qq(8,ini,jh,kni))
              prtmp2=qq(8,ino,jh,kno) &
                     +slp1*(qq(8,ino,jh,kno)-qq(8,ini,jh,kno))
              pr2d(l,n)=prtmp1+slp2*(prtmp2-prtmp1)
!
              inktmp1=qq(9,ini,jh,kni) &
                      +slp1*(qq(9,ino,jh,kni)-qq(9,ini,jh,kni))
              inktmp2=qq(9,ino,jh,kno) &
                      +slp1*(qq(9,ino,jh,kno)-qq(9,ini,jh,kno))
              ink2d(l,n)=inktmp1+slp2*(inktmp2-inktmp1)              
!
              brtmp1=qq(10,ini,jh,kni) &
                     +slp1*(qq(10,ino,jh,kni)-qq(10,ini,jh,kni))
              brtmp2=qq(10,ino,jh,kno) &
                     +slp1*(qq(10,ino,jh,kno)-qq(10,ini,jh,kno))
              tmpbr=brtmp1+slp2*(brtmp2-brtmp1)
              bphtmp1=qq(11,ini,jh,kni) &
                      +slp1*(qq(11,ino,jh,kni)-qq(11,ini,jh,kni))
              bphtmp2=qq(11,ino,jh,kno) &
                      +slp1*(qq(11,ino,jh,kno)-qq(11,ini,jh,kno))
              tmpbph=bphtmp1+slp2*(bphtmp2-bphtmp1)
              bthtmp1=qq(12,ini,jh,kni) &
                      +slp1*(qq(12,ino,jh,kni)-qq(12,ini,jh,kni))
              bthtmp2=qq(12,ino,jh,kno) &
                      +slp1*(qq(12,ino,jh,kno)-qq(12,ini,jh,kno))
              tmpbth=bthtmp1+slp2*(bthtmp2-bthtmp1)
!
              bx2d(l,n)=tmpbr*sin(th)+tmpbth*cos(th)
              by2d(l,n)=tmpbph
              bz2d(l,n)=tmpbr*cos(th)-tmpbth*sin(th)
!
              pomtmp1=pom(ini,jh,kni) &
                      +slp1*(pom(ino,jh,kni)-pom(ini,jh,kni))
              pomtmp2=pom(ino,jh,kno) &
                      +slp1*(pom(ino,jh,kno)-pom(ini,jh,kno))
              pom2d(l,n)=pomtmp1+slp2*(pomtmp2-pomtmp1)     
!
              dir2dx(l,n)=x1_new(l)
              dir2dz(l,n)=x3_new(n)
            enddo
          enddo
!
!- Output
          do n=1,nmax
            do l=1,lmax
            
!              if(l .eq. 128) then
!                write(*,*) "l,dir2dx,dir2dz=",l,dir2dx(i,k),dir2dz(i,k)
!              endif

              write(9,800) dir2dx(l,n),dir2dz(l,n),de2d(l,n), &
                           vx2d(l,n),vy2d(l,n),vz2d(l,n), &
                           pr2d(l,n),bx2d(l,n),by2d(l,n),bz2d(l,n),&
                           pom2d(l,n)
            enddo
          enddo
          write(9,*) time,'  time'
!
          deallocate(dir2dx, dir2dz, de2d, vx2d, vy2d, vz2d, &
                     pr2d, ink2d, bx2d, by2d, bz2d, &
                     x1_old, x3_old, x1_new, x3_new, &
                     pom, pom2d, stat=merr)
!
        elseif(metric .eq. 4) then
!- Spherical => position in spherical & vectors in cartesian)
!- allocate of 2D array -!
          allocate(dir2dx(imax,kmax), dir2dz(imax,kmax), de2d(imax,kmax), &
                   vx2d(imax,kmax), vy2d(imax,kmax), vz2d(imax,kmax), &
                   pr2d(imax,kmax), ink2d(imax,kmax), &
                   bx2d(imax,kmax), by2d(imax,kmax), bz2d(imax,kmax), &
                   stat=merr)
!
          do k=1,kmax
            do i=1,imax
              dir2dx(i,k)=qq(1,i,jh,k)
              dir2dz(i,k)=qq(3,i,jh,k)
!              dir2dz(i,k)=qq(3,i,jh,k)*180.d0/pi
              th=qq(3,i,jh,k)
!
              de2d(i,k)=qq(4,i,jh,k)
!
!              vr=qq(5,i,jh,k)
!              vph=qq(6,i,jh,k)
!              vth=qq(7,i,jh,k)

              vx2d(i,k)=qq(5,i,jh,k)
              vy2d(i,k)=qq(6,i,jh,k)
              vz2d(i,k)=qq(7,i,jh,k)
!
!              vx2d(i,k)=vr*sin(th)+vth*cos(th)
!              vy2d(i,k)=vph
!              vz2d(i,k)=vr*cos(th)-vth*sin(th)   

              pr2d(i,k)=qq(8,i,jh,k)
              ink2d(i,k)=qq(9,i,jh,k)
!
              bx2d(i,k)=qq(10,i,jh,k)
              by2d(i,k)=qq(11,i,jh,k)
              bz2d(i,k)=qq(12,i,jh,k)

!              br=qq(10,i,jh,k)
!              bph=qq(11,i,jh,k)
!              bth=qq(12,i,jh,k)
!
!              bx2d(i,k)=br*sin(th)+bth*cos(th)
!              by2d(i,k)=bph
!              bz2d(i,k)=br*cos(th)-bth*sin(th)           
 
            enddo
          enddo
!
          do k=1,kmax
            do i=1,imax
              write(9,800) dir2dx(i,k),dir2dz(i,k),de2d(i,k), &
                           vx2d(i,k),vy2d(i,k),vz2d(i,k), &
                           pr2d(i,k),bx2d(i,k),by2d(i,k),bz2d(i,k)
            enddo
          enddo
          write(9,*) time,'  time'
!
        endif
!
      elseif(idirec .eq. 2) then !- yz-direction -!
!
!- set half grid position -!
        ih=(imax+1)/2
!
        if(metric .eq. 1) then                 
!- Cartesian => Cartesian
!- allocate of 2D array -!
          allocate(dir2dy(jmax,kmax), dir2dz(jmax,kmax), de2d(jmax,kmax), &
                   vx2d(jmax,kmax), vy2d(jmax,kmax), vz2d(jmax,kmax), &
                   pr2d(jmax,kmax), ink2d(jmax,kmax), &
                   bx2d(jmax,kmax), by2d(jmax,kmax), bz2d(jmax,kmax), &
                   stat=merr)

          do k=1,kmax
            do j=1,jmax
              dir2dy(j,k)=qq(2,ih,j,k)
              dir2dz(j,k)=qq(3,ih,j,k)
!
              de2d(j,k)=qq(4,ih,j,k)
              vx2d(j,k)=qq(5,ih,j,k)
              vy2d(j,k)=qq(6,ih,j,k)
              vz2d(j,k)=qq(7,ih,j,k)
              pr2d(j,k)=qq(8,ih,j,k)
              ink2d(j,k)=qq(9,ih,j,k)
              bx2d(j,k)=qq(10,ih,j,k)
              by2d(j,k)=qq(11,ih,j,k)
              bz2d(j,k)=qq(12,ih,j,k) 
!                     
            enddo
          enddo
!- Output
          do j=1,jmax
            do k=1,kmax
              write(9,800) dir2dy(j,k),dir2dz(j,k),de2d(j,k), &
                           vx2d(j,k),vy2d(j,k),vz2d(j,k), &
                           pr2d(j,k),bx2d(j,k),by2d(j,k),bz2d(j,k)
            enddo
          enddo
          write(9,*) time,'  time'
!
          deallocate(dir2dy, dir2dz, de2d, vx2d, vy2d, vz2d, &
                     pr2d, ink2d, bx2d, by2d, bz2d, stat=merr)
!
        endif
!
      elseif(idirec .eq. 3) then !- xy-direction -!
!- set half grid position -!
        kh=(kmax+1)/2
        if(metric .eq. 1) then
!- Cartesian => Cartesian
!- allocate of 2D array -!
          allocate(dir2dx(imax,jmax), dir2dy(imax,jmax), de2d(imax,jmax), &
                   vx2d(imax,jmax), vy2d(imax,jmax), vz2d(imax,jmax), &
                   pr2d(imax,jmax), ink2d(imax,jmax), &
                   bx2d(imax,jmax), by2d(imax,jmax), bz2d(imax,jmax), &
                   stat=merr)

          do j=1,jmax
            do i=1,imax
              dir2dx(i,j)=qq(1,i,j,kh)
              dir2dy(i,j)=qq(2,i,j,kh)
!
              de2d(i,j)=qq(4,i,j,kh)
              vx2d(i,j)=qq(5,i,j,kh)
              vy2d(i,j)=qq(6,i,j,kh)
              vz2d(i,j)=qq(7,i,j,kh)
              pr2d(i,j)=qq(8,i,j,kh)
              ink2d(i,j)=qq(9,i,j,kh)
              bx2d(i,j)=qq(10,i,j,kh)
              by2d(i,j)=qq(11,i,j,kh)
              bz2d(i,j)=qq(12,i,j,kh)                      
            enddo
          enddo
!- Output
          do i=1,imax
            do j=1,jmax
              write(9,800) dir2dx(i,j),dir2dy(i,j),de2d(i,j), &
                           vx2d(i,j),vy2d(i,j),vz2d(i,j), &
                           pr2d(i,j),bx2d(i,j),by2d(i,j),bz2d(i,j)
            enddo
          enddo
          write(9,*) time,'  time'
!
          deallocate(dir2dx, dir2dy, de2d, vx2d, vy2d, vz2d, &
                     pr2d, ink2d, bx2d, by2d, bz2d, stat=merr)
!
        endif
!
      endif     
!
!=========
!
    elseif(ndim .eq. 3) then !- 3D Output -!
      if(metric .eq. 1) then
!- Cartesian => Cartesian
!- allocate of 2D array -!
        allocate(dir3dx(imax,jmax,kmax), dir3dy(imax,jmax,kmax), &
                 dir3dz(imax,jmax,kmax), de3d(imax,jmax,kmax), &
                 vx3d(imax,jmax,kmax), vy3d(imax,jmax,kmax), &
                 vz3d(imax,jmax,kmax), &
                 pr3d(imax,jmax,kmax), ink3d(imax,jmax,kmax), &
                 bx3d(imax,jmax,kmax), by3d(imax,jmax,kmax), &
                 bz3d(imax,jmax,kmax), stat=merr)      
!
        do k=1,kmax
          do j=1,jmax
            do i=1,imax
              dir3dx(i,j,k)=qq(1,i,j,k)
              dir3dy(i,j,k)=qq(2,i,j,k)
              dir3dz(i,j,k)=qq(3,i,j,k)
!
              de3d(i,j,k)=qq(4,i,j,k)
              vx3d(i,j,k)=qq(5,i,j,k)
              vy3d(i,j,k)=qq(6,i,j,k)
              vz3d(i,j,k)=qq(7,i,j,k)
              pr3d(i,j,k)=qq(8,i,j,k)
              ink3d(i,j,k)=qq(9,i,j,k)
              bx3d(i,j,k)=qq(10,i,j,k)
              by3d(i,j,k)=qq(11,i,j,k)
              bz3d(i,j,k)=qq(12,i,j,k)
            enddo
          enddo
        enddo
!
!- Output
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              write(9,800) dir3dx(i,j,k), dir3dy(i,j,k),dir3dz(i,j,k), &
                           de3d(i,j,k), vx3d(i,j,k),vy3d(i,j,k),vz3d(i,j,k), &
                           pr3d(i,j,k), bx3d(i,j,k),by3d(i,j,k),bz3d(i,j,k)
            enddo
          enddo
        enddo
        write(9,*) time,'  time'
!
        deallocate(dir3dx, dir3dy, dir3dz, de3d, vx3d, vy3d, vz3d, &
                   pr3d, ink3d, bx3d, by3d, bz3d, stat=merr)
!
      endif
!
    endif

    close(9)
  enddo
!
 800  format(1h ,1pe12.4,21(1pe12.4))
!
  stop
end program kamt
