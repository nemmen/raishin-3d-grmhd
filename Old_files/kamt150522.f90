!***********************************************************************
!      RAISHIN code: 3D General Relativistic MHD (3D MPI) version 
!      output program for radiation (output in sphereical coordinates)
!      written by Y. Mizuno
!      ver. 140618
!***********************************************************************
!
program kamt
  use pram, only: imax, jmax, kmax, xmin, xmax, ymin, ymax, zmin, zmax, &
                   ix1, ix2, ix3, pi, R0, hslope
  use outpram !- for input output parameter -!
 
  implicit none

!======================================================================@
!    Difinition for variables
!======================================================================@
!
  integer :: i, j, k, l, m, n
  integer :: mh, it, ih, jh, kh
  integer :: merr
  character*256 :: filename
!
  integer, parameter :: nq=13
!
  real(8), allocatable :: qq(:,:,:,:)
! 
  real(8), allocatable :: dir1dx(:), dir1dy(:), dir1dz(:), &
                          de1d(:), ut1d(:), ux1d(:), uy1d(:), uz1d(:), &
                          pr1d(:), bt1d(:), bx1d(:), by1d(:), bz1d(:)
!
  real(8), allocatable :: dir2dx(:,:), dir2dy(:,:), dir2dz(:,:), &
                          de2d(:,:), &
                          ut2d(:,:), ux2d(:,:), uy2d(:,:), uz2d(:,:), &
                          pr2d(:,:), bt2d(:,:), &
                          bx2d(:,:), by2d(:,:), bz2d(:,:), pom2d(:,:)
!
  real(8), allocatable :: dir3dx(:,:,:), dir3dy(:,:,:), dir3dz(:,:,:), &
                          de3d(:,:,:), &
                          ut3d(:,:,:), ux3d(:,:,:), uy3d(:,:,:), uz3d(:,:,:), &
                          pr3d(:,:,:), &
                          bt3d(:,:,:), bx3d(:,:,:), by3d(:,:,:), bz3d(:,:,:)
!
  real(8), allocatable ::  x1_old(:), x3_old(:), x1_new(:), x3_new(:)

  integer :: nt0, nt, ifile, ini, ino, kni, kno, icheck, kcheck
  real(8) :: time 
  real(8) :: rr, th, ph, vr, vph, vth, br, bph, bth
  real(8) :: dx1o, dx3o, dx1n, dx3n, dr1n, xx0, rr0, x1ostat, r1nstat, &
             x1to, x1tn, x3ostat, x3to, x3tn, tmpth, slp1, slp2, tmp1
!
!- allocate variables -!
  allocate(qq(nq,imax,jmax,kmax),stat=merr)      
!
!
!======================================================================@
!     File Open
!======================================================================@
!
  open(unit=6,file='kamt.outdat',status='unknown')
  open(unit=8,file='structr.outdat',status='unknown',form='unformatted')
!
!======================================================================@
!- initialize -!
!
  nt0=0
  nt=0
  ifile=1
!
!- read output file setting information -!
!
!  read(1,*) nh, ns, ndim, idirec, metric

  write(6,*) ' number of historical section:', nh
  write(6,*) ' dimension:', ndim, ' direction:', idirec 
  write(6,*) ' metric:', metric
!
!**********************************************************************@
!     Main Loop1 Start 
!**********************************************************************@
!
  do mh=1,nh

!======================================================================@
!     Read data from structr.outdat
!======================================================================@
!
    nt0=nt
!
    nt=ns+(mh-1)
    write(6,*) ' nt:',nt
!
!-  Initialize of reading array -!
!
    do k=1,kmax
      do j=1,jmax
        do i=1,imax
          do n=1,nq
            qq(n,i,j,k)=0.d0
          enddo
        enddo
      enddo
    enddo

!- read data -
    if( nt.lt.nt0 ) then
! 
      rewind(8)
!
      do it=1,nt
!
        read(8,end=140) time
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              read(8,end=142) (qq(n,i,j,k),n=1,nq)
            enddo
          enddo
        enddo
!
      enddo
!
    elseif( nt.gt.nt0 ) then
!
      do it=1,nt-nt0
! 
       read(8,end=140) time
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              read(8,end=142) (qq(n,i,j,k),n=1,nq)
            enddo
          enddo
        enddo
!
      enddo

    endif
!
    go to 164
! 
 142 continue
     write(6,*) 'file end found during reading ONE PROFILE'
     write(6,*) ' it =',it,' i =',i,' j =',j,' k =',k
     write(6,*) 'STOP at MAIN'
     stop
!
 140 continue
     write(6,*) 'file end found during reading'
     write(6,*) ' it =',it,' i =',i,' j =',j,' k =',k
     write(6,*) 'continue process'
! 
 164 continue
!
!----------------------------------------------------------------------@
!- Output data for analysis -!
!
!- set output file -!
    write(filename,990) mh
    open( unit=9, file=filename,status='unknown')
990 format('ok',i3.3)  
!
    if(ndim .eq. 1) then !- 1D output -!
!
      if(idirec .eq. 1) then !- i direction -!
!- allocate of 1D array -!
         allocate(dir1dx(imax), de1d(imax), &
                 ut1d(imax), ux1d(imax), uy1d(imax), uz1d(imax), &
                 pr1d(imax), bt1d(imax), bx1d(imax), by1d(imax), bz1d(imax), &
                 stat=merr)
!- set half grid position -!
        jh=(jmax+1)/2
        kh=(kmax+1)/2
        write(*,*) "phi, theta",qq(2,1,jh,kh), qq(3,1,jh,kh)
!
        if(metric .eq. 1 .or. metric .eq. 2) then        
!- Cartesian, clyndrical => Cartesian             
          do i=1,imax
            dir1dx(i)=qq(1,i,jh,kh)
!
            de1d(i)=qq(4,i,jh,kh)
            ut1d(i)=qq(5,i,jh,kh)
            ux1d(i)=qq(6,i,jh,kh)
            uy1d(i)=qq(7,i,jh,kh)
            uz1d(i)=qq(8,i,jh,kh)
            pr1d(i)=qq(9,i,jh,kh)
            bt1d(i)=qq(10,i,jh,kh)
            bx1d(i)=qq(11,i,jh,kh)
            by1d(i)=qq(12,i,jh,kh)
            bz1d(i)=qq(13,i,jh,kh)            
          enddo  
        elseif(metric .eq. 3) then
!- Spherical => Cartesian
          do i=1,imax
            dir1dx(i)=qq(1,i,jh,kh)
!
            de1d(i)=qq(4,i,jh,kh)
            ut1d(i)=qq(5,i,jh,kh)
            ux1d(i)=qq(6,i,jh,kh)
            uy1d(i)=qq(7,i,jh,kh)
            uz1d(i)=-qq(8,i,jh,kh)
            pr1d(i)=qq(9,i,jh,kh)
            bt1d(i)=qq(10,i,jh,kh)
            bx1d(i)=qq(11,i,jh,kh)
            by1d(i)=qq(12,i,jh,kh)
            bz1d(i)=-qq(13,i,jh,kh) 
          enddo
        endif
!- output
!        do i=1,imax
!          write(10+mh,800) dir1dx(i),de1d(i),ux1d(i),uy1d(i),uz1d(i), &
!                           pr1d(i),bx1d(i),by1d(i),bz1d(i)
!        enddo
!        write(10+mh,*) time,'  time'
!
        do i=1,imax
          write(9,800) dir1dx(i),de1d(i),ut1d(i),ux1d(i),uy1d(i),uz1d(i), &
                       pr1d(i),bt1d(i),bx1d(i),by1d(i),bz1d(i)
        enddo
        write(9,*) time,'  time'
!
        deallocate(dir1dx, de1d, ut1d, ux1d, uy1d, uz1d, &
                   pr1d, bt1d, bx1d, by1d, bz1d, stat=merr)
!
      elseif(idirec .eq. 2) then !- j-direction -!
!- allocate of 1D array -!
         allocate(dir1dy(jmax), de1d(jmax), &
                 ut1d(jmax), ux1d(jmax), uy1d(jmax), uz1d(jmax), &
                 pr1d(jmax), bt1d(jmax), bx1d(jmax), by1d(jmax), bz1d(jmax), &
                 stat=merr)
!- set half grid position -!
        ih=(imax+1)/2
        kh=(kmax+1)/2
!
        if(metric .eq. 1) then
!- Cartesian, => Cartesian
          do j=1,jmax
            dir1dy(j)=qq(2,ih,j,kh)
!
            de1d(j)=qq(4,ih,j,kh)
            ut1d(j)=qq(5,ih,j,kh)
            ux1d(j)=qq(6,ih,j,kh)
            uy1d(j)=qq(7,ih,j,kh)
            uz1d(j)=qq(8,ih,j,kh)
            pr1d(j)=qq(9,ih,j,kh)
            bt1d(j)=qq(10,ih,j,kh)
            bx1d(j)=qq(11,ih,j,kh)
            by1d(j)=qq(12,ih,j,kh)
            bz1d(j)=qq(13,ih,j,kh)                   
          enddo
        endif
!- output
        do j=1,jmax
          write(9,800) dir1dy(j),de1d(j),ut1d(j),ux1d(j),uy1d(j),uz1d(j), &
                       pr1d(j),bt1d(j),bx1d(j),by1d(j),bz1d(j)
        enddo
        write(9,*) time,'  time'
!
        deallocate(dir1dy, de1d, ut1d, ux1d, uy1d, uz1d, &
                   pr1d, bt1d, bx1d, by1d, bz1d, stat=merr)
!
      elseif(idirec .eq. 3) then !- k-direction -!
!- allocate of 1D array -!
         allocate(dir1dz(kmax), de1d(kmax), &
                 ut1d(kmax), ux1d(kmax), uy1d(kmax), uz1d(kmax), &
                 pr1d(kmax), bt1d(kmax), bx1d(kmax), by1d(kmax), bz1d(kmax), &
                 stat=merr)
!- set half grid position -!
        ih=(imax+1)/2
        jh=(jmax+1)/2
!
        if(metric .eq. 1) then
!- Cartesian, => Cartesian 
          do k=1,kmax  
            dir1dz(k)=qq(3,ih,jh,k)
!
            de1d(k)=qq(4,ih,jh,k)
            ut1d(k)=qq(5,ih,jh,k)
            ux1d(k)=qq(6,ih,jh,k)
            uy1d(k)=qq(7,ih,jh,k)
            uz1d(k)=qq(8,ih,jh,k)
            pr1d(k)=qq(9,ih,jh,k)
            bt1d(k)=qq(10,ih,jh,k)
            bx1d(k)=qq(11,ih,jh,k)
            by1d(k)=qq(12,ih,jh,k)
            bz1d(k)=qq(13,ih,jh,k)  
          enddo
        endif
!- output
        do k=1,kmax
          write(9,800) dir1dz(k),de1d(k),ut1d(k),ux1d(k),uy1d(k),uz1d(k), &
                       pr1d(k),bt1d(k),bx1d(k),by1d(k),bz1d(k)
        enddo
        write(9,*) time,'  time'
!
        deallocate(dir1dz, de1d, ut1d, ux1d, uy1d, uz1d, &
                   pr1d, bt1d, bx1d, by1d, bz1d, stat=merr)
!
      endif
!
!=========
 !
    elseif(ndim .eq. 2) then !- 2D Output -! 
!
      if(idirec .eq. 1) then !- xz plane -!

!- set half grid position -!
        jh=(jmax+1)/2
!
        if(metric .eq. 1 .or. metric .eq. 2) then
!- Cartesian, Cylindrical => Cartesian
!- allocate of 2D array -!
          allocate(dir2dx(imax,kmax), dir2dz(imax,kmax), de2d(imax,kmax), &
                   ut2d(imax,kmax), ux2d(imax,kmax), &
                   uy2d(imax,kmax), uz2d(imax,kmax), &
                   pr2d(imax,kmax), bt2d(imax,kmax), &
                   bx2d(imax,kmax), by2d(imax,kmax), bz2d(imax,kmax), &
                   stat=merr)
!
          do k=1,kmax
            do i=1,imax
              dir2dx(i,k)=qq(1,i,jh,k)
              dir2dz(i,k)=qq(3,i,jh,k)
!
              de2d(i,k)=qq(4,i,jh,k)
              ut2d(i,k)=qq(5,i,jh,k)
              ux2d(i,k)=qq(6,i,jh,k)
              uy2d(i,k)=qq(7,i,jh,k)
              uz2d(i,k)=qq(8,i,jh,k)
              pr2d(i,k)=qq(9,i,jh,k)
              bt2d(i,k)=qq(10,i,jh,k)
              bx2d(i,k)=qq(11,i,jh,k)
              by2d(i,k)=qq(12,i,jh,k)
              bz2d(i,k)=qq(13,i,jh,k) 
            enddo
          enddo
!- Output
          do k=1,kmax
            do i=1,imax

              write(9,800) dir2dx(i,k),dir2dz(i,k),de2d(i,k), &
                           ut2d(i,k),ux2d(i,k),uy2d(i,k),uz2d(i,k), &
                           pr2d(i,k),bt2d(i,k),bx2d(i,k),by2d(i,k),bz2d(i,k)
            enddo
          enddo
          write(9,*) time,'  time'
!
          deallocate(dir2dx, dir2dz, de2d, ut2d, ux2d, uy2d, uz2d, &
                     pr2d, bt2d, bx2d, by2d, bz2d, stat=merr)
!      
        elseif(metric .eq. 4) then
!
!- Spherical => position in spherical & vectors in cartesian)
!- allocate of 2D array -!
          allocate(dir2dx(imax,kmax), dir2dz(imax,kmax), de2d(imax,kmax), &
                   ut2d(imax,kmax), ux2d(imax,kmax), &
                   uy2d(imax,kmax), uz2d(imax,kmax), &
                   pr2d(imax,kmax), bt2d(imax,kmax), &
                   bx2d(imax,kmax), by2d(imax,kmax), bz2d(imax,kmax), &
                   stat=merr)
!
          do k=1,kmax
            do i=1,imax
              dir2dx(i,k)=qq(1,i,jh,k)
              dir2dz(i,k)=qq(3,i,jh,k)
!              dir2dz(i,k)=qq(3,i,jh,k)*180.d0/pi
              th=qq(3,i,jh,k)
!
              de2d(i,k)=qq(4,i,jh,k)
!
!              vr=qq(5,i,jh,k)
!              vph=qq(6,i,jh,k)
!              vth=qq(7,i,jh,k)

              ut2d(i,k)=qq(5,i,jh,k)
              ux2d(i,k)=qq(6,i,jh,k)
              uy2d(i,k)=qq(7,i,jh,k)
              uz2d(i,k)=qq(8,i,jh,k)
!
!              ux2d(i,k)=vr*sin(th)+vth*cos(th)
!              uy2d(i,k)=vph
!              uz2d(i,k)=vr*cos(th)-vth*sin(th)   

              pr2d(i,k)=qq(9,i,jh,k)
!
              bt2d(i,k)=qq(10,i,jh,k)
              bx2d(i,k)=qq(11,i,jh,k)
              by2d(i,k)=qq(12,i,jh,k)
              bz2d(i,k)=qq(13,i,jh,k)

!              br=qq(10,i,jh,k)
!              bph=qq(11,i,jh,k)
!              bth=qq(12,i,jh,k)
!
!              bx2d(i,k)=br*sin(th)+bth*cos(th)
!              by2d(i,k)=bph
!              bz2d(i,k)=br*cos(th)-bth*sin(th)           
 
            enddo
          enddo
!
          do k=1,kmax
            do i=1,imax
              write(9,800) dir2dx(i,k),dir2dz(i,k),de2d(i,k), &
                           ut2d(i,k),ux2d(i,k),uy2d(i,k),uz2d(i,k), &
                           pr2d(i,k),bt2d(i,k),bx2d(i,k),by2d(i,k),bz2d(i,k)
            enddo
          enddo
          write(9,*) time,'  time'
!
          deallocate(dir2dx, dir2dz, de2d, ut2d, ux2d, uy2d, uz2d, &
                     pr2d, bt2d, bx2d, by2d, bz2d, stat=merr)          
        endif
!
      elseif(idirec .eq. 2) then !- yz-direction -!
!
!- set half grid position -!
        ih=(imax+1)/2
!
        if(metric .eq. 1) then                 
!- Cartesian => Cartesian
!- allocate of 2D array -!
          allocate(dir2dy(jmax,kmax), dir2dz(jmax,kmax), de2d(jmax,kmax), &
                   ut2d(jmax,kmax), ux2d(jmax,kmax), &
                   uy2d(jmax,kmax), uz2d(jmax,kmax), &
                   pr2d(jmax,kmax), bt2d(jmax,kmax), &
                   bx2d(jmax,kmax), by2d(jmax,kmax), bz2d(jmax,kmax), &
                   stat=merr)

          do k=1,kmax
            do j=1,jmax
              dir2dy(j,k)=qq(2,ih,j,k)
              dir2dz(j,k)=qq(3,ih,j,k)
!
              de2d(j,k)=qq(4,ih,j,k)
              ut2d(j,k)=qq(5,ih,j,k)
              ux2d(j,k)=qq(6,ih,j,k)
              uy2d(j,k)=qq(7,ih,j,k)
              uz2d(j,k)=qq(8,ih,j,k)
              pr2d(j,k)=qq(9,ih,j,k)
              bt2d(j,k)=qq(10,ih,j,k)
              bx2d(j,k)=qq(11,ih,j,k)
              by2d(j,k)=qq(12,ih,j,k)
              bz2d(j,k)=qq(13,ih,j,k) 
!                     
            enddo
          enddo
!- Output
          do j=1,jmax
            do k=1,kmax
              write(9,800) dir2dy(j,k),dir2dz(j,k),de2d(j,k), &
                           ut2d(j,k),ux2d(j,k),uy2d(j,k),uz2d(j,k), &
                           pr2d(j,k),bt2d(j,k),bx2d(j,k),by2d(j,k),bz2d(j,k)
            enddo
          enddo
          write(9,*) time,'  time'
!
          deallocate(dir2dy, dir2dz, de2d, ut2d, ux2d, uy2d, uz2d, &
                     pr2d, bt2d, bx2d, by2d, bz2d, stat=merr)
!
        endif
!
      elseif(idirec .eq. 3) then !- xy-direction -!
!- set half grid position -!
        kh=(kmax+1)/2
        if(metric .eq. 1) then
!- Cartesian => Cartesian
!- allocate of 2D array -!
          allocate(dir2dx(imax,jmax), dir2dy(imax,jmax), de2d(imax,jmax), &
                   ut2d(imax,jmax), ux2d(imax,jmax), &
                   uy2d(imax,jmax), uz2d(imax,jmax), &
                   pr2d(imax,jmax), bt2d(imax,jmax), &
                   bx2d(imax,jmax), by2d(imax,jmax), bz2d(imax,jmax), &
                   stat=merr)

          do j=1,jmax
            do i=1,imax
              dir2dx(i,j)=qq(1,i,j,kh)
              dir2dy(i,j)=qq(2,i,j,kh)
!
              de2d(i,j)=qq(4,i,j,kh)
              ut2d(i,j)=qq(5,i,j,kh)
              ux2d(i,j)=qq(6,i,j,kh)
              uy2d(i,j)=qq(7,i,j,kh)
              uz2d(i,j)=qq(8,i,j,kh)
              pr2d(i,j)=qq(9,i,j,kh)
              bt2d(i,j)=qq(10,i,j,kh)
              bx2d(i,j)=qq(11,i,j,kh)
              by2d(i,j)=qq(12,i,j,kh)
              bz2d(i,j)=qq(13,i,j,kh)                      
            enddo
          enddo
!- Output
          do i=1,imax
            do j=1,jmax
              write(9,800) dir2dx(i,j),dir2dy(i,j),de2d(i,j), &
                           ut2d(i,j),ux2d(i,j),uy2d(i,j),uz2d(i,j), &
                           pr2d(i,j),bt2d(i,j),bx2d(i,j),by2d(i,j),bz2d(i,j)
            enddo
          enddo
          write(9,*) time,'  time'
!
          deallocate(dir2dx, dir2dy, de2d, ut2d, ux2d, uy2d, uz2d, &
                     pr2d, bt2d, bx2d, by2d, bz2d, stat=merr)
!
        endif
!
      endif     
!
!=========
!
    elseif(ndim .eq. 3) then !- 3D Output -!
      if(metric .eq. 1) then
!- Cartesian => Cartesian
!- allocate of 2D array -!
        allocate(dir3dx(imax,jmax,kmax), dir3dy(imax,jmax,kmax), &
                 dir3dz(imax,jmax,kmax), de3d(imax,jmax,kmax), &
                 ut3d(imax,jmax,kmax), ux3d(imax,jmax,kmax), &
                 uy3d(imax,jmax,kmax), uz3d(imax,jmax,kmax), &
                 pr3d(imax,jmax,kmax), bt3d(imax,jmax,kmax), &
                 bx3d(imax,jmax,kmax), by3d(imax,jmax,kmax), &
                 bz3d(imax,jmax,kmax), stat=merr)      
!
        do k=1,kmax
          do j=1,jmax
            do i=1,imax
              dir3dx(i,j,k)=qq(1,i,j,k)
              dir3dy(i,j,k)=qq(2,i,j,k)
              dir3dz(i,j,k)=qq(3,i,j,k)
!
              de3d(i,j,k)=qq(4,i,j,k)
              ut3d(i,j,k)=qq(5,i,j,k)
              ux3d(i,j,k)=qq(6,i,j,k)
              uy3d(i,j,k)=qq(7,i,j,k)
              uz3d(i,j,k)=qq(8,i,j,k)
              pr3d(i,j,k)=qq(9,i,j,k)
              bt3d(i,j,k)=qq(10,i,j,k)
              bx3d(i,j,k)=qq(11,i,j,k)
              by3d(i,j,k)=qq(12,i,j,k)
              bz3d(i,j,k)=qq(13,i,j,k)
            enddo
          enddo
        enddo
!
!- Output
!
        do i=1,imax
          do j=1,jmax
            do k=1,kmax
              write(9,800) dir3dx(i,j,k), dir3dy(i,j,k),dir3dz(i,j,k), &
                           de3d(i,j,k), ut3d(i,j,k), ux3d(i,j,k),&
                           uy3d(i,j,k), uz3d(i,j,k), &
                           pr3d(i,j,k), bt3d(i,j,k), bx3d(i,j,k),&
                           by3d(i,j,k),bz3d(i,j,k)
            enddo
          enddo
        enddo
        write(9,*) time,'  time'
!
        deallocate(dir3dx, dir3dy, dir3dz, de3d, ut3d, ux3d, uy3d, uz3d, &
                   pr3d, bt3d, bx3d, by3d, bz3d, stat=merr)
!
      endif
!
    endif

    close(9)
  enddo
!
 800  format(1h ,1pe12.4,21(1pe12.4))
!
  stop
end program kamt
