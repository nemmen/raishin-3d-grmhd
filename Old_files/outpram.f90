module outpram
!
! Input file for output data (for data analysis) 
!
  implicit none

  integer, parameter :: lmax=128, mmax=7, nmax=128 !- Grid number for image -!
  real(8), parameter :: xnmin=0.0d0, xnmax=40.0d0 !- min & max size of x
  real(8), parameter :: ynmin=-0.1d0, ynmax=0.1d0 !- min & max size of y  
  real(8), parameter :: znmin=-40.d0, znmax=40.0d0 !- min & max size of z
  integer, parameter :: nh=2 !- total output file number -!
  integer, parameter :: ns=1 !- start (reading) output file number -!
  integer, parameter :: ndim=2 !- output dimension -!
  integer, parameter :: idirec=1 !- output direction or surface -!
  integer, parameter :: metric=4 !- metric (car, cyl, sph)

! 
!!!!!!! Parameter explanation !!!!!!!
! Output dimesion 
!
!   ndim=1 : 1D output
!   ndim=2 : 2D output
!   ndim=3 : 3D output
!
! Output direction
!
!   For ndim=1 case (1D output)
! 
!     idirec=1 : x-direction   
!     idirec=2 : y-direction  
!     idirec=3 : z-direction  
!
!   For ndim=2 case (2D output)
! 
!     idirec=1 : xz-plane   
!     idirec=2 : yz-plane  
!     idirec=3 : xy-plane  
!
! Metric (simulation used)
!
!   metric=1 : Cartesian
!   metric=2 : Cylindrical
!   metric=3 : Spherical  


end module outpram

