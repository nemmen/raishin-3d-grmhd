!-----------------------------------------------------------------------
subroutine recov(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1, &
                 is1,ie1,js1,je1,ks1,ke1)
!-----------------------------------------------------------------------
!     Calculation of primitive variables from conserved variables
!     
  use pram, only : imax, jmax, kmax, nv, iwvec, iter, ieos
  implicit none
!
  integer :: nm1, is1, ie1, js1, je1, ks1, ke1
  real(8) :: uu(nv,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: uri(nv,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: gcon(0:3,0:3,is1:ie1,js1:je1,ks1:ke1), &
             gcov(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: detg(is1:ie1,js1:je1,ks1:ke1)

  real(8) :: x1(imax), x2(jmax), x3(kmax)
!  
!- 2 variables (W, v^2) following Noble et al. (2006)  
  if( iwvec.eq.1 ) then
    call recov1(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1,is1,ie1,js1,je1,ks1,ke1)
!- 1 variable (W) following Mignone & McKinney (2007)
  elseif( iwvec.eq.2 ) then
     call recov2(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1,is1,ie1,js1,je1,ks1,ke1)
!- 1 variables (W) following Noble et al. (2006)  
  elseif( iwvec.eq.3 ) then
     call recov3(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1,is1,ie1,js1,je1,ks1,ke1)
!- 1 variables (v^2) for polytropic EoS  
  elseif( iwvec.eq.4 .and. ieos .eq. 3) then
    call recov4(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1,is1,ie1,js1,je1,ks1,ke1)
  else
    write(*,*) "choose wrong inversion procedure, iwvec, ieos=", iwvec, ieos
    stop 
  endif
!
  return
end subroutine recov
!
!---------------------------------------------------------------------@
subroutine recov1(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1,is1,ie1,js1,je1,ks1,ke1)
!---------------------------------------------------------------------@
!- Following Noble et al. (2006), 2-variables inversion procedure -!
!- (only ideal EoS)
  
  use pram, only : imax, jmax, kmax, nv, gam, c0, ieos, iter, dmin, pmin, &
                   kpol
  implicit none
!
  integer :: i, j, k, m, n, nnn, nm1, is1, ie1, js1, je1, ks1, ke1
  integer :: merr, igue, irecov

  real(8) :: uu(nv,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: uri(nv,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: gcon(0:3,0:3,is1:ie1,js1:je1,ks1:ke1), &
             gcov(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: detg(is1:ie1,js1:je1,ks1:ke1)

  real(8) :: x1(imax), x2(jmax), x3(kmax)

  real(8), allocatable :: gcov1(:,:), gcon1(:,:)
  real(8), allocatable :: qcon(:), qcov(:), bcon(:), bcov(:), &
                          oncov(:), oncon(:) 
  real(8), allocatable :: util(:), util1(:), qtcon(:)
      
  real(8) :: safty, saftybig, gam1, gam2, dv, alpha1, detg1 
  real(8) :: dro, ro, pp, ro1, pp1, roh, roe, bsq, qdotb, qdotbsq, qdotn, &
             qsq, qtsq, utsq, gfsq, gf, vsq, wg, wsq, xsq, qsq1, &
             delta, deltaend, df, f, xxn, yyn, xo, yo, w2, w3, gtmp, ptmp, &
             f1, g1, dfx, dfy, dgx, dgy, det, dx, dy, dpdw, dpdvsq 
  real(8) :: tmp1a, tmp1b, tmp1c, tmp1d, wgp, wgm, vsq1, tmp3a, tmp3b, tmp4a
  real(8) :: rin
!
!- allocate variables -!
  allocate(gcov1(0:3,0:3), gcon1(0:3,0:3), stat=merr)
  allocate(qcon(0:3), qcov(0:3), bcon(0:3), bcov(0:3), &
           oncov(0:3), oncon(0:3), util(1:3), util1(1:3), qtcon(1:3), stat=merr)
!
!--- define variable of safe number ----
      safty = 1.0d-10
      saftybig = 1.0d10
      dv=1.0d-5
      
      gam1=gam/(gam-1.0)
      gam2=1.0/gam1
      wg=0.d0

      igue=1
!
      rin=15.d0

!=====================================================================@
!
! ----  Calculation of variables ---
!
  do k=ks1+nm1,ke1-nm1
    do j=js1+nm1,je1-nm1
      do i=is1+nm1,ie1-nm1
!- copy of metric terms -!
        do m=0,3
          do n=0,3
            gcov1(m,n)=gcov(m,n,i,j,k)
            gcon1(m,n)=gcon(m,n,i,j,k)
          enddo
        enddo
!
        alpha1=1./sqrt(-gcon1(0,0))
!
        detg1=detg(i,j,k)
      
        dro=alpha1*uu(1,i,j,k)*(1./detg1)

!        qcov(0)=alpha1*(uu(5,i,j,k)+uu(1,i,j,k))*(1./detg1)
        qcov(0)=alpha1*(uu(5,i,j,k)-uu(1,i,j,k))*(1./detg1)
!        qcov(0)=alpha1*uu(5,i,j,k)*(1./detg1)
        qcov(1)=alpha1*uu(2,i,j,k)*(1./detg1)
        qcov(2)=alpha1*uu(3,i,j,k)*(1./detg1)
        qcov(3)=alpha1*uu(4,i,j,k)*(1./detg1)

        tmp4a=alpha1*uu(5,i,j,k)*(1./detg1)
        
        bcon(0)=0.d0
        bcon(1)=alpha1*uu(7,i,j,k)*(1./detg1)
        bcon(2)=alpha1*uu(8,i,j,k)*(1./detg1)
        bcon(3)=alpha1*uu(9,i,j,k)*(1./detg1)
         
        ro=uri(1,i,j,k)
        util(1)=uri(2,i,j,k)
        util(2)=uri(3,i,j,k)
        util(3)=uri(4,i,j,k)
!        vcon(1)=uri(2,i,j,k)
!        vcon(2)=uri(3,i,j,k)
!        vcon(3)=uri(4,i,j,k)
        pp=uri(5,i,j,k)
        
        ro1=uri(1,i,j,k)
        util1(1)=uri(2,i,j,k)
        util1(2)=uri(3,i,j,k)
        util1(3)=uri(4,i,j,k)
!        vcon1(1)(i,j,k)=uri(2,i,j,k)
!        vcon1(2)(i,j,k)=uri(3,i,j,k)
!        vcon1(3)(i,j,k)=uri(4,i,j,k)
        pp1=uri(5,i,j,k)
!
        if(ieos .eq. 0 .or. ieos .eq. 3) then
          roh=ro+(gam/(gam-1.0))*pp
        elseif(ieos .eq. 1) then
          roh=(5./2.)*pp &
              +sqrt((9./4.)*pp**2+ro**2)
        elseif(ieos .eq. 2) then
          roe=(3./2.)*(pp+((3.*pp**2) &
              /(2.0*ro+sqrt(2.*pp**2+4.*ro**2)) ))
          roh=ro+roe+pp
        endif

!- cal of covariant 3-magnetic field -! 
        call lower(bcon,bcov,gcov1)
!- cal of contravariant 4-q -!
        call upper(qcov,qcon,gcon1)
!- cal of B-field square (3-vector) -!
        bsq=bcon(1)*bcov(1)+bcon(2)*bcov(2)+bcon(3)*bcov(3)
!- cal of q*B -!
        qdotb=qcov(0)*bcon(0)+qcov(1)*bcon(1)+qcov(2)*bcon(2)+qcov(3)*bcon(3)
!- cal of (Q*B)^2 -!
        qdotbsq=qdotb*qdotb
!- set of covariant 4-vector n -!
        oncov(0)=-1.d0*alpha1
        oncov(1)=0.d0
        oncov(2)=0.d0
        oncov(3)=0.d0
!- cal of contravariant 4-vector n -!
        call upper(oncov,oncon,gcon1)
!- cal of Q*n -!
        qdotn=qcon(0)*oncov(0)
!
!- cal of square of Q -!
        qsq=0.d0
        qsq=qcov(0)*qcon(0)+qcov(1)*qcon(1) &
           +qcov(2)*qcon(2)+qcov(3)*qcon(3)
!- cal of Q^2+Q*n -!
        qtsq=qsq+(qdotn*qdotn)
!
!        qsq1=0.d0
!        qsq1=qcov(1)*qcon(1)+qcov(2)*qcon(2)+qcov(3)*qcon(3)
!
!
!- Calculate W from last timestep and use for inital guess -!      
!
!- calculation of 4-velocity square 
        utsq=0.d0
!        vsq=0.d0
        do m=1,3
          do n=1,3
            utsq=utsq+gcov1(m,n)*util(m)*util(n)
!            vsq=vsq+gcov1(m,n)*util(m)*util(n)
          enddo
        enddo
!
        if(utsq .lt. 0.d0 .and. abs(utsq) .le. safty) then
          utsq=abs(utsq)
        endif
        if(utsq .lt. 0.d0 .or. abs(utsq) .gt. saftybig) then
          utsq=1.d9
        endif 
!- calculation of Lorentz factor -!
        gfsq=1.+utsq
        gf=sqrt(gfsq)
!        gf=1./sqrt(1.-vsq)
!        gfsq=gf**2
        vsq=(gfsq-1.)*(1./gfsq)
!- calculation of W -!      
!        if(vsq .lt. 0.9d0 .and. bsq/roh .lt. 1.d0) then 
! 
!          ro=dro*(1./gf)
!          roh=ro+gam1*pp
!          wg=abs(gfsq*roh)
!
!        else
          
!          if(igue .eq. 1) then
!            vsq1=1.d0
!          else
            vsq1=vsq
!          endif
!            
          tmp1a=4.-vsq1
          tmp1b=4.*(bsq+qdotn)
          tmp1c=qtsq+bsq*bsq+2.*bsq*qdotn
          tmp1d=(tmp1b*tmp1b)-4.*tmp1a*tmp1c
          if(tmp1d .le. 0.d0) then
            tmp1d=abs(tmp1d)
          endif
          wgp=(-tmp1b+sqrt(tmp1d))*(1./(2.*tmp1a))
          wgm=(-tmp1b-sqrt(tmp1d))*(1./(2.*tmp1a))
!
         if(tmp1d .le. 0.d0) then
           write(*,*) "inside sqrt < 0 for cal wg", tmp1b, tmp1c
          endif
!
          if(wgp .gt. 0.d0) then
            wg=wgp
          elseif(wgm .ge. 0.d0) then
            wg=wgm
          endif
          
!        endif
!
! Make sure that W is large enough so that v^2 < 1
!       
        tmp3a=(wg*wg*wg)*(wg+2.*bsq)-qdotbsq*(2.*wg+bsq)
        tmp3b=(wg*wg)*(qtsq-bsq*bsq)

        do nnn=1,iter
          if(tmp3a .le. tmp3b) then
             write(*,*) "added wg at i,j,k=",i,j,k
             wg= abs(1.5*wg)
             tmp3a=(wg*wg*wg)*(wg+2.*bsq)-qdotbsq*(2.*wg+bsq)
             tmp3b=(wg*wg)*(qtsq-bsq*bsq)
          endif
        enddo
!
! Calculate W & v^2 for initial guess
!
        wsq=wg*wg
        xsq=(bsq+wg)*(bsq+wg)
        vsq=abs((wsq*qtsq+qdotbsq*(bsq+2.*wg))*(1./(wsq*xsq)))
!       
        if(vsq .gt. 1.d0) then
          write(6,*) 'vsq > 1.0' 
          vsq=1.d0-dv
        endif
!
!=====================================================================@
!- Newton Rapson calculation -!

!- set parameter  -!

        delta=1.d0
        deltaend = 1.d-8
        df=1.d0
        f=1.d0
        irecov=0

        xxn=wg
        yyn=vsq
        xo=wg
        yo=vsq

!- Newton-Raphson routine start -!
         
        do nnn=1,iter

          if(abs(delta) .ge. deltaend .and. irecov .eq. 0) then
           
            if(nnn .eq. iter) then
              irecov=1

            else
              wg=xxn
              vsq=yyn

              wsq=wg*wg
              w3=wsq*wg
              gtmp=1.-vsq
              if(gtmp .lt. 0.d0) then
                gtmp=safty
              endif   

              if(ieos .eq. 0) then !- gamma-law EoS -!
                ptmp=gam2*(wg*gtmp-dro*sqrt(gtmp))
              elseif(ieos .eq. 3) then !- polytropic EoS -!   
                ptmp=kpol*(dro**gam)*sqrt(gtmp)
              endif
              
              f1=qtsq-vsq*(wg+bsq)*(wg+bsq)+(qdotbsq*(bsq+2.*wg))*(1./wsq)
              g1=-qdotn-0.5*bsq*(1.+vsq)+0.5*(qdotbsq*(1./wsq))-wg+ptmp

              if(ieos .eq. 0) then !- gamma-law EoS -!
                dpdw=gam2*gtmp
                dpdvsq=-gam2*wg+0.5*gam2*dro*(1./(sqrt(gtmp)))
              elseif(ieos .eq. 3) then !- polytropic EoS -!
                dpdw=0.d0
                dpdvsq=-0.5*kpol*(dro**gam)*(1./(sqrt(gtmp)))  
              endif
              
              dfx=-2.*(wg+bsq)*(vsq+qdotbsq*(1./w3))
              dfy=-((wg+bsq)*(wg+bsq))
              dgx=-1.-(qdotbsq*(1./w3))+dpdw
              dgy=-0.5*bsq+dpdvsq
              
              det=dfx*dgy-dfy*dgx

              if(det .eq. 0.d0) then
                write(*,*) "det=0 in recov1 at", i, j, k
                det=1.d0 
              endif   
              dx=(-dgy*f1+dfy*g1)*(1./det)
              dy=(dgx*f1-dfx*g1)*(1./det)

              df=-f1*f1-g1*g1
              f=-0.5*df
            
              delta=0.5*(abs(dx)+abs(dy))
!              delta=abs(dx)
 
              xo=wg
              yo=vsq
            
              xxn=wg+dx
              yyn=vsq+dy
!
              if(xxn .lt. 0.d0) then
!                irecov=2
                 xxn=abs(xxn)
              endif
!         
              if(xxn .gt. 1.d8) then
                xxn=xo
              endif
!            
              if(yyn .gt. 1.d0) then
                dv=1.0d-5
                yyn=1.0-dv
              endif
             
              if(yyn .lt. 0.d0) then
                yyn=0.d0
              endif  

            endif
          endif

        enddo
!
!- Newton-Raphson routine end -!
!
        if(irecov .eq. 0) then
          wg=xxn
          vsq=yyn
!
          gtmp=sqrt(1.-vsq)
          gf=1./gtmp
          ro=dro*gtmp
!
          w2=wg*(1.-vsq)
          if(ieos .eq. 3) then !-polytropic EoS
            pp=kpol*ro**gam
          elseif(ieos .eq. 0) then !- gamma-law EoS 
            pp=gam2*(w2-ro)
          endif
            
          if(ro .lt. 0.d0) then
            write(*,*) 'negative density', ro, 'at i, j, k', i, j ,k
!            ro=dmin*((x1(i)/rin)**(-3./2.))
            ro=dmin
          endif
          if (pp .lt. 0.d0) then
!            irecov=3
            write(*,*) 'negative pressure', pp, 'at i, j, k', i, j ,k
!            pp=pmin*((x1(i)/rin)**(-5./2.))
            pp=pmin
          endif

!          if(irecov .ne. 3) then
            qtcon(1)=qcon(1)+oncon(1)*qdotn
            qtcon(2)=qcon(2)+oncon(2)*qdotn
            qtcon(3)=qcon(3)+oncon(3)*qdotn

            util(1)=(gf*(1./(wg+bsq)))*(qtcon(1)+qdotb*bcon(1)*(1./wg))
            util(2)=(gf*(1./(wg+bsq)))*(qtcon(2)+qdotb*bcon(2)*(1./wg))
            util(3)=(gf*(1./(wg+bsq)))*(qtcon(3)+qdotb*bcon(3)*(1./wg))

!            vcon(1)=(1./(xxn+bsq))*(qtcon(1)+qdotb*bcon(1)/wg)
!            vcon(2)=(1./(xxn+bsq))*(qtcon(2)+qdotb*bcon(2)/wg)
!            vcon(3)=(1./(xxn+bsq))*(qtcon(3)+qdotb*bcon(3)/wg)
!           endif
        endif
                 
        wg=xxn
        vsq=yyn   

!---------------------------------------------------------------------
!- Error message -!                
        if(irecov .eq. 1) then
          write(6,*) ' >> Not convergence in recov1d' &
                    ,' at i, j, k:',i, j, k
          write(6,*) ' >> delta =', delta &
                    ,', at x1, x2, x3:',x1(i),x2(j),x3(k)
         
        elseif(irecov .eq. 2) then
          write(6,*) ' >> wg  < 0 in recov1d' &
                    ,' at i, j, k:',i, j, k
          write(6,*) ' >> wg', wg &
                    ,', at x1, x2, x3:',x1(i),x2(j),x3(k)

!         elseif(irecov .eq. 3) then
!           write(6,*) ' >> ro or pr < 0 in recov1d' &
!                    ,' at i, j, k:',i, j, k
!           write(6,*) ' >> ro, pr', ro, pp &
!                     ,' at x1, x2, x3: ',x1(i),x2(j),x3(k)
        endif
!
!---------------------------------------------------------------------@
!   Set Primitive variables
!---------------------------------------------------------------------@
!
        if(irecov .eq. 0) then
         
          uri(1,i,j,k)=ro
          uri(2,i,j,k)=util(1)
          uri(3,i,j,k)=util(2)
          uri(4,i,j,k)=util(3)
!          uri(2,i,j,k)=vcon(1)
!          uri(3,i,j,k)=vcon(2)
!          uri(4,i,j,k)=vcon(3)
          uri(5,i,j,k)=pp
          uri(6,i,j,k)=1.d0
          uri(7,i,j,k)=uu(7,i,j,k)*(1./detg1)
          uri(8,i,j,k)=uu(8,i,j,k)*(1./detg1)
          uri(9,i,j,k)=uu(9,i,j,k)*(1./detg1)
        
        else
         
          uri(1,i,j,k)=ro1
          uri(2,i,j,k)=util1(1)
          uri(3,i,j,k)=util1(2)
          uri(4,i,j,k)=util1(3)
!          uri(2,i,j,k)=vcon1(1)
!          uri(3,i,j,k)=vcon1(2)
!          uri(4,i,j,k)=vcon1(3)
          uri(5,i,j,k)=pp1
          uri(6,i,j,k)=1.d0
          uri(7,i,j,k)=uu(7,i,j,k)*(1./detg1)
          uri(8,i,j,k)=uu(8,i,j,k)*(1./detg1)
          uri(9,i,j,k)=uu(9,i,j,k)*(1./detg1)
         
        endif
!
      enddo
    enddo
  enddo
!
!=====================================================================@
!
  deallocate( gcov1, gcon1, qcon, qcov, bcon, bcov, &
              oncov, oncon, util, util1, qtcon, stat=merr )
!
  return
end subroutine recov1
!
!---------------------------------------------------------------------@
subroutine recov2(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1,is1,ie1,js1,je1,ks1,ke1)
!---------------------------------------------------------------------@
!- Following Mignone & McKinney (2007) 1 variables inversion procedure -!
!- with variable EoS  

  use pram, only : imax, jmax, kmax, nv, gam, c0, ieos, iter, dmin, pmin, &
                   kpol
  implicit none
!
  integer :: i, j, k, m, n, nnn, nm1, is1, ie1, js1, je1, ks1, ke1
  integer :: merr, igue, irecov

  real(8) :: uu(nv,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: uri(nv,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: gcon(0:3,0:3,is1:ie1,js1:je1,ks1:ke1), &
             gcov(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: detg(is1:ie1,js1:je1,ks1:ke1)

  real(8) :: x1(imax), x2(jmax), x3(kmax)

  real(8), allocatable :: gcov1(:,:), gcon1(:,:)
  real(8), allocatable :: qcon(:), qcov(:), bcon(:), bcov(:), &
                          oncov(:), oncon(:) 
  real(8), allocatable :: util(:), util1(:), qtcon(:)
      
  real(8) :: safty, saftybig, gam1, gam2, dv, alpha1, detg1 
  real(8) :: dro, ro, pp, ro1, pp1, roh, roe, bsq, qdotb, qdotbsq, qdotn, &
             qsq, qtsq, utsq, gfsq, gf, vsq, usq, wg, wsq, xsq, &
             delta, deltaend, df, f, xxn, xo, wgd, w3, gtmp, &
             xi, dpdxi, dpdro, dvdwd, dxidwd, drodwd, dpdwd
  real(8) :: tmp1a, tmp1b, tmp1c, tmp1d, wgp, wgm, vsq1, tmp3a, tmp3b, &
             tmp4a, tmp4b, tmp4c, tmp4d, tmp5a, tmp5b, tmp5c, tmp5d, &
             vsqo
!
!- allocate variables -!
  allocate(gcov1(0:3,0:3), gcon1(0:3,0:3), stat=merr)
  allocate(qcon(0:3), qcov(0:3), bcon(0:3), bcov(0:3), &
           oncov(0:3), oncon(0:3), util(1:3), util1(1:3), qtcon(1:3), stat=merr)
!
!--- define variable of safe number ----
      safty = 1.0d-10
      saftybig = 1.0d10
      dv=1.0d-5
      
      gam1=gam/(gam-1.0)
      gam2=1.0/gam1
      wg=0.d0

      igue=1

!=====================================================================@
!
! ----  Calculation of variables ---
!
  do k=ks1+nm1,ke1-nm1
    do j=js1+nm1,je1-nm1
      do i=is1+nm1,ie1-nm1
!- copy of metric terms -!
        do m=0,3
          do n=0,3
            gcov1(m,n)=gcov(m,n,i,j,k)
            gcon1(m,n)=gcon(m,n,i,j,k)
          enddo
        enddo
!
        alpha1=1./sqrt(-gcon1(0,0))
!
        detg1=detg(i,j,k)
      
        dro=alpha1*uu(1,i,j,k)/detg1

!        qcov(0)=alpha1*(uu(5,i,j,k)+uu(1,i,j,k))*(1./detg1)
        qcov(0)=alpha1*(uu(5,i,j,k)-uu(1,i,j,k))*(1./detg1)
!        qcov(0)=alpha1*uu(5,i,j,k)*(1./detg1)
        qcov(1)=alpha1*uu(2,i,j,k)*(1./detg1)
        qcov(2)=alpha1*uu(3,i,j,k)*(1./detg1)
        qcov(3)=alpha1*uu(4,i,j,k)*(1./detg1)

        tmp4a=alpha1*uu(5,i,j,k)*(1./detg1)
        
        bcon(0)=0.d0
        bcon(1)=alpha1*uu(7,i,j,k)*(1./detg1)
        bcon(2)=alpha1*uu(8,i,j,k)*(1./detg1)
        bcon(3)=alpha1*uu(9,i,j,k)*(1./detg1)
         
        ro=uri(1,i,j,k)
        util(1)=uri(2,i,j,k)
        util(2)=uri(3,i,j,k)
        util(3)=uri(4,i,j,k)
!        vcon(1)=uri(2,i,j,k)
!        vcon(2)=uri(3,i,j,k)
!        vcon(3)=uri(4,i,j,k)
        pp=uri(5,i,j,k)
        
        ro1=uri(1,i,j,k)
        util1(1)=uri(2,i,j,k)
        util1(2)=uri(3,i,j,k)
        util1(3)=uri(4,i,j,k)
!        vcon1(1)(i,j,k)=uri(2,i,j,k)
!        vcon1(2)(i,j,k)=uri(3,i,j,k)
!        vcon1(3)(i,j,k)=uri(4,i,j,k)
        pp1=uri(5,i,j,k)
!
        if(ieos .eq. 0 .or. ieos .eq. 3) then
          roh=ro+(gam/(gam-1.0))*pp
        elseif(ieos .eq. 1) then
          roh=(5./2.)*pp &
              +sqrt((9./4.)*pp**2+ro**2)
        elseif(ieos .eq. 2) then
          roe=(3./2.)*(pp+((3.*pp**2) &
              /(2.0*ro+sqrt(2.*pp**2+4.*ro**2)) ))
          roh=ro+roe+pp
        endif

!- cal of covariant 3-magnetic field -! 
        call lower(bcon,bcov,gcov1)
!- cal of contravariant 4-q -!
        call upper(qcov,qcon,gcon1)
!- cal of B-field square (3-vector) -!
        bsq=bcon(1)*bcov(1)+bcon(2)*bcov(2)+bcon(3)*bcov(3)
!- cal of q*B -!
        qdotb=qcov(0)*bcon(0)+qcov(1)*bcon(1)+qcov(2)*bcon(2)+qcov(3)*bcon(3)
!- cal of (Q*B)^2 -!
        qdotbsq=qdotb*qdotb
!- set of covariant 4-vector n -!
        oncov(0)=-1.d0*alpha1
        oncov(1)=0.d0
        oncov(2)=0.d0
        oncov(3)=0.d0
!- cal of contravariant 4-vector n -!
        call upper(oncov,oncon,gcon1)
!- cal of Q*n -!
        qdotn=qcon(0)*oncov(0)
!
!- cal of square of Q -!
        qsq=0.d0
        qsq=qcov(0)*qcon(0)+qcov(1)*qcon(1) &
           +qcov(2)*qcon(2)+qcov(3)*qcon(3)
!- cal of Q^2+Q*n -!
        qtsq=qsq+(qdotn*qdotn)
!
!- Calculate W from last timestep and use for inital guess -!      
!
!- calculation of 4-velocity square 
        utsq=0.d0
!        vsq=0.d0
        do m=1,3
          do n=1,3
            utsq=utsq+gcov1(m,n)*util(m)*util(n)
!            vsq=vsq+gcov1(m,n)*util(m)*util(n)
          enddo
        enddo
!
        if(utsq .lt. 0.d0 .and. abs(utsq) .le. safty) then
          utsq=abs(utsq)
        endif
        if(utsq .lt. 0.d0 .or. abs(utsq) .gt. saftybig) then
          utsq=1.d9
        endif 
!- calculation of Lorentz factor -!
        gfsq=1.+utsq
        gf=sqrt(gfsq)
!        gf=1./sqrt(1.-vsq)
!        gfsq=gf**2
        vsq=(gfsq-1.)*(1./gfsq)
!- calculation of W -!      
!        if(vsq .lt. 0.9d0 .and. bsq/roh .lt. 1.d0) then 
! 
!          ro=dro*(1./gf)
!          roh=ro+gam1*pp
!          wg=abs(gfsq*roh)
!
!        else
          
!          if(igue .eq. 1) then
!            vsq1=1.d0
!          else
            vsq1=vsq
!          endif
!            
          tmp1a=4.-vsq1
          tmp1b=4.*(bsq+qdotn)
          tmp1c=qtsq+bsq*bsq+2.*bsq*qdotn
          tmp1d=(tmp1b*tmp1b)-4.*tmp1a*tmp1c
          if(tmp1d .le. 0.d0) then
            tmp1d=abs(tmp1d)
          endif
          wgp=(-tmp1b+sqrt(tmp1d))*(1./(2.*tmp1a))
          wgm=(-tmp1b-sqrt(tmp1d))*(1./(2.*tmp1a))
!
         if(tmp1d .le. 0.d0) then
           write(*,*) "inside sqrt < 0 for cal wg", tmp1b, tmp1c
         endif
!
          if(wgp .gt. 0.d0) then
            wg=wgp
          elseif(wgm .ge. 0.d0) then
            wg=wgm
          endif
!         
!        endif
!
! Make sure that W is large enough so that v^2 < 1
!       
        tmp3a=(wg*wg*wg)*(wg+2.*bsq)-qdotbsq*(2.*wg+bsq)
        tmp3b=(wg*wg)*(qtsq-bsq*bsq)
!
        do nnn=1,iter
          if(tmp3a .le. tmp3b) then
             write(*,*) "added wg at i,j,k=",i,j,k
             wg= abs(1.5*wg)
             tmp3a=(wg*wg*wg)*(wg+2.*bsq)-qdotbsq*(2.*wg+bsq)
             tmp3b=(wg*wg)*(qtsq-bsq*bsq)
          endif
        enddo
!
!=====================================================================@
!- Newton Rapson calculation -!

!- set parameter  -!

        delta=1.d0
        deltaend =1.d-8
        df=1.d0
        f=1.d0
        irecov=0

        wgd=wg-dro
        xxn=wgd
        xo=wgd
!        xxn=wg
!        xo=wg
        
!- Newton-Raphson routine start -!

        do nnn=1,iter

          if(abs(delta) .ge. deltaend .and. irecov .eq. 0) then
           
            if(nnn .eq. iter) then
              irecov=1

           else
              vsqo=vsq
              wgd=xxn
              wg=wgd+dro
!              wg=xxn
!              
!- Calculation of velocity and Lorentz factor -!
              wsq=wg*wg
              w3=wsq*wg
              xsq=(bsq+wg)*(bsq+wg)
              vsq=abs((wsq*qtsq+qdotbsq*(bsq+2.*wg))*(1./(wsq*xsq)))
!       
              if(vsq .gt. 1.d0) then
                write(6,*) 'vsq > 1.0' 
                vsq=1.d0-dv
!                vsq=vsqo
             endif
!         
              gtmp=sqrt(1.-vsq)
              gf=1./gtmp
!              usq=vsq/(1.-vsq)
!              gf=sqrt(1.+usq)
              gfsq=gf*gf
!- Calculation of density, xi -!
              ro=dro*gtmp
!
!              xi=(wgd*(1./gfsq))-(dro*usq)*(1./((1.0+gf)*gfsq))
              xi=(wg-dro*gf)*(1./gfsq)
!- Calculation of pressure from EoS -!              
              if(ieos .eq. 0) then
                pp=gam2*xi
              elseif(ieos .eq. 1 .or. ieos .eq. 2) then
                tmp4a=2.*xi*(xi+2.*ro)
                tmp4b=5.*(xi+ro)+sqrt(9.*(xi**2)+18.*ro*xi+25.*(ro**2))
                pp=tmp4a*(1./tmp4b)
              endif
!
!- calculation of dp/dxi and dp/dro -!              
              if(ieos .eq. 0) then !- gamma-law EoS -!
                dpdxi=gam2
                dpdro=0.d0
              elseif(ieos .eq. 1 .or. ieos .eq. 2) then !- TM EoS -!
                dpdxi=(2.*xi+2.*ro-5.*pp)*(1./(5.*ro+5.*xi-8.*pp))
                dpdro=(2.*xi-5.*pp)*(1./(5.*ro+5.*xi-8.*pp))
              endif
!- calculation of dv^2/dw -!
              tmp1a=(bsq+wg)
              tmp1b=(bsq+wg)*(bsq+wg)*(bsq+wg)
              if(qtsq .eq. 0.d0) then
                tmp1c=0.d0
              else   
                tmp1c=-2.*qtsq*(1./tmp1b)
              endif   
              if(qdotbsq .eq. 0.d0) then
                tmp1d=0.d0
              else   
                tmp1d=-2.*qdotbsq*(3.*wg*tmp1a+bsq*bsq)*(1./(tmp1b*w3))
              endif   
              
              dvdwd=tmp1c+tmp1d
!- calculation of dxi/dw and dro/dw -!          
              dxidwd=(1./gfsq)-0.5*gf*(dro+2.*gf*xi)*dvdwd
              drodwd=-0.5*dro*gf*dvdwd
!              drodwd=0.5*dro*gf*dvdwd
!- calculation of dp/dw -!              
              dpdwd=dpdxi*dxidwd+dpdro*drodwd
          
! --- calculation of f(W') and df(W')/dW' ----
!
              tmp5a=bsq*qtsq-qdotbsq
              tmp5b=bsq+wgd+dro
!              tmp5b=bsq+wg
!            
              if(tmp5a .eq. 0.d0) then
                tmp5c=0.d0
                tmp5d=0.d0
              else
                tmp5c=0.5*tmp5a*(1./(tmp5b*tmp5b))
                tmp5d=tmp5a*(1./(tmp5b*tmp5b*tmp5b))
              endif
!
!- Mignone & McKinney
              f=-(qdotn+dro)-wgd+pp-0.5*bsq-tmp5c
!              f=-qdotn-wg+pp-0.5*bsq-tmp5c
              df=-1.+dpdwd+tmp5d
!
!- Noble et al (2006)
!              f=-wg-0.5*bsq*(1.+vsq)+(0.5*qdotbsq/wsq)-qdotn+pp
!              df=-1.+dpdwd-(qdotbsq/w3)-0.5*bsq*dvdwd
!             
              delta=f/df                            
!             
              xo=wgd
!              xo=wg
              xxn=wgd-delta
!              xxn=wg-delta
!  
              if(xxn .lt. 0.d0) then
!                irecov=2
                 xxn=abs(xxn)
              endif
!        
              if(xxn .gt. 1.d8) then
                xxn=xo
              endif             
!
            endif
          endif

        enddo
!
!- Newton-Raphson routine end -!
!
        if(irecov .eq. 0) then
          wgd=xxn
          wg=wgd+dro
!          wg=xxn
          wsq=wg*wg
          xsq=(bsq+wg)*(bsq+wg)
          vsq=abs((wsq*qtsq+qdotbsq*(bsq+2.*wg))*(1./(wsq*xsq)))
!       
          if(vsq .gt. 1.d0) then
            write(6,*) 'vsq > 1.0' 
            vsq=1.d0-dv
!            vsq=vsqo
         endif
!         
          gtmp=sqrt(1.-vsq)
          gf=1./gtmp
!          usq=vsq/(1.-vsq)
!          gf=sqrt(1.+usq)
          gfsq=gf*gf
!- Calculation of density, xi -!
          ro=dro*gtmp
!          xi=(wgd*(1./gfsq))-(dro*usq)*(1./((1.+gf)*gfsq))
          xi=(wg-dro*gf)*(1./gfsq)
!- Calculation of pressure from EoS -!              
          if(ieos .eq. 0) then
            pp=gam2*xi
          elseif(ieos .eq. 1 .or. ieos .eq. 2) then
            tmp4a=2.*xi*(xi+2.*ro)
            tmp4b=5.*(xi+ro)+sqrt(9.*(xi*xi)+18.*ro*xi+25.*(ro*ro))
            pp=tmp4a*(1./tmp4b)
          endif

          if(ro .lt. 0.d0) then
            write(*,*) 'negative density', ro, 'at i, j, k', i, j ,k
!            ro=dmin*((x1(i)/rin)**(-3./2.))
            ro=dmin
          endif
          if (pp .lt. 0.d0) then
!            irecov=3
            write(*,*) 'negative pressure', pp, 'at i, j, k', i, j ,k
!            pp=pmin*((x1(i)/rin)**(-5./2.))
            pp=pmin
          endif
!        
          if(irecov .ne. 3) then
            qtcon(1)=qcon(1)+oncon(1)*qdotn
            qtcon(2)=qcon(2)+oncon(2)*qdotn
            qtcon(3)=qcon(3)+oncon(3)*qdotn

            util(1)=(gf*(1./(wg+bsq)))*(qtcon(1)+qdotb*bcon(1)*(1./wg))
            util(2)=(gf*(1./(wg+bsq)))*(qtcon(2)+qdotb*bcon(2)*(1./wg))
            util(3)=(gf*(1./(wg+bsq)))*(qtcon(3)+qdotb*bcon(3)*(1./wg))

!            vcon(1)=(1./(xxn+bsq))*(qtcon(1)+qdotb*bcon(1)/wg)
!            vcon(2)=(1./(xxn+bsq))*(qtcon(2)+qdotb*bcon(2)/wg)
!            vcon(3)=(1./(xxn+bsq))*(qtcon(3)+qdotb*bcon(3)/wg)
           endif
        endif   
!
!---------------------------------------------------------------------
!- Error message -!                
        if(irecov .eq. 1) then  
          write(6,*) ' >> Not convergence in recov2d' &
                    ,' at i, j, k:',i, j, k
          write(6,*) ' >> delta =', delta &
                    ,', at x1, x2, x3:',x1(i),x2(j),x3(k)
         
        elseif(irecov .eq. 2) then
          write(6,*) ' >> wg  < 0 in recov2d' &
                    ,' at i, j, k:',i, j, k
          write(6,*) ' >> wg', wg &
                    ,', at x1, x2, x3:',x1(i),x2(j),x3(k)

         elseif(irecov .eq. 3) then
           write(6,*) ' >> ro or pr < 0 in recov2d' &
                     ,' at i, j, k:',i, j, k
           write(6,*) ' >> ro, pr', ro, pp &
                     ,' at x1, x2, x3: ',x1(i),x2(j),x3(k)
        endif
!
!---------------------------------------------------------------------@
!   Set Primitive variables
!---------------------------------------------------------------------@
!
        if(irecov .eq. 0) then
         
          uri(1,i,j,k)=ro
          uri(2,i,j,k)=util(1)
          uri(3,i,j,k)=util(2)
          uri(4,i,j,k)=util(3)
!          uri(2,i,j,k)=vcon(1)
!          uri(3,i,j,k)=vcon(2)
!          uri(4,i,j,k)=vcon(3)
          uri(5,i,j,k)=pp
          uri(6,i,j,k)=1.d0
          uri(7,i,j,k)=uu(7,i,j,k)*(1./detg1)
          uri(8,i,j,k)=uu(8,i,j,k)*(1./detg1)
          uri(9,i,j,k)=uu(9,i,j,k)*(1./detg1)
        
        else
         
          uri(1,i,j,k)=ro1
          uri(2,i,j,k)=util1(1)
          uri(3,i,j,k)=util1(2)
          uri(4,i,j,k)=util1(3)
!          uri(2,i,j,k)=vcon1(1)
!          uri(3,i,j,k)=vcon1(2)
!          uri(4,i,j,k)=vcon1(3)
          uri(5,i,j,k)=pp1
          uri(6,i,j,k)=1.d0
          uri(7,i,j,k)=uu(7,i,j,k)*(1./detg1)
          uri(8,i,j,k)=uu(8,i,j,k)*(1./detg1)
          uri(9,i,j,k)=uu(9,i,j,k)*(1./detg1)
         
        endif
!
      enddo
    enddo
  enddo
!
!=====================================================================@
!
  deallocate( gcov1, gcon1, qcon, qcov, bcon, bcov, &
              oncov, oncon, util, util1, qtcon, stat=merr )
!
  return
end subroutine recov2
!
!---------------------------------------------------------------------@
subroutine recov3(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1,is1,ie1,js1,je1,ks1,ke1)
!---------------------------------------------------------------------@
!- Noble et al. (2006) 1 variable inversion procedure -!
!- (ideal EoS only)
  
  use pram, only : imax, jmax, kmax, nv, gam, c0, ieos, iter, dmin, pmin, &
                   kpol, akm, pi
  implicit none
!
  integer :: i, j, k, m, n, nnn, nm1, is1, ie1, js1, je1, ks1, ke1
  integer :: merr, igue, irecov

  real(8) :: uu(nv,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: uri(nv,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: gcon(0:3,0:3,is1:ie1,js1:je1,ks1:ke1), &
             gcov(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: detg(is1:ie1,js1:je1,ks1:ke1)

  real(8) :: x1(imax), x2(jmax), x3(kmax)

  real(8), allocatable :: gcov1(:,:), gcon1(:,:)
  real(8), allocatable :: qcon(:), qcov(:), bcon(:), bcov(:), &
                          oncov(:), oncon(:) 
  real(8), allocatable :: util(:), util1(:), qtcon(:)
      
  real(8) :: safty, saftybig, gam1, gam2, dv, alpha1, detg1 
  real(8) :: dro, ro, pp, ro1, pp1, roh, roe, bsq, qdotb, qdotbsq, qdotn, &
             qsq, qtsq, utsq, gfsq, gf, vsq, wg, wsq, xsq, qsq1, &
             delta, deltaend, df, f, xxn, xo, w2, w3, gtmp, ptmp, &
             dpdw, dpdw2, dpdvsq, dvsqdw 
  real(8) :: tmp1a, tmp1b, tmp1c, tmp1d, wgp, wgm, vsq1, tmp3a, tmp3b, tmp4a
  real(8) :: rin, rbh1, rr, th
!
!- allocate variables -!
  allocate(gcov1(0:3,0:3), gcon1(0:3,0:3), stat=merr)
  allocate(qcon(0:3), qcov(0:3), bcon(0:3), bcov(0:3), &
           oncov(0:3), oncon(0:3), util(1:3), util1(1:3), qtcon(1:3), stat=merr)
!
!--- define variable of safe number ----
      safty = 1.0d-10
      saftybig = 1.0d10
      dv=1.0d-5
!      
!--- Parameters ---!
!      
      gam1=gam/(gam-1.0)
      gam2=1.0/gam1
      wg=0.d0

      igue=1
!
      rin=15.d0
      rbh1=1.+sqrt(1.-akm*akm)
!
!
!=====================================================================@
!
! ----  Calculation of variables ---
!
  do k=ks1+nm1,ke1-nm1
    do j=js1+nm1,je1-nm1
       do i=is1+nm1,ie1-nm1
!
!- set parameter for Newton Raphson -!

      delta=1.d0
      deltaend = 1.d-8
      df=1.d0
      f=1.d0
!      irecov=0
!      
!        rr=x1(i)
!        th=x3(k)*180.d0/pi 
!        if(x1(i) .ge. 35.d0 .and. x1(i) .le. 35.5d0) then
!          if((uri(7,i,j,k)) .gt. 0.d0 .or. abs(uri(9,i,j,k)) .gt. 0.d0) then
!            write(*,*) 'rr, th, br, bph, bth in recov1=', rr, th, uri(7,i,j,k), uri(8,i,j,k), uri(9,i,j,k)
!          endif           
!       endif          
!
!- check the position within the BH radius -!
!
        if(x1(i) .le. rbh1) then
          irecov=1
        else
          irecov=0
        endif   
!          
!- copy of metric terms -!
!
        do m=0,3
          do n=0,3
            gcov1(m,n)=gcov(m,n,i,j,k)
            gcon1(m,n)=gcon(m,n,i,j,k)
          enddo
        enddo
!
        alpha1=1./sqrt(-gcon1(0,0))
!
        detg1=detg(i,j,k)
      
        dro=alpha1*uu(1,i,j,k)*(1./detg1)

!        qcov(0)=alpha1*(uu(5,i,j,k)+uu(1,i,j,k))*(1./detg1)
        qcov(0)=alpha1*(uu(5,i,j,k)-uu(1,i,j,k))*(1./detg1)
!        qcov(0)=alpha1*uu(5,i,j,k)*(1./detg1)
        qcov(1)=alpha1*uu(2,i,j,k)*(1./detg1)
        qcov(2)=alpha1*uu(3,i,j,k)*(1./detg1)
        qcov(3)=alpha1*uu(4,i,j,k)*(1./detg1)

        tmp4a=alpha1*uu(5,i,j,k)*(1./detg1)
        
        bcon(0)=0.d0
        bcon(1)=alpha1*uu(7,i,j,k)*(1./detg1)
        bcon(2)=alpha1*uu(8,i,j,k)*(1./detg1)
        bcon(3)=alpha1*uu(9,i,j,k)*(1./detg1)
         
        ro=uri(1,i,j,k)
        util(1)=uri(2,i,j,k)
        util(2)=uri(3,i,j,k)
        util(3)=uri(4,i,j,k)
!        vcon(1)=uri(2,i,j,k)
!        vcon(2)=uri(3,i,j,k)
!        vcon(3)=uri(4,i,j,k)
        pp=uri(5,i,j,k)
        
        ro1=uri(1,i,j,k)
        util1(1)=uri(2,i,j,k)
        util1(2)=uri(3,i,j,k)
        util1(3)=uri(4,i,j,k)
!        vcon1(1)(i,j,k)=uri(2,i,j,k)
!        vcon1(2)(i,j,k)=uri(3,i,j,k)
!        vcon1(3)(i,j,k)=uri(4,i,j,k)
        pp1=uri(5,i,j,k)
!
        if(ieos .eq. 0 .or. ieos .eq. 3) then
          roh=ro+(gam/(gam-1.0))*pp
        elseif(ieos .eq. 1) then
          roh=(5./2.)*pp &
              +sqrt((9./4.)*pp**2+ro**2)
        elseif(ieos .eq. 2) then
          roe=(3./2.)*(pp+((3.*pp**2) &
              /(2.0*ro+sqrt(2.*pp**2+4.*ro**2)) ))
          roh=ro+roe+pp
        endif
!
!- check B-field minimum -!
!
!        if(abs(bcon(1)) .lt. bmin) then
!          bcon(1)=0.d0
!        endif
!        if(abs(bcon(2)) .lt. bmin) then
!          bcon(2)=0.d0
!        endif       
!        if(abs(bcon(3)) .lt. bmin) then
!          bcon(3)=0.d0
!        endif   
        
!- cal of covariant 3-magnetic field -! 
        call lower(bcon,bcov,gcov1)
!- cal of contravariant 4-q -!
        call upper(qcov,qcon,gcon1)
!- cal of B-field square (3-vector) -!
        bsq=bcon(1)*bcov(1)+bcon(2)*bcov(2)+bcon(3)*bcov(3)
!- cal of q*B -!
        qdotb=qcov(0)*bcon(0)+qcov(1)*bcon(1)+qcov(2)*bcon(2)+qcov(3)*bcon(3)
!- cal of (Q*B)^2 -!
        qdotbsq=qdotb*qdotb
!- set of covariant 4-vector n -!
        oncov(0)=-1.d0*alpha1
        oncov(1)=0.d0
        oncov(2)=0.d0
        oncov(3)=0.d0
!- cal of contravariant 4-vector n -!
        call upper(oncov,oncon,gcon1)
!- cal of Q*n -!
        qdotn=qcon(0)*oncov(0)
!
!- cal of square of Q -!
        qsq=0.d0
        qsq=qcov(0)*qcon(0)+qcov(1)*qcon(1) &
           +qcov(2)*qcon(2)+qcov(3)*qcon(3)
!- cal of Q^2+Q*n -!
        qtsq=qsq+(qdotn*qdotn)
!
!        qsq1=0.d0
!        qsq1=qcov(1)*qcon(1)+qcov(2)*qcon(2)+qcov(3)*qcon(3)
!
!
!- Calculate W from last timestep and use for inital guess -!      
!
!- calculation of 4-velocity square 
        utsq=0.d0
!        vsq=0.d0
        do m=1,3
          do n=1,3
            utsq=utsq+gcov1(m,n)*util(m)*util(n)
!            vsq=vsq+gcov1(m,n)*util(m)*util(n)
          enddo
        enddo
!
        if(utsq .lt. 0.d0 .and. abs(utsq) .le. safty) then
          utsq=abs(utsq)
        endif
        if(utsq .lt. 0.d0 .or. abs(utsq) .gt. saftybig) then
          utsq=1.d9
        endif 
!- calculation of Lorentz factor -!
        gfsq=1.+utsq
        gf=sqrt(gfsq)
!        gf=1./sqrt(1.-vsq)
!        gfsq=gf**2
        vsq=(gfsq-1.)*(1./gfsq)
!- calculation of W -!      
!        if(vsq .lt. 0.9d0 .and. bsq/roh .lt. 1.d0) then 
! 
!          ro=dro/gf
!          roh=ro+gam1*pp
!          wg=abs(gfsq*roh)
!
!        else
          
!          if(igue .eq. 1) then
!            vsq1=1.d0
!          else
            vsq1=vsq
!          endif
!            
          tmp1a=4.-vsq1
          tmp1b=4.*(bsq+qdotn)
          tmp1c=qtsq+bsq*bsq+2.*bsq*qdotn
          tmp1d=(tmp1b*tmp1b)-4.*tmp1a*tmp1c
          if(tmp1d .le. 0.d0) then
            tmp1d=abs(tmp1d)
          endif
          wgp=(-tmp1b+sqrt(tmp1d))*(1./(2.*tmp1a))
          wgm=(-tmp1b-sqrt(tmp1d))*(1./(2.*tmp1a))
!
         if(tmp1d .le. 0.d0) then
           write(*,*) "inside sqrt < 0 for cal wg", tmp1b, tmp1c
          endif
!
          if(wgp .gt. 0.d0) then
            wg=wgp
          elseif(wgm .ge. 0.d0) then
            wg=wgm
          endif
          
!        endif
!
! Make sure that W is large enough so that v^2 < 1
!       
        tmp3a=(wg*wg*wg)*(wg+2.*bsq)-qdotbsq*(2.*wg+bsq)
        tmp3b=(wg*wg)*(qtsq-bsq*bsq)

        do nnn=1,iter
          if(tmp3a .le. tmp3b) then
!             write(*,*) "added wg at i,j,k=",i,j,k
             wg= abs(1.5*wg)
             tmp3a=(wg*wg*wg)*(wg+2.*bsq)-qdotbsq*(2.*wg+bsq)
             tmp3b=(wg*wg)*(qtsq-bsq*bsq)
          endif
        enddo
!
!=====================================================================@
!- Newton Rapson calculation -!

        xxn=wg
        xo=wg

!- Newton-Raphson routine start -!
         
        do nnn=1,iter

          if(abs(delta) .ge. deltaend .and. irecov .eq. 0) then
           
            if(nnn .eq. iter) then
              irecov=1

            else
              wg=xxn

              wsq=wg*wg
              w3=wsq*wg
!
!- calculation of v^2 -!
!
              xsq=(bsq+wg)*(bsq+wg)
              vsq=abs((wsq*qtsq+qdotbsq*(bsq+2.*wg))*(1./(wsq*xsq)))
!       
              if(vsq .gt. 1.d0) then
                write(6,*) 'vsq > 1.0' 
                vsq=1.d0-dv
              endif
              
              gtmp=1.-vsq
!
!-calculation of dv^2/dw -!
!
              tmp1a=(bsq+wg)
              tmp1b=(bsq+wg)*(bsq+wg)*(bsq+wg)
              tmp1c=-2.*qtsq*(1./tmp1b)
              tmp1d=-2.*qdotbsq*(3.*wg*tmp1a+bsq*bsq)*(1./(tmp1b*w3))
              dvsqdw=tmp1c+tmp1d
!
!- calculation of pressure -!
!              
              if(ieos .eq. 0) then !- gamma-law EoS -!
                ptmp=gam2*(wg*gtmp-dro*sqrt(gtmp))
              elseif(ieos .eq. 3) then !- polytropic EoS -!   
                ptmp=kpol*(dro*sqrt(gtmp))**gam
              endif
!
!- calculation of dp/dw, dp/dv^2 -!
!              
              if(ieos .eq. 0) then !- gamma-law EoS -!
                dpdw=gam2*gtmp
                dpdvsq=-gam2*wg+0.5*gam2*dro*(1./(sqrt(gtmp)))
                dpdw2=dpdw+dpdvsq*dvsqdw                
              endif
!             
!- calculation of f and df -!
!              
              f=-wg-0.5*bsq*(1.+vsq)+(0.5*qdotbsq*(1./wsq))-qdotn+ptmp
              df=-1.+dpdw2-(qdotbsq*(1./w3))-0.5*bsq*dvsqdw
!
!- calculation of delta -!
!              
              delta=f*(1./df)
! 
              xo=wg
              xxn=wg-delta
!
              if(xxn .lt. 0.d0) then
!                irecov=2
                 xxn=abs(xxn)
              endif
!         
              if(xxn .gt. 1.d8) then
                xxn=xo
              endif
!              
            endif
          endif

        enddo
!
!- Newton-Raphson routine end -!
!
        if(irecov .eq. 0) then
          wg=xxn
          wsq=wg*wg
          xsq=(bsq+wg)*(bsq+wg)
          vsq=abs((wsq*qtsq+qdotbsq*(bsq+2.*wg))*(1./(wsq*xsq)))
!       
          if(vsq .gt. 1.d0) then
            write(6,*) 'vsq > 1.0' 
            vsq=1.d0-dv
          endif           
!
          gtmp=sqrt(1.-vsq)
          gf=1./gtmp
          ro=dro*gtmp
!
          w2=wg*(1.-vsq)
          if(ieos .eq. 3) then !-polytropic EoS
            pp=kpol*ro**gam
          elseif(ieos .eq. 0) then !- gamma-law EoS 
            pp=gam2*(w2-ro)
          endif
            
          if(ro .lt. 0.d0) then
            write(*,*) 'negative density', ro, 'at i, j, k', i, j ,k
!            ro=dmin*((x1(i)/rin)**(-3./2.))
            ro=dmin
          endif
          if (pp .lt. 0.d0) then
!            irecov=3
            write(*,*) 'negative pressure', pp, 'at i, j, k', i, j ,k
!            pp=pmin*((x1(i)/rin)**(-5./2.))
            pp=pmin
          endif

!          if(irecov .ne. 3) then
            qtcon(1)=qcon(1)+oncon(1)*qdotn
            qtcon(2)=qcon(2)+oncon(2)*qdotn
            qtcon(3)=qcon(3)+oncon(3)*qdotn

            util(1)=(gf*(1./(wg+bsq)))*(qtcon(1)+qdotb*bcon(1)*(1./wg))
            util(2)=(gf*(1./(wg+bsq)))*(qtcon(2)+qdotb*bcon(2)*(1./wg))
            util(3)=(gf*(1./(wg+bsq)))*(qtcon(3)+qdotb*bcon(3)*(1./wg))

!            vcon(1)=(1./(xxn+bsq))*(qtcon(1)+qdotb*bcon(1)/wg)
!            vcon(2)=(1./(xxn+bsq))*(qtcon(2)+qdotb*bcon(2)/wg)
!            vcon(3)=(1./(xxn+bsq))*(qtcon(3)+qdotb*bcon(3)/wg)
!           endif
        endif
                 
        wg=xxn  

!---------------------------------------------------------------------
!- Error message -!                
        if(irecov .eq. 1) then
          write(6,*) ' >> Not convergence in recov3' &
                    ,' at i, j, k:',i, j, k
          write(6,*) ' >> delta =', delta &
                    ,', at x1, x2, x3:',x1(i),x2(j),x3(k)
         
        elseif(irecov .eq. 2) then
          write(6,*) ' >> wg  < 0 in recov3' &
                    ,' at i, j, k:',i, j, k
          write(6,*) ' >> wg', wg &
                    ,', at x1, x2, x3:',x1(i),x2(j),x3(k)

!         elseif(irecov .eq. 3) then
!           write(6,*) ' >> ro or pr < 0 in recov1d' &
!                    ,' at i, j, k:',i, j, k
!           write(6,*) ' >> ro, pr', ro, pp &
!                     ,' at x1, x2, x3: ',x1(i),x2(j),x3(k)
        endif
!
!---------------------------------------------------------------------@
!   Set Primitive variables
!---------------------------------------------------------------------@
!
        if(irecov .eq. 0) then
         
          uri(1,i,j,k)=ro
          uri(2,i,j,k)=util(1)
          uri(3,i,j,k)=util(2)
          uri(4,i,j,k)=util(3)
!          uri(2,i,j,k)=vcon(1)
!          uri(3,i,j,k)=vcon(2)
!          uri(4,i,j,k)=vcon(3)
          uri(5,i,j,k)=pp
          uri(6,i,j,k)=1.d0
!          uri(7,i,j,k)=bcon(1)
!          uri(8,i,j,k)=bcon(2)
!          uri(9,i,j,k)=bcon(3)
!          
          uri(7,i,j,k)=uu(7,i,j,k)*(1./detg1)
          uri(8,i,j,k)=uu(8,i,j,k)*(1./detg1)
          uri(9,i,j,k)=uu(9,i,j,k)*(1./detg1)
!
        rr=x1(i)
        th=x3(k)*180.d0/pi 
!        if(x1(i) .ge. 35.d0 .and. x1(i) .le. 35.5d0) then
!          if((uri(7,i,j,k)) .gt. 0.d0 .or. abs(uri(9,i,j,k)) .gt. 0.d0) then
!            write(*,*) 'rr, th, br, bph, bth in recov3a=', rr, th, uri(7,i,j,k), uri(8,i,j,k), uri(9,i,j,k)
!          endif           
!        endif
       
        else
         
          uri(1,i,j,k)=ro1
          uri(2,i,j,k)=util1(1)
          uri(3,i,j,k)=util1(2)
          uri(4,i,j,k)=util1(3)
!          uri(2,i,j,k)=vcon1(1)
!          uri(3,i,j,k)=vcon1(2)
!          uri(4,i,j,k)=vcon1(3)
          uri(5,i,j,k)=pp1
          uri(6,i,j,k)=1.d0
          uri(7,i,j,k)=uu(7,i,j,k)*(1./detg1)
          uri(8,i,j,k)=uu(8,i,j,k)*(1./detg1)
          uri(9,i,j,k)=uu(9,i,j,k)*(1./detg1)
         
        endif
!
      enddo
    enddo
  enddo
!
!=====================================================================@
!
  deallocate( gcov1, gcon1, qcon, qcov, bcon, bcov, &
              oncov, oncon, util, util1, qtcon, stat=merr )
!
  return
end subroutine recov3
!
!---------------------------------------------------------------------@
subroutine recov4(uu,uri,gcov,gcon,detg,x1,x2,x3,nm1,is1,ie1,js1,je1,ks1,ke1)
!---------------------------------------------------------------------@
!- 1 variable inversion procedure -!
!- (Polytropic EoS only)
  
  use pram, only : imax, jmax, kmax, nv, gam, c0, ieos, iter, dmin, pmin, &
                   kpol
  implicit none
!
  integer :: i, j, k, m, n, nnn, nm1, is1, ie1, js1, je1, ks1, ke1
  integer :: merr, igue, irecov

  real(8) :: uu(nv,is1:ie1,js1:je1,ks1:ke1)
  real(8) :: uri(nv,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: gcon(0:3,0:3,is1:ie1,js1:je1,ks1:ke1), &
             gcov(0:3,0:3,is1:ie1,js1:je1,ks1:ke1)

  real(8) :: detg(is1:ie1,js1:je1,ks1:ke1)

  real(8) :: x1(imax), x2(jmax), x3(kmax)

  real(8), allocatable :: gcov1(:,:), gcon1(:,:)
  real(8), allocatable :: qcon(:), qcov(:), bcon(:), bcov(:), &
                          oncov(:), oncon(:) 
  real(8), allocatable :: util(:), util1(:), qtcon(:)
      
  real(8) :: safty, saftybig, gam1, gam2, dv, alpha1, detg1 
  real(8) :: dro, ro, pp, ro1, pp1, roh, roe, bsq, qdotb, qdotbsq, qdotn, &
             qsq, qtsq, utsq, gfsq, gf, vsq, wg, wsq, qsq1, &
             delta, deltaend, df, f, xxn, xo, w3, gtmp, &
             dgfdvsq, dwdvsq 
  real(8) :: tmp1a, tmp1b, tmp1c, tmp1d, wgp, wgm, vsq1, tmp3a, tmp3b, tmp4a
  real(8) :: rin
!
!- allocate variables -!
  allocate(gcov1(0:3,0:3), gcon1(0:3,0:3), stat=merr)
  allocate(qcon(0:3), qcov(0:3), bcon(0:3), bcov(0:3), &
           oncov(0:3), oncon(0:3), util(1:3), util1(1:3), qtcon(1:3), stat=merr)
!
!--- define variable of safe number ----
      safty = 1.0d-10
      saftybig = 1.0d10
      dv=1.0d-5
      
      gam1=gam/(gam-1.0)
      gam2=1.0/gam1
      wg=0.d0

      igue=1
!
      rin=15.d0

!=====================================================================@
!
! ----  Calculation of variables ---
!
  do k=ks1+nm1,ke1-nm1
    do j=js1+nm1,je1-nm1
      do i=is1+nm1,ie1-nm1
!- copy of metric terms -!
        do m=0,3
          do n=0,3
            gcov1(m,n)=gcov(m,n,i,j,k)
            gcon1(m,n)=gcon(m,n,i,j,k)
          enddo
        enddo
!
        alpha1=1./sqrt(-gcon1(0,0))
!
        detg1=detg(i,j,k)
      
        dro=alpha1*uu(1,i,j,k)/detg1

!        qcov(0)=alpha1*(uu(5,i,j,k)+uu(1,i,j,k))/detg1
        qcov(0)=alpha1*(uu(5,i,j,k)-uu(1,i,j,k))/detg1
!        qcov(0)=alpha1*uu(5,i,j,k)/detg1
        qcov(1)=alpha1*uu(2,i,j,k)/detg1
        qcov(2)=alpha1*uu(3,i,j,k)/detg1
        qcov(3)=alpha1*uu(4,i,j,k)/detg1

        tmp4a=alpha1*uu(5,i,j,k)/detg1
        
        bcon(0)=0.d0
        bcon(1)=alpha1*uu(7,i,j,k)/detg1
        bcon(2)=alpha1*uu(8,i,j,k)/detg1
        bcon(3)=alpha1*uu(9,i,j,k)/detg1
         
        ro=uri(1,i,j,k)
        util(1)=uri(2,i,j,k)
        util(2)=uri(3,i,j,k)
        util(3)=uri(4,i,j,k)
!        vcon(1)=uri(2,i,j,k)
!        vcon(2)=uri(3,i,j,k)
!        vcon(3)=uri(4,i,j,k)
        pp=kpol*ro**gam
        
        ro1=uri(1,i,j,k)
        util1(1)=uri(2,i,j,k)
        util1(2)=uri(3,i,j,k)
        util1(3)=uri(4,i,j,k)
!        vcon1(1)(i,j,k)=uri(2,i,j,k)
!        vcon1(2)(i,j,k)=uri(3,i,j,k)
!        vcon1(3)(i,j,k)=uri(4,i,j,k)
        pp1=kpol*ro1**gam
!
        roh=ro+gam1*pp

!- cal of covariant 3-magnetic field -! 
        call lower(bcon,bcov,gcov1)
!- cal of contravariant 4-q -!
        call upper(qcov,qcon,gcon1)
!- cal of B-field square (3-vector) -!
        bsq=bcon(1)*bcov(1)+bcon(2)*bcov(2)+bcon(3)*bcov(3)
!- cal of q*B -!
        qdotb=qcov(0)*bcon(0)+qcov(1)*bcon(1)+qcov(2)*bcon(2)+qcov(3)*bcon(3)
!- cal of (Q*B)^2 -!
        qdotbsq=qdotb*qdotb
!- set of covariant 4-vector n -!
        oncov(0)=-1.d0*alpha1
        oncov(1)=0.d0
        oncov(2)=0.d0
        oncov(3)=0.d0
!- cal of contravariant 4-vector n -!
        call upper(oncov,oncon,gcon1)
!- cal of Q*n -!
        qdotn=qcon(0)*oncov(0)
!
!- cal of square of Q -!
        qsq=0.d0
        qsq=qcov(0)*qcon(0)+qcov(1)*qcon(1) &
           +qcov(2)*qcon(2)+qcov(3)*qcon(3)
!- cal of Q^2+Q*n -!
        qtsq=qsq+(qdotn*qdotn)
!
!        qsq1=0.d0
!        qsq1=qcov(1)*qcon(1)+qcov(2)*qcon(2)+qcov(3)*qcon(3)
!
!
!- Calculate W from last timestep and use for inital guess -!      
!
!- calculation of 4-velocity square 
        utsq=0.d0
!        vsq=0.d0
        do m=1,3
          do n=1,3
            utsq=utsq+gcov1(m,n)*util(m)*util(n)
!            vsq=vsq+gcov1(m,n)*util(m)*util(n)
          enddo
        enddo
!
        if(utsq .lt. 0.d0 .and. abs(utsq) .le. safty) then
          utsq=abs(utsq)
        endif
        if(utsq .lt. 0.d0 .or. abs(utsq) .gt. saftybig) then
          utsq=1.d9
        endif 
!- calculation of Lorentz factor -!
        gfsq=1.+utsq
        gf=sqrt(gfsq)
!        gf=1./sqrt(1.-vsq)
!        gfsq=gf**2
        vsq=(gfsq-1.)/gfsq
!- calculation of W -!      
!        if(vsq .lt. 0.9d0 .and. bsq/roh .lt. 1.d0) then 
! 
          ro=dro/gf
          pp=kpol*ro**gam
          roh=ro+gam1*pp
          wg=abs(gfsq*roh)
!
!        else
          
!          if(igue .eq. 1) then
!            vsq1=1.d0
!          else
!            vsq1=vsq
!          endif
!            
!          tmp1a=4.-vsq1
!          tmp1b=4.*(bsq+qdotn)
!          tmp1c=qtsq+bsq*bsq+2.*bsq*qdotn
!          tmp1d=(tmp1b*tmp1b)-4.*tmp1a*tmp1c
!          if(tmp1d .le. 0.d0) then
!            tmp1d=abs(tmp1d)
!          endif
!          wgp=(-tmp1b+sqrt(tmp1d))/(2.*tmp1a)
!          wgm=(-tmp1b-sqrt(tmp1d))/(2.*tmp1a)
!
!         if(tmp1d .le. 0.d0) then
!           write(*,*) "inside sqrt < 0 for cal wg", tmp1b, tmp1c
!          endif
!
!          if(wgp .gt. 0.d0) then
!            wg=wgp
!          elseif(wgm .ge. 0.d0) then
!            wg=wgm
!          endif
          
!        endif
!
! Make sure that W is large enough so that v^2 < 1
!       
        tmp3a=(wg*wg*wg)*(wg+2.*bsq)-qdotbsq*(2.*wg+bsq)
        tmp3b=(wg*wg)*(qtsq-bsq*bsq)

        do nnn=1,iter
          if(tmp3a .le. tmp3b) then
             write(*,*) "added wg at i,j,k=",i,j,k
             wg= abs(10.0*wg)
             tmp3a=(wg*wg*wg)*(wg+2.*bsq)-qdotbsq*(2.*wg+bsq)
             tmp3b=(wg*wg)*(qtsq-bsq*bsq)
          endif
        enddo
!
!=====================================================================@
!- Newton Rapson calculation -!

!- set parameter  -!

        delta=1.d0
        deltaend = 1.d-8
        df=1.d0
        f=1.d0
        irecov=0

        xxn=wg
        xo=wg

!- Newton-Raphson routine start -!
         
        do nnn=1,iter

          if(abs(delta) .ge. deltaend .and. irecov .eq. 0) then
           
            if(nnn .eq. iter) then
              irecov=1

            else
              vsq=xxn
!               
!- calculation of W -!
!              
              gf=1./sqrt(1.-vsq)
              gfsq=gf*gf
              
              ro=dro/gf
              pp=kpol*ro**gam
              roh=ro+gam1*pp
              wg=abs(gfsq*roh)
              
              wsq=wg*wg
              w3=wsq*wg
!
!-calculation of dW/dv^2 -!
!
              dgfdvsq=0.5*(1.-vsq)**(-3./2.)
              dwdvsq=2.*gf*roh*dgfdvsq
!              dwdvsq=dro*dgfdvsq &
!                    +gam1*kpol*(dro**gam)*(2.-gam)*(gf**(1.-gam))*dgfdvsq
!              
!- calculation of f and df -!
!              
              f=-qtsq+vsq*(wg+bsq)*(wg+bsq)-(qdotbsq*(bsq+2.*wg))/wsq
              tmp1a=(wg+bsq)*(wg+bsq)
              tmp1b=2.*vsq*(wg+bsq)*dwdvsq
              tmp1c=(2.*qdotbsq*(bsq+2.*wg)/w3)*dwdvsq
              tmp1d=(-2.*qdotbsq/wsq)*dwdvsq
              df=tmp1a+tmp1b+tmp1c+tmp1d
!
!- calculation of delta -!
!              
              delta=f/df
! 
              xo=vsq
              xxn=vsq-delta
!
              if(xxn .ge. 1.d0) then
!                irecov=2
                xxn=1.-dv
              endif
!              
            endif
          endif

        enddo
!
!- Newton-Raphson routine end -!
!
        if(irecov .eq. 0) then
          vsq=xxn
          gf=1./sqrt(1.-vsq)
          gfsq=gf*gf
              
          ro=dro/gf
          pp=kpol*ro**gam
          roh=ro+gam1*pp

          wg=abs(gfsq*roh) 
            
          if(ro .lt. 0.d0) then
            write(*,*) 'negative density', ro, 'at i, j, k', i, j ,k
!            ro=dmin*((x1(i)/rin)**(-3./2.))
            ro=dmin
          endif
          if (pp .lt. 0.d0) then
!            irecov=3
            write(*,*) 'negative pressure', pp, 'at i, j, k', i, j ,k
!            pp=pmin*((x1(i)/rin)**(-5./2.))
            pp=pmin
          endif

!          if(irecov .ne. 3) then
            qtcon(1)=qcon(1)+oncon(1)*qdotn
            qtcon(2)=qcon(2)+oncon(2)*qdotn
            qtcon(3)=qcon(3)+oncon(3)*qdotn

            util(1)=(gf/(wg+bsq))*(qtcon(1)+qdotb*bcon(1)/wg)
            util(2)=(gf/(wg+bsq))*(qtcon(2)+qdotb*bcon(2)/wg)
            util(3)=(gf/(wg+bsq))*(qtcon(3)+qdotb*bcon(3)/wg)

!            vcon(1)=(1./(xxn+bsq))*(qtcon(1)+qdotb*bcon(1)/wg)
!            vcon(2)=(1./(xxn+bsq))*(qtcon(2)+qdotb*bcon(2)/wg)
!            vcon(3)=(1./(xxn+bsq))*(qtcon(3)+qdotb*bcon(3)/wg)
!           endif
        endif
                 
        wg=xxn  

!---------------------------------------------------------------------
!- Error message -!                
        if(irecov .eq. 1) then
          write(6,*) ' >> Not convergence in recov3' &
                    ,' at i, j, k:',i, j, k
          write(6,*) ' >> delta =', delta &
                    ,', at x1, x2, x3:',x1(i),x2(j),x3(k)
         
!        elseif(irecov .eq. 2) then
!          write(6,*) ' >> wg  < 0 in recov3' &
!                    ,' at i, j, k:',i, j, k
!          write(6,*) ' >> wg', wg &
!                    ,', at x1, x2, x3:',x1(i),x2(j),x3(k)

!         elseif(irecov .eq. 3) then
!           write(6,*) ' >> ro or pr < 0 in recov1d' &
!                    ,' at i, j, k:',i, j, k
!           write(6,*) ' >> ro, pr', ro, pp &
!                     ,' at x1, x2, x3: ',x1(i),x2(j),x3(k)
        endif
!
!---------------------------------------------------------------------@
!   Set Primitive variables
!---------------------------------------------------------------------@
!
        if(irecov .eq. 0) then
         
          uri(1,i,j,k)=ro
          uri(2,i,j,k)=util(1)
          uri(3,i,j,k)=util(2)
          uri(4,i,j,k)=util(3)
!          uri(2,i,j,k)=vcon(1)
!          uri(3,i,j,k)=vcon(2)
!          uri(4,i,j,k)=vcon(3)
          uri(5,i,j,k)=pp
          uri(6,i,j,k)=1.d0
          uri(7,i,j,k)=uu(7,i,j,k)/detg1
          uri(8,i,j,k)=uu(8,i,j,k)/detg1
          uri(9,i,j,k)=uu(9,i,j,k)/detg1
        
        else
         
          uri(1,i,j,k)=ro1
          uri(2,i,j,k)=util1(1)
          uri(3,i,j,k)=util1(2)
          uri(4,i,j,k)=util1(3)
!          uri(2,i,j,k)=vcon1(1)
!          uri(3,i,j,k)=vcon1(2)
!          uri(4,i,j,k)=vcon1(3)
          uri(5,i,j,k)=pp1
          uri(6,i,j,k)=1.d0
          uri(7,i,j,k)=uu(7,i,j,k)/detg1
          uri(8,i,j,k)=uu(8,i,j,k)/detg1
          uri(9,i,j,k)=uu(9,i,j,k)/detg1
         
        endif
!
      enddo
    enddo
  enddo
!
!=====================================================================@
!
  deallocate( gcov1, gcon1, qcon, qcov, bcon, bcov, &
              oncov, oncon, util, util1, qtcon, stat=merr )
!
  return
end subroutine recov4
