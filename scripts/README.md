# Running and producing data from RAISHIN GRMHD simulations using a cluster

Here are the basic steps required in order to run and produce data for RAISHIN.

- Setup your model carefully (`pram.f90`, `mdgrmhd.f90` etc)

- Run `prepare.py` to configure the appropriate number of cores and nodes for your MPI run. For example, prepare source for 2D, 512 cores, 24 cores/node:

```python
prepare.py 2 512 24
```

Preparing the same run in a desktop machine:

    prepare.py 2 8 0

- If running in a cluster: copy the resulting files to `alphacrucis` (24 cores/node). I use a `bitbucket` repo specific for this purpose (`git push` on your station, `git pull` on `alphacrucis`).

- `make clean`, `make` and `qsub mpirun.job` to submit the job. Wait. Use `status.sh` once in a while to check the status of your simulation.

- Once finished, copy the raw data files to the workstation where the visualization and analysis will be carried out

- Run `convert.py` to convert the raw data files to VTK format

# Running on the desktop

Use `run.py`. TBD

# TODO

- [ ] adapt scripts above to handle 3D