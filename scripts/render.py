import nmmn.grmhd
import pylab

def render(file):
    d=nmmn.grmhd.Raishin()
    d.vtk(file)
    var=d.regrid(d.rho)
    pylab.imshow(log10(var),extent=[d.xc1d[0],d.xc1d[-1],d.yc1d[0],d.yc1d[-1]], cmap='viridis', vmin=-6, vmax=0)
    pylab.xlim(0,30)
    pylab.ylim(-20,20)
    pylab.axes().set_aspect('equal')
    pylab.colorbar()
    circle2=pylab.Circle((0,0),1,color='k')
    pylab.gca().add_artist(circle2)
    pylab.savefig('tmp.png',transparent=True,dpi=150)




    import time # for benchmarking
    import multiprocessing
    
    print "BCES,", nsim,"trials... ",
    tic=time.time()
    
    # Find out number of cores available
    ncores=multiprocessing.cpu_count()
    # We will divide the processing into how many parts?
    n=2*ncores
    
    """
    Must create lists that will be distributed among the many
    cores with structure 
    core1 <- [y1,y1err,y2,y2err,cerr,nsim/n]
    core2 <- [y1,y1err,y2,y2err,cerr,nsim/n]
    etc...
    """
    pargs=[]    # this is a list of lists!
    for i in range(n):
        pargs.append([y1,y1err,y2,y2err,cerr,nsim/n])
    
    # Initializes the parallel engine
    pool = multiprocessing.Pool(processes=ncores)   # multiprocessing package
            
    """
    Each core processes ab(input)
        return matrixes Am,Bm with the results of nsim/n
        presult[i][0] = Am with nsim/n lines
        presult[i][1] = Bm with nsim/n lines
    """
    presult=pool.map(ab, pargs) # multiprocessing
    pool.close()    # close the parallel engine