#!/usr/bin/env bash
#
# Script that tells you:
#
# § [ ] when the simulation started running
# § [x] how long (simulation `t`) it has ran so far
# § [x] storage pressure
# 

# how long has it been running?
if [[ $(qstat) ]] ; then
	echo "Job running"
else
	echo "Job finished"
fi
echo

# current status of simulation 
grep "time=" grmhd.out | tail -n1
echo

# storage pressure
echo -n "Space taken = "
du  -hc *dat | tail -n1